-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Feb 02, 2019 at 07:54 AM
-- Server version: 5.7.23
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hba`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `uid` int(11) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `role` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`uid`, `fullname`, `username`, `password`, `salt`, `email`, `phone`, `role`, `status`, `created`) VALUES
(1, 'Super Administrator', 'superadmin', '1728c2e216c9ecf29fa6175e8e77b2dd4a92bf86', 'manzoor', 'support@celiums.com', '914954060511', 1, 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `admins_login_history`
--

CREATE TABLE `admins_login_history` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `ipaddress` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admins_login_history`
--

INSERT INTO `admins_login_history` (`id`, `uid`, `ipaddress`, `timestamp`) VALUES
(1, 1, '::1', '0000-00-00 00:00:00'),
(2, 1, '::1', '0000-00-00 00:00:00'),
(3, 1, '::1', '0000-00-00 00:00:00'),
(4, 1, '::1', '0000-00-00 00:00:00'),
(5, 1, '::1', '0000-00-00 00:00:00'),
(6, 1, '::1', '0000-00-00 00:00:00'),
(7, 1, '::1', '0000-00-00 00:00:00'),
(8, 1, '::1', '0000-00-00 00:00:00'),
(9, 1, '::1', '0000-00-00 00:00:00'),
(10, 1, '::1', '0000-00-00 00:00:00'),
(11, 1, '::1', '0000-00-00 00:00:00'),
(12, 1, '::1', '0000-00-00 00:00:00'),
(13, 1, '127.0.0.1', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `admin_function`
--

CREATE TABLE `admin_function` (
  `id` int(11) NOT NULL,
  `function_name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `function_link` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `admin_function_permission`
--

CREATE TABLE `admin_function_permission` (
  `function_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `admin_menu`
--

CREATE TABLE `admin_menu` (
  `id` int(11) NOT NULL,
  `class` varchar(100) NOT NULL,
  `name` varchar(300) NOT NULL,
  `link` varchar(300) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `status` enum('Y','N') NOT NULL DEFAULT 'N',
  `display` int(1) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_menu`
--

INSERT INTO `admin_menu` (`id`, `class`, `name`, `link`, `parent_id`, `status`, `display`, `sort_order`) VALUES
(1, 'fa-dashboard', 'Dashboard', 'home/dashboard', 0, 'Y', 1, 1),
(2, 'fa-user', 'Manage Users', '', 0, 'Y', 1, 11),
(3, '', 'Users', 'users/overview', 2, 'Y', 1, 1),
(4, '', 'Add User', 'users/add', 2, 'Y', 1, 2),
(5, '', 'Roles', 'users/roles', 2, 'Y', 1, 3),
(6, '', 'Add Roles', 'users/addrole', 2, 'Y', 1, 4),
(7, '', 'Users Submenu', 'users', 2, 'Y', 0, 1),
(8, '', 'Dashboard Submenu', 'home', 1, 'Y', 0, 1),
(9, '', 'Settings', 'home/settings', 1, 'Y', 1, 1),
(10, '', 'Clear Cache', 'home/clearcache', 1, 'Y', 1, 2),
(11, '', 'Update Database', 'home/updatedb', 1, 'Y', 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `admin_menu_permission`
--

CREATE TABLE `admin_menu_permission` (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `admin_reset`
--

CREATE TABLE `admin_reset` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_key` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `admin_roles`
--

CREATE TABLE `admin_roles` (
  `rid` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_roles`
--

INSERT INTO `admin_roles` (`rid`, `name`, `status`) VALUES
(1, 'Super Admin', 1);

-- --------------------------------------------------------

--
-- Table structure for table `admin_widgets`
--

CREATE TABLE `admin_widgets` (
  `wid` int(11) NOT NULL,
  `widget_name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `widget_key` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `admin_widgets_permission`
--

CREATE TABLE `admin_widgets_permission` (
  `wid` int(11) NOT NULL,
  `rid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `database_updates`
--

CREATE TABLE `database_updates` (
  `update_id` int(11) NOT NULL,
  `update_name` varchar(255) NOT NULL,
  `update_status` varchar(255) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `title` varchar(300) NOT NULL,
  `settingkey` varchar(300) NOT NULL,
  `settingtype` varchar(100) NOT NULL,
  `status` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `title`, `settingkey`, `settingtype`, `status`) VALUES
(1, 'From Name', 'FROM_NAME', 'text', 'Y'),
(2, 'From Mail Address', 'FROM_MAIL', 'text', 'Y'),
(3, 'Registration Notification Email', 'REG_NOTIFY_EMAIL', 'text', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `settings_desc`
--

CREATE TABLE `settings_desc` (
  `desc_id` int(11) NOT NULL,
  `settings_id` int(11) NOT NULL,
  `settingvalue` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings_desc`
--

INSERT INTO `settings_desc` (`desc_id`, `settings_id`, `settingvalue`) VALUES
(1, 1, 'Easy Clean'),
(2, 2, 'support@easyclean.ae'),
(3, 3, 'support@easyclean.ae');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `admins_login_history`
--
ALTER TABLE `admins_login_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_function`
--
ALTER TABLE `admin_function`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_menu`
--
ALTER TABLE `admin_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_reset`
--
ALTER TABLE `admin_reset`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_roles`
--
ALTER TABLE `admin_roles`
  ADD PRIMARY KEY (`rid`);

--
-- Indexes for table `admin_widgets`
--
ALTER TABLE `admin_widgets`
  ADD PRIMARY KEY (`wid`);

--
-- Indexes for table `database_updates`
--
ALTER TABLE `database_updates`
  ADD PRIMARY KEY (`update_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings_desc`
--
ALTER TABLE `settings_desc`
  ADD PRIMARY KEY (`desc_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `admins_login_history`
--
ALTER TABLE `admins_login_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `admin_function`
--
ALTER TABLE `admin_function`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admin_menu`
--
ALTER TABLE `admin_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `admin_reset`
--
ALTER TABLE `admin_reset`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admin_roles`
--
ALTER TABLE `admin_roles`
  MODIFY `rid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admin_widgets`
--
ALTER TABLE `admin_widgets`
  MODIFY `wid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `database_updates`
--
ALTER TABLE `database_updates`
  MODIFY `update_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `settings_desc`
--
ALTER TABLE `settings_desc`
  MODIFY `desc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
