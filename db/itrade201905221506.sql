-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 22, 2019 at 03:05 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `itrade`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `uid` int(11) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `role` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`uid`, `fullname`, `username`, `password`, `salt`, `email`, `phone`, `role`, `status`, `created`) VALUES
(1, 'Super Administrator', 'superadmin', '1728c2e216c9ecf29fa6175e8e77b2dd4a92bf86', 'manzoor', 'support@celiums.com', '914954060511', 1, 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `admins_login_history`
--

CREATE TABLE `admins_login_history` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `ipaddress` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admins_login_history`
--

INSERT INTO `admins_login_history` (`id`, `uid`, `ipaddress`, `timestamp`) VALUES
(1, 1, '::1', '0000-00-00 00:00:00'),
(2, 1, '::1', '0000-00-00 00:00:00'),
(3, 1, '::1', '0000-00-00 00:00:00'),
(4, 1, '::1', '0000-00-00 00:00:00'),
(5, 1, '::1', '0000-00-00 00:00:00'),
(6, 1, '::1', '0000-00-00 00:00:00'),
(7, 1, '::1', '0000-00-00 00:00:00'),
(8, 1, '::1', '0000-00-00 00:00:00'),
(9, 1, '::1', '0000-00-00 00:00:00'),
(10, 1, '::1', '0000-00-00 00:00:00'),
(11, 1, '::1', '0000-00-00 00:00:00'),
(12, 1, '::1', '0000-00-00 00:00:00'),
(13, 1, '127.0.0.1', '0000-00-00 00:00:00'),
(14, 1, '127.0.0.1', '0000-00-00 00:00:00'),
(15, 1, '127.0.0.1', '0000-00-00 00:00:00'),
(16, 1, '127.0.0.1', '0000-00-00 00:00:00'),
(17, 1, '127.0.0.1', '0000-00-00 00:00:00'),
(18, 1, '127.0.0.1', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `admin_function`
--

CREATE TABLE `admin_function` (
  `id` int(11) NOT NULL,
  `function_name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `function_link` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `admin_function_permission`
--

CREATE TABLE `admin_function_permission` (
  `function_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `admin_menu`
--

CREATE TABLE `admin_menu` (
  `id` int(11) NOT NULL,
  `class` varchar(100) NOT NULL,
  `name` varchar(300) NOT NULL,
  `link` varchar(300) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `status` enum('Y','N') NOT NULL DEFAULT 'N',
  `display` int(1) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_menu`
--

INSERT INTO `admin_menu` (`id`, `class`, `name`, `link`, `parent_id`, `status`, `display`, `sort_order`) VALUES
(1, 'fa-dashboard', 'Dashboard', 'home/dashboard', 0, 'Y', 1, 1),
(2, 'fa-user', 'Manage Users', '', 0, 'Y', 1, 11),
(3, '', 'Users', 'users/overview', 2, 'Y', 1, 1),
(4, '', 'Add User', 'users/add', 2, 'Y', 1, 2),
(5, '', 'Roles', 'users/roles', 2, 'Y', 1, 3),
(6, '', 'Add Roles', 'users/addrole', 2, 'Y', 1, 4),
(7, '', 'Users Submenu', 'users', 2, 'Y', 0, 1),
(8, '', 'Dashboard Submenu', 'home', 1, 'Y', 0, 1),
(9, '', 'Settings', 'home/settings', 1, 'Y', 1, 1),
(10, '', 'Clear Cache', 'home/clearcache', 1, 'Y', 1, 2),
(11, '', 'Update Database', 'home/updatedb', 1, 'Y', 1, 3),
(100, 'fa-file-text', 'Manage Banners', 'banners', 0, 'Y', 1, 1),
(101, '', 'Banners', 'banners/overview', 100, 'Y', 1, 1),
(102, '', 'Add Banners', 'banners/add', 100, 'Y', 1, 2),
(103, 'fa-file-text', 'Manage Pages', 'pages', 0, 'Y', 1, 2),
(104, '', 'Pages Submenu', 'pages', 103, 'Y', 0, 1),
(105, '', 'Pages', 'pages/overview', 103, 'Y', 1, 1),
(106, '', 'Add Page', 'pages/add', 103, 'Y', 1, 2),
(107, 'fa-file-text', 'Manage Products', 'products', 0, 'Y', 1, 2),
(108, '', 'Products', 'products/overview', 107, 'Y', 1, 1),
(109, '', 'Add Products', 'products/add', 107, 'Y', 1, 2),
(110, 'fa fa-cogs', 'Manage Services', 'services', 0, 'Y', 1, 4),
(111, '', 'Services', 'services/overview', 110, 'Y', 1, 1),
(112, '', 'Add Services', 'services/add', 110, 'Y', 1, 2),
(113, '', 'Config', 'configuration', 1, 'Y', 1, 4),
(200, 'fa fa-user', 'Manage clients', 'clients', 0, 'Y', 1, 1),
(201, '', 'Clients Submenu', 'clients', 200, 'Y', 0, 1),
(202, '', 'Clients', 'clients/overview', 200, 'Y', 1, 1),
(204, '', 'Add Clients', 'clients/add', 200, 'Y', 1, 2),
(205, 'fa fa-newspaper-o', 'Manage News Letter', 'Newsletter', 0, 'Y', 1, 1),
(206, '', 'News Letter', 'newsletter/overview', 205, 'Y', 1, 1),
(207, 'fa fa-file-text', 'Request Demo', 'Requestdemo', 0, 'Y', 1, 1),
(208, '', 'Request Demo', 'requestdemo/overview', 207, 'Y', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `admin_menu_permission`
--

CREATE TABLE `admin_menu_permission` (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `admin_reset`
--

CREATE TABLE `admin_reset` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_key` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `admin_roles`
--

CREATE TABLE `admin_roles` (
  `rid` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_roles`
--

INSERT INTO `admin_roles` (`rid`, `name`, `status`) VALUES
(1, 'Super Admin', 1);

-- --------------------------------------------------------

--
-- Table structure for table `admin_widgets`
--

CREATE TABLE `admin_widgets` (
  `wid` int(11) NOT NULL,
  `widget_name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `widget_key` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `admin_widgets_permission`
--

CREATE TABLE `admin_widgets_permission` (
  `wid` int(11) NOT NULL,
  `rid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `banner_link` varchar(255) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `image` varchar(300) NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `slug` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `title`, `banner_link`, `caption`, `image`, `sort_order`, `slug`, `status`) VALUES
(34, 'Point of Sale', 'home', 'orem ipsum dolor sit amet, consect ultrices ', 'slider-01.jpg', 0, 'point_of_sale190520010112', 1);

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `title`, `image`, `sort_order`, `status`) VALUES
(1, '1', 'client_icon_01.png', 0, 1),
(2, '2', 'client_icon_02.png', 0, 1),
(3, '3', 'client_icon_03.png', 0, 1),
(4, '4', 'client_icon_04.png', 0, 1),
(5, '5', 'client_icon_05.png', 0, 1),
(6, '6', 'client_icon_06.png', 0, 1),
(7, '7', 'client_icon_07.png', 0, 1),
(8, '8', 'client_icon_08.png', 0, 1),
(9, '9', 'client_icon_09.png', 0, 1),
(10, '10', 'client_icon_10.png', 0, 1),
(11, '11', 'client_icon_11.png', 0, 1),
(12, '12', 'client_icon_12.png', 0, 1),
(13, '13', 'client_icon_13.png', 0, 1),
(14, '14', 'client_icon_14.png', 0, 1),
(15, '15', 'client_icon_151.png', 0, 1),
(16, '16', 'client_icon_15.png', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `configuration`
--

CREATE TABLE `configuration` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `short_desc` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `config_key` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `configuration`
--

INSERT INTO `configuration` (`id`, `title`, `short_desc`, `description`, `config_key`, `status`) VALUES
(1, 'Our Products', 'Lorem ipsum dolor sit amet, consect ultrices iaculisivamus', '', 'home_product_key', 1),
(2, 'Services', 'Lorem ipsum dolor sit amet, consect ultrices iaculisivamus susci', '', 'home_services_key', 1),
(3, 'Our Clients', 'Lorem ipsum dolor sit amet, consect ultrices iaculisivamus susci', '', 'home_client_key', 1),
(4, 'Why itrade ?', 'Lorem ipsum dolor sit amet, consect ultrices iaculisivamus susci', '', 'home_why_itrade_key', 1);

-- --------------------------------------------------------

--
-- Table structure for table `database_updates`
--

CREATE TABLE `database_updates` (
  `update_id` int(11) NOT NULL,
  `update_name` varchar(255) NOT NULL,
  `update_status` varchar(255) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `database_updates`
--

INSERT INTO `database_updates` (`update_id`, `update_name`, `update_status`, `update_time`) VALUES
(1, '201905141429-pages-create.txt', '1', '2019-05-14 09:10:58'),
(2, '201905141427-banner-create.txt', '1', '2019-05-14 09:11:00'),
(3, '201905141434-admin-banner.txt', '1', '2019-05-14 09:11:00'),
(4, '201905141438-admin-pages.txt', '1', '2019-05-14 09:11:01'),
(5, '201905151206-products-create.txt', '1', '2019-05-15 06:38:55'),
(6, '201905151249-adminmenu-products.txt', '1', '2019-05-15 07:20:05'),
(10, '201905151550-adminmenu-services.txt', '1', '2019-05-15 10:35:51'),
(11, '201905151509-services-create.txt', '1', '2019-05-15 10:35:53'),
(12, '201905201155-client-adminmenu.txt', '1', '2019-05-20 11:23:22'),
(13, '201905201144-client-create.txt', '1', '2019-05-20 11:23:28'),
(16, '201905211135-adminmenu-config.txt', '1', '2019-05-21 06:08:22'),
(17, '201905211101-configuration-create.txt', '1', '2019-05-21 06:08:24'),
(18, '201905211210-configuration-insert.txt', '1', '2019-05-21 06:45:49'),
(19, '201905211221-request_demo-create.txt', '1', '2019-05-21 06:52:13'),
(20, '201905201040-client-alter.txt', '1', '2019-05-21 11:25:37'),
(21, '201905211244-newsletter.txt', '1', '2019-05-21 11:27:32'),
(22, '201905210323-requestdemo-adminmenu.txt', '1', '2019-05-21 11:27:32'),
(23, '201905211237-admin-menu-newsletter.txt', '1', '2019-05-21 11:27:32'),
(24, '201905221038-pages-alter.txt', '1', '2019-05-22 05:08:38');

-- --------------------------------------------------------

--
-- Table structure for table `newsletter`
--

CREATE TABLE `newsletter` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `newsletter`
--

INSERT INTO `newsletter` (`id`, `email`, `status`) VALUES
(1, 'sunilact3@gmail.com', 1),
(2, 'sunilact3@gmail.com', 1),
(3, 'sunilact3@gmail.com', 1),
(4, 'sunilact3@gmail.com', 1),
(5, 'sunilact3@gmail.com', 1),
(6, 'sunilact3@gmail.com', 1),
(7, 'sunilact3@gmail.com', 1),
(8, 'sunilact3@gmail.com', 1),
(9, 'sunilact3@gmail.com', 1),
(10, 'faslukd@gmail.com', 1),
(11, 'sunilact3@gmail.com', 1),
(12, 'sunilact3@gmail.com', 1),
(13, 'sunilact3@gmail.com', 1),
(14, 'sunilact3@gmail.com', 1),
(15, 'sunilact3@gmail.com', 1),
(16, 'sunilact3@gmail.com', 1),
(17, 'sunilact3@gmail.com', 1),
(18, 'sunilact3@gmail.com', 1),
(19, 'sunilact3@gmail.com', 1),
(20, 'sunilact3@gmail.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `page_key` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `meta_desc` varchar(255) NOT NULL,
  `short_desc` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `meta_title`, `page_key`, `meta_keywords`, `meta_desc`, `short_desc`, `body`, `image`, `status`) VALUES
(9, 'About Us', '1', 'ABOUT_KEY', '1', '1', 'The leading Customized ERP solution provider for retail and trading industry in the middle east.', '<div class=\"row inner_about_cnt\">\r\n	<div class=\"col-sm-6\">\r\n		<h4>\r\n			iTrade ERP Solutions is the most sophisticated, powerful and user-friendly software with advance features and supports different types of trading and retail environment which can also be further customized to your business operations and needs.</h4>\r\n		<p>\r\n			iTrade ERP Solutions FZCO is one of the leading Customized ERP solution provider for retail and trading industry in the middle east. We are located in Dubai Silicon Oasis and have operations in UAE, Qatar and Oman in the middle-east and having installations of iTrade software in almost all of the GCC countries. The company was established a decade ago to provide well organized, dynamic and robust Software solution to various kinds of business.</p>\r\n	</div>\r\n	<div class=\"col-sm-6\">\r\n		<p>\r\n			Since there has been a major breakthrough in the Technology and economic situation in the Middle East, there was a tremendous need for Robust Software solutions which can be customized as per the operations of the business nature. In this context, the employers of these countries are now switching their focus to adopt a system where they can manage all the day to day procedures through a single ERP system and monitor all the activities of the company. So to guarantee appropriate selection and convenient deployment of the Software system, major companies generally select an organization that are considerably, professionally experienced and thoroughly familiarized with developing Customized Software solutions in co-relation with swiftness and efficiency.</p>\r\n	</div>\r\n</div>\r\n', 'about_banner.jpg', 1),
(10, 'our products', 'the leading Customized ERP solution provider for retail and trading industry in the middle east.', 'PRODUCT_KEY', 'the leading Customized ERP solution provider for retail and trading industry in the middle east.', 'the leading Customized ERP solution provider for retail and trading industry in the middle east.', 'the leading Customized ERP solution provider for retail and trading industry in the middle east.', '<p>\r\n	the leading Customized ERP solution provider for retail and trading industry in the middle east.</p>\r\n', 'product_banner.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `slug` varchar(250) NOT NULL,
  `title` varchar(250) NOT NULL,
  `short_desc` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_desc` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `slug`, `title`, `short_desc`, `description`, `meta_title`, `meta_desc`, `meta_keywords`, `icon`, `sort_order`, `status`) VALUES
(11, 'itrade_retail_erp_solution', 'iTrade Retail ERP Solution', 'iTrade Retail ERP Solution is powered with Touch and Non-Touch Screen based POS Module and is fully integrated with accounting.', '<p>\r\n	iTrade Retail ERP Solution is powered with Touch and Non-Touch Screen based POS Module and is fully integrated with accounting, inventory tracking &amp; management, Document Management, Budgeting forecasting of Sales and Purchase, Vendor management, operation reporting and payroll modules.User access can be controlled by user rights and the users are presented with a screen that shows only the functions that they can access; this can be easily controlled and updated by the admin.iTrade Retail ERP Solution consists of different modules, integrated together for better performance and simplifying the operations with a single point of data entry throughout the entire modules.</p>\r\n', 'ERP', 'iTrade Retail ERP desc', 'iTrade Retail ERP', 'pro-icon-01.png', 0, 1),
(12, 'itrade_accounts_and_inventory_module_for_retail', 'iTrade Accounts & Inventory Module for Retail', 'iTrade Accounts and Inventory Module helps to ensure the financial integrity in the company reporting by integrating the transactions in inventory system.', '<p>\r\n	iTrade Accounts and Inventory Module helps to ensure the financial integrity in the company reporting by integrating the transactions in inventory system with back-office chart of accounts and delivers the proven, comprehensive financial management capabilities required to grow a changing, complex business. This integration provides a competitive edge with abilities to plan effectively and to streamline the operations across entire organization by delivering with the real-time visibility required to make better, faster decisions.</p>\r\n', 'iTrade Accounts and Inventory Module', 'iTrade Accounts and Inventory Module', 'iTrade Accounts and Inventory Module', 'pro-icon-02.png', 0, 1),
(13, 'itrade_point_of_sale_module', 'iTrade Point of Sale Module', 'iTrade Retail POS Module provides the most intuitive user friendly interface and maintains a high level of performance and scalability ensuring a positive customer experience at the point of sale.', '<p>\r\n	iTrade Retail POS Module provides the most intuitive user friendly interface and maintains a high level of performance and scalability ensuring a positive customer experience at the point of sale / check-out point which add value to a company by enabling it to differentiate itself from competitors who do not offer the same experience. The most important factor in a retail business is to get the customer out with the product smoothly and efficiently from Check-out counter as quickly as possible without making them to wait due to the system error arising from server / network failure.iTrade Retail POS Module is designed in a way that all of the daily activities and transactions would automatically imported into accounting without any labour on the user&#39;s end.</p>\r\n', 'iTrade Retail POS Module', 'iTrade Retail POS Module', 'iTrade Retail POS Module', 'pro-icon-03.png', 0, 1),
(14, 'itrade_trading_erp_solution', 'iTrade Trading ERP Solution', 'iTrade Trading ERP Solution is powered with advance features & user friendly interface and is fully integrated with accounting.', '<p>\r\n	iTrade Trading ERP Solution is powered with advance features &amp; user friendly interface and is fully integrated with accounting, inventory tracking management, Enquiry and Quotation management, Van Sales, Document Management, Budgeting forecasting of Sales and Purchase, operation reporting and payroll modules.User access can be controlled by user rights and the users are presented with a screen that shows only the functions that they can access; this can be easily controlled and updated by the admin.iTrade Trading ERP Solution consists of different modules, integrated together for better performance and simplifying the operations with a single point of data entry throughout the entire modules.</p>\r\n', 'iTrade Trading ERP Solution', 'iTrade Trading ERP Solution', 'iTrade Trading ERP Solution', 'pro-icon-04.png', 0, 1),
(15, 'mobile_application', 'Mobile application', 'Business intelligence dashboard for Android and IOS Devices.', '<p>\r\n	Business intelligence dashboard for Android and IOS Devices.</p>\r\n', 'Mobile application', 'Mobile application', 'Mobile application', 'pro-icon-05.png', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `request_demo`
--

CREATE TABLE `request_demo` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request_demo`
--

INSERT INTO `request_demo` (`id`, `name`, `email`, `phone`, `status`) VALUES
(1, 'Sunil cleaner2', 'sunilact3@gmail.com', '9061448855', 0),
(2, 'Sunil cleaner2', 'sunilact3@gmail.com', '9061448855', 0),
(3, 'Fajaru nasik', 'faslukd@gmail.com', '9895469669', 0),
(4, 'Sunil cleaner2', 'faslukd@gmail.com', '9061448855', 0),
(5, 'Abc cleaner2', 'saleel@celiums.com', '8555447774', 0),
(6, 'itradewebsite', 'cleaner2@hh.com', '9061448855', 0),
(7, 'dsds', 'cleaner1@gg.com', '4343', 0),
(8, 'Sunil cleaner2', 'faslukd@gmail.com', '9895469669', 0),
(9, 'superadmin', 'cleaner3@gmail.com', '4343', 0),
(10, 'dsds', 'faslukd@gmail.com', '9061448855', 0),
(11, 'Sunil Cleaner', 'faslukd@gmail.com', '9061448855', 1),
(12, 'ss', 'saleel@celiums.com', '3232', 1),
(13, 'dsds', 'saleel@celiums.com', '9061448855', 1),
(14, 'Sunil cleaner2', 'sunilact3@gmail.com', '4343', 1);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `short_desc` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(250) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `meta_desc` varchar(255) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `slug`, `title`, `short_desc`, `description`, `image`, `meta_title`, `meta_keywords`, `meta_desc`, `sort_order`, `status`) VALUES
(11, 'enquiry', 'Enquiry Management', 'Enquiry Management', '<p>\r\n	Enquiry Management</p>\r\n', 'srv_img_03.jpg', 'Enquiry Management', 'Enquiry Management', 'Enquiry Management', 1, 1),
(13, 'customized', 'Customized Software Solution', 'Customized Software Solution', '<p>\r\n	Customized Software Solution</p>\r\n', 'srv_img_01.jpg', 'Customized Software Solution', 'Customized Software Solution', 'Customized Software Solution', 0, 1),
(14, 'accounts', 'Accounts, Inventory & Warehouse Management System', 'Accounts, Inventory & Warehouse Management System', '<p>\r\n	Accounts, Inventory &amp; Warehouse Management System</p>\r\n', 'srv_img_02.jpg', 'Accounts, Inventory & Warehouse Management System', 'Accounts, Inventory & Warehouse Management System', 'Accounts, Inventory & Warehouse Management System', 0, 1),
(15, 'school_management_system', 'School Management System', 'School Management System', '<p>\r\n	School Management System</p>\r\n', 'srv_img_04.jpg', 'School Management System', 'School Management System', 'School Management System', 0, 1),
(16, 'production_and_manufacturing_system', 'Production and Manufacturing system', 'Production and Manufacturing system', '<p>\r\n	Production and Manufacturing system</p>\r\n', 'srv_img_05.jpg', 'Production and Manufacturing system', 'Production and Manufacturing system', 'Production and Manufacturing system', 0, 1),
(17, 'document_management_system', 'Document Management System', 'Document Management System', '<p>\r\n	Document Management System</p>\r\n', 'srv_img_06.jpg', 'Document Management System', 'Document Management System', 'Document Management System', 0, 1),
(18, 'point_of_sale_solution_for_retail', 'Point of Sale Solution for retail', 'Point of Sale Solution for retail', '<p>\r\n	Point of Sale Solution for retailPoint of Sale Solution for retail</p>\r\n', 'srv_img_07.jpg', 'Point of Sale Solution for retail', 'Point of Sale Solution for retail', 'Point of Sale Solution for retail', 0, 1),
(19, 'point_of_sale_solution_for_retail190520033422', 'Point of Sale Solution for retail', 'Point of Sale Solution for retail', '<p>\r\n	Point of Sale Solution for retail Point of Sale Solution for retail</p>\r\n', 'srv_img_08.jpg', 'Point of Sale Solution for retail', 'Point of Sale Solution for retail', 'Point of Sale Solution for retail', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `title` varchar(300) NOT NULL,
  `settingkey` varchar(300) NOT NULL,
  `settingtype` varchar(100) NOT NULL,
  `status` enum('Y','N') NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `title`, `settingkey`, `settingtype`, `status`) VALUES
(1, 'From Name', 'FROM_NAME', 'text', 'Y'),
(2, 'From Mail Address', 'FROM_MAIL', 'text', 'Y'),
(3, 'Registration Notification Email', 'REG_NOTIFY_EMAIL', 'text', 'Y'),
(4, 'Meta Title', 'DEFAULT_META_TITLE', 'text', 'Y'),
(5, 'Meta Desc', 'DEFAULT_META_DESCRIPTION', 'text', 'Y'),
(6, 'Meta Keywords', 'DEFAULT_META_KEYWORDS', 'text', 'Y'),
(7, 'Facebook', 'FACEBOOK_LINK', 'text', 'Y'),
(8, 'Twitter', 'TWITTER_LINK', 'text', 'Y'),
(9, 'Linked in', 'LINKED_IN', 'text', 'Y'),
(10, 'Instagram', 'INSTA_LINK', 'text', 'Y'),
(11, 'Youtube', 'YOUTUBE_LINK', 'text', 'Y'),
(12, 'Admin Email', 'ADMIN_EMAIL', 'text', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `settings_desc`
--

CREATE TABLE `settings_desc` (
  `desc_id` int(11) NOT NULL,
  `settings_id` int(11) NOT NULL,
  `settingvalue` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings_desc`
--

INSERT INTO `settings_desc` (`desc_id`, `settings_id`, `settingvalue`) VALUES
(1, 1, 'ITrade'),
(2, 2, 'support@itrade.com'),
(3, 3, 'support@itrade'),
(4, 4, 'meta title'),
(5, 5, 'meta desc'),
(6, 6, 'meta keywords'),
(7, 7, 'https://www.facebook.com/'),
(8, 8, 'https://twitter.com/'),
(9, 9, 'https://www.linkedin.com/'),
(10, 10, 'https://instagram.com'),
(11, 11, 'https://youtube.com'),
(12, 12, 'sunilact3@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `admins_login_history`
--
ALTER TABLE `admins_login_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_function`
--
ALTER TABLE `admin_function`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_menu`
--
ALTER TABLE `admin_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_reset`
--
ALTER TABLE `admin_reset`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_roles`
--
ALTER TABLE `admin_roles`
  ADD PRIMARY KEY (`rid`);

--
-- Indexes for table `admin_widgets`
--
ALTER TABLE `admin_widgets`
  ADD PRIMARY KEY (`wid`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `configuration`
--
ALTER TABLE `configuration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `database_updates`
--
ALTER TABLE `database_updates`
  ADD PRIMARY KEY (`update_id`);

--
-- Indexes for table `newsletter`
--
ALTER TABLE `newsletter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_demo`
--
ALTER TABLE `request_demo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings_desc`
--
ALTER TABLE `settings_desc`
  ADD PRIMARY KEY (`desc_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `admins_login_history`
--
ALTER TABLE `admins_login_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `admin_function`
--
ALTER TABLE `admin_function`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `admin_menu`
--
ALTER TABLE `admin_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=209;
--
-- AUTO_INCREMENT for table `admin_reset`
--
ALTER TABLE `admin_reset`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `admin_roles`
--
ALTER TABLE `admin_roles`
  MODIFY `rid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `admin_widgets`
--
ALTER TABLE `admin_widgets`
  MODIFY `wid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `configuration`
--
ALTER TABLE `configuration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `database_updates`
--
ALTER TABLE `database_updates`
  MODIFY `update_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `newsletter`
--
ALTER TABLE `newsletter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `request_demo`
--
ALTER TABLE `request_demo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `settings_desc`
--
ALTER TABLE `settings_desc`
  MODIFY `desc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
