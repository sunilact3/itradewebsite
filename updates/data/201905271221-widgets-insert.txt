INSERT INTO `admin_widgets` (`wid`, `widget_name`, `description`, `widget_key`) VALUES
(1, 'New Requests', 'Displays the requests for Demos', 'new_request_lists'),
(2, 'New Enquiry', 'Displays Enquiries from contact us pages', 'new_enquiry_lists');