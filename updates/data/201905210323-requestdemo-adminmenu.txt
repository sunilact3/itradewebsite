INSERT INTO `admin_menu` (`id`, `class`, `name`, `link`, `parent_id`, `status`, `display`, `sort_order`) VALUES ('207', 'fa fa-file-text', 'Request Demo', 'Requestdemo', '0', 'Y', '1', '1');

INSERT INTO `admin_menu` (`id`, `class`, `name`, `link`, `parent_id`, `status`, `display`, `sort_order`) VALUES ('208', '', 'Request Demo', 'requestdemo/overview', '207', 'Y', '1', '1');

ALTER TABLE `request_demo` CHANGE `status` `status` TINYINT(1) NOT NULL DEFAULT '1';

