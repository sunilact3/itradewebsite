function launchIntoFullscreen(element) {
    if(element.requestFullscreen) {
        element.requestFullscreen();
    } else if(element.mozRequestFullScreen) {
        element.mozRequestFullScreen();
    } else if(element.webkitRequestFullscreen) {
        element.webkitRequestFullscreen();
    } else if(element.msRequestFullscreen) {
        element.msRequestFullscreen();
    }
}
function exitFullscreen() {
    if(document.exitFullscreen) {
        document.exitFullscreen();
    } else if(document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
    } else if(document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
    }
}

function set_sidemenu(){
    var windowHeight = $( window ).height();
    if( windowHeight > 900) {
        if(!$('.left-menu-block').hasClass('menu_fixed')) {
            $('.left-menu-block').addClass('menu_fixed');
        }
    } else {
        if($('.left-menu-block').hasClass('menu_fixed')) {
            $('.left-menu-block').removeClass('menu_fixed');
        }
    }
}

$(document).ready(function(){
    set_sidemenu();
    $('.fullscreen-button').click(function(){
        if( window.innerHeight == screen.height) {
            $(this).children('.glyphicon').removeClass('glyphicon-resize-small');
            $(this).children('.glyphicon').addClass('glyphicon-fullscreen');
            exitFullscreen();
        } else{
            $(this).children('.glyphicon').removeClass('glyphicon-fullscreen');
            $(this).children('.glyphicon').addClass('glyphicon-resize-small');
            launchIntoFullscreen(document.documentElement);
        }
    });
	
    $('.confirmDelete').on('click', function (e) {
        e.preventDefault();
        var url = $(this).attr('href');
        $('#confirm-delete')
            .modal({ backdrop: 'static', keyboard: false })
            .one('click', '#delete', function (e) {
                window.location.href = url;
            });
    });
	
	 $('.confirmApprove').on('click', function (e) {
        e.preventDefault();
        var url = $(this).attr('href');
        $('#confirm-approve')
            .modal({ backdrop: 'static', keyboard: false })
            .one('click', '#approve', function (e) {
                window.location.href = url;
            });
    });
    
     $('.confirmScheduleApprove').on('click', function (e) {
        e.preventDefault();
        var url = $(this).attr('href');
        $('#confirm-schedule-approve')
            .modal({ backdrop: 'static', keyboard: false })
            .one('click', '#schedule-approve', function (e) {
                window.location.href = url;
            });
    });

    $('.confirmReject').on('click', function (e) {
        e.preventDefault();
        var url = $(this).attr('href');
        $('#confirm-reject')
            .modal({ backdrop: 'static', keyboard: false })
            .one('click', '#reject', function (e) {
                window.location.href = url;
            });
    });
	
	$('.confirmStart').on('click', function (e) {
        e.preventDefault();
        var url = $(this).attr('href');
        $('#confirm-start')
            .modal({ backdrop: 'static', keyboard: false })
            .one('click', '#start', function (e) {
                window.location.href = url;
            });
    });
	
	$('.confirmStop').on('click', function (e) {
        e.preventDefault();
        var url = $(this).attr('href');
        $('#confirm-stop')
            .modal({ backdrop: 'static', keyboard: false })
            .one('click', '#stop', function (e) {
               $('#schedule-stop-form').submit();
            });
    });
	
	$('.confirmCancel').on('click', function (e) {
        e.preventDefault();
  
        $('#confirm-cancel')
            .modal({ backdrop: 'static', keyboard: false })
            .one('click', '#cancel', function (e) {
               $('#cancel-form').submit();
    	});
	});
	
	$('.confirmClose').on('click', function (e) {
        e.preventDefault();
  
        $('#confirm-close')
            .modal({ backdrop: 'static', keyboard: false })
            .one('click', '#close', function (e) {
               $('#close-form').submit();
    	});
	});
	
	$('.confirmReschedule').on('click', function (e) {
        e.preventDefault();
 
        $('#confirm-reschedule')
            .modal({ backdrop: 'static', keyboard: false })
            .one('click', '#reschedule', function (e) {
               $('#schedule-form').submit( );
            });
    });
    
    $('.confirmContract').on('click', function (e) {
        e.preventDefault();
  
        $('#confirm-contract')
            .modal({ backdrop: 'static', keyboard: false })
            .one('click', '#contract', function (e) {
               $('#contract-form').submit();
    	});
	});
	
});
$(window).resize(function(){
    set_sidemenu();
});