<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('admin_url'))
{
	function admin_url($url,$protocol = NULL){
        $url = get_instance()->config->item('adminfolder').'/'.$url;
        return site_url($url,$protocol);
    }
}

if ( ! function_exists('admin_url_string'))
{
	function admin_url_string($url){
        return get_instance()->config->item('adminfolder').'/'.$url;
    }
}

if ( ! function_exists('public_admin_base_url'))
{
	function public_admin_base_url($url,$protocol = NULL){
        $url = 'public/'.get_instance()->config->item('adminfolder').'/'.$url;
        return base_url($url,$protocol);
    }
}

if ( ! function_exists('frontend_url_string'))
{
	function frontend_url_string($url){
        return get_instance()->config->item('frontendfolder').'/'.$url;
    }
}

if ( ! function_exists('public_frontend_base_url'))
{
	function public_frontend_base_url($url,$protocol = NULL){
        $url = 'public/'.get_instance()->config->item('frontendfolder').'/'.$url;
        return base_url($url,$protocol);
    }
}

if ( ! function_exists('frontend_model_path'))
{
	function frontend_model_path($name){
		//$frontend_model_path = get_instance()->config->item('frontendfolder');
		$frontend_model_path = 'frontend';
        return $frontend_model_path.'/'.$name;
    }
}


function sanitizeStringForUrl($string)
    {
    $string = trim(strtolower($string));
    $string = html_entity_decode($string);
    $string = str_replace(array('ä','ü','ö','ß'),array('ae','ue','oe','ss'),$string);
    $string = preg_replace('#[\s]{2,}#',' ',$string);	
    $string = str_replace(array(' '),array('-'),$string);
    return $string;
    }