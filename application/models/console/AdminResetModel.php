<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class AdminResetModel extends CS_Model {

    function __construct() {
        parent::__construct();
        $this->table_name = 'admin_reset';
        $this->primary_key = 'id';
    }


    function exists($cond) {
        $this->db->where($cond);
        $query = $this->db->get($this->table_name);
        $result = $query->num_rows();
        if ($result > 0) {
            return true;
        } else {
            return false;
        }
    }

}
