<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class AdminRolesModel extends CS_Model {

    function __construct() {
        parent::__construct();
        $this->table_name = 'admin_roles';
        $this->primary_key = 'rid';
    }

    function getRolesArray(){
        $reqcats = array();
        $this->db->select('*');
        $this->db->from($this->table_name);
        $query = $this->db->get();
        $results = $query->result_array();
        foreach($results as $result):
        $reqcats[$result['rid']] = $result['name'];
        endforeach;
        return $reqcats;
    }
    function check_permission($function_link,$rid){
        $this->db->select('*');
        $this->db->from('admin_function');
        $cond = array('admin_function.function_link' => $function_link);
        $this->db->where($cond);
        $function_query = $this->db->get();
        $function_result = $function_query->num_rows();
        if ($function_result > 0) {
            $this->db->select('*');
            $this->db->from('admin_function');
            $this->db->join('admin_function_permission', 'admin_function.id = admin_function_permission.function_id');
            $cond = array('admin_function.function_link' => $function_link,'admin_function_permission.role_id'=>$rid);
            $this->db->where($cond);
            $query = $this->db->get();
            $result = $query->num_rows();
            if ($result > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
    function check_reqpermission($request,$rid){
        $this->db->select('*');
        $this->db->from('admin_status');
        $cond = array('admin_status.status_key' => $request->status);
        $this->db->where($cond);
        $function_query = $this->db->get();
        $function_result = $function_query->num_rows();
        if ($function_result > 0) {
            $this->db->select('*');
            $this->db->from('admin_status');
            $this->db->join('admin_status_permission', 'admin_status.sid = admin_status_permission.sid');
            $cond = array('admin_status.status_key' => $request->status,'admin_status_permission.rid'=>$rid);
            $this->db->where($cond);
            $query = $this->db->get();
            $result = $query->num_rows();
            if ($result > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }


    function updatemenu($data,$rid){
        $this->db->delete('admin_menu_permission', array('role_id'=>$rid));
        return $this->db->insert_batch('admin_menu_permission', $data);
    }

    function updatepermission($data,$rid){
        $this->db->delete('admin_function_permission', array('role_id'=>$rid));
        return $this->db->insert_batch('admin_function_permission', $data);
    }

    function updatewidgets($data,$rid){
        $this->db->delete('admin_widgets_permission', array('rid'=>$rid));
        return $this->db->insert_batch('admin_widgets_permission', $data);
    }
    function updatestatuses($data,$rid){
        $this->db->delete('admin_status_permission', array('rid'=>$rid));
        return $this->db->insert_batch('admin_status_permission', $data);
    }

    function get_menu($rid){
        $this->db->where('role_id', $rid);
        $query = $this->db->get('admin_menu_permission');
        $results = $query->result_array();
        $menus = array();
        foreach($results as $result):
            $menus[] = $result['menu_id'];
        endforeach;
        return $menus;
    }

    function get_function($rid){
        $this->db->where('role_id', $rid);
        $query = $this->db->get('admin_function_permission');
        $results = $query->result_array();
        $functions = array();
        foreach($results as $result):
            $functions[] = $result['function_id'];
        endforeach;
        return $functions;
    }
    function get_widgets($rid){
        $this->db->where('rid', $rid);
        $query = $this->db->get('admin_widgets_permission');
        $results = $query->result_array();
        $functions = array();
        foreach($results as $result):
            $functions[] = $result['wid'];
        endforeach;
        return $functions;
    }
    function get_statuses($rid){
        $this->db->where('rid', $rid);
        $query = $this->db->get('admin_status_permission');
        $results = $query->result_array();
        $functions = array();
        foreach($results as $result):
            $functions[] = $result['sid'];
        endforeach;
        return $functions;
    }
    function getWidgets($rid){
        if($this->session->userdata('admin_user_role')!='1'){
            $this->db->select('*');
            $this->db->from('admin_widgets');
            $this->db->join('admin_widgets_permission', 'admin_widgets.wid = admin_widgets_permission.wid');
            $this->db->where('rid', $rid);
        } else {
            $this->db->select('*');
            $this->db->from('admin_widgets');
        }
        $query = $this->db->get();
        $results = $query->result_array();

        $functions = array();
        foreach($results as $result):
            $functions[] = $result['widget_key'];
        endforeach;
        return $functions;
    }

}
