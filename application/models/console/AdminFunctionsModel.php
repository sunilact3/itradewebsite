<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class AdminFunctionsModel extends CS_Model {

    function __construct() {
        parent::__construct();
        $this->table_name = 'admin_function';
        $this->primary_key = 'id';
    }

    function get_all() {
        $functions = array();
        $this->db->where('parent_id', '0');
        $this->db->order_by('function_name', 'ASC');
        $query = $this->db->get($this->table_name);
        $main_menus = $query->result_array();
        foreach ($main_menus as $main_menu):
            $this->db->where('parent_id', $main_menu['id']);
            $this->db->order_by('function_name', 'ASC');
            $query = $this->db->get($this->table_name);
            $sub_menus = $query->result_array();
            $functions[] = array(
                'id' => $main_menu['id'],
                'function_name' => $main_menu['function_name'],
                'description' => $main_menu['description'],
                'function_link' => $main_menu['function_link'],
                'sub_menu' => $sub_menus
            );
        endforeach;
        return $functions;
    }

}
