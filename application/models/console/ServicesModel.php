<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ServicesModel extends CS_Model {

    function __construct() {
        parent::__construct();
        $this->table_name = 'services';
        $this->primary_key = 'id';
    }

    function get_pagination($num, $offset, $cond = '') {
        $this->db->select('*');
        if (is_array($cond) && count($cond) > 0) {
            $this->db->where($cond);
        }
        $this->db->from($this->table_name);
        $this->db->order_by('sort_order','asc');
        $this->db->limit($num, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }
    function get_array_cond_limit($cond, $limit) {
		if (is_array($cond) && count($cond) > 0) {
		$this->db->where($cond);
		}
		$this->db->from($this->table_name);
        $this->db->order_by('sort_order', 'asc');
        $this->db->limit($limit);
		$query = $this->db->get();
		return $query->result_array();
	}    
}