<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ConfigurationModel extends CS_Model {

    function __construct() {
        parent::__construct();
        $this->table_name = 'configuration';
        $this->primary_key = 'id';
    }

}
