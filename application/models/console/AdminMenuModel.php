<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class AdminMenuModel extends CS_Model {

    function __construct() {
        parent::__construct();
        $this->table_name = 'admin_menu';
        $this->primary_key = 'id';
    }
    function get_menu() {
        if($this->session->userdata('admin_user_role')!='1'){
            return $this->get_usermenu();
        } else {
            return $this->get_supermenu();
        }
    }
    function get_usermenu() {
        $role_id = $this->session->userdata('admin_user_role');
        $menu = array();
        $this->db->where('admin_menu.status', 'Y');
        $this->db->where('admin_menu.parent_id', '0');
        $this->db->where('admin_menu_permission.role_id', $role_id);
        $this->db->order_by('admin_menu.sort_order', 'ASC');
        $this->db->from($this->table_name);
        $this->db->join('admin_menu_permission','admin_menu_permission.menu_id = admin_menu.id');
        $query = $this->db->get();
        $main_menus = $query->result_array();
        foreach ($main_menus as $main_menu):
            $this->db->where('admin_menu.status', 'Y');
            $this->db->where('admin_menu.parent_id', $main_menu['id']);
            $this->db->where('admin_menu_permission.role_id', $role_id);
            $this->db->order_by('admin_menu.sort_order', 'ASC');
            $this->db->from($this->table_name);
            $this->db->join('admin_menu_permission','admin_menu_permission.menu_id = admin_menu.id');
            $query = $this->db->get();
            $sub_menus = $query->result_array();
            $menu[] = array(
                'id' => $main_menu['id'],
                'name' => $main_menu['name'],
                'class' => $main_menu['class'],
                'link' => $main_menu['link'],
                'sub_menu' => $sub_menus
            );
        endforeach;
        return $menu;
    }
    function get_supermenu() {
        $menu = array();
        $this->db->where('status', 'Y');
        $this->db->where('parent_id', '0');
        $this->db->order_by('sort_order', 'ASC');
        $query = $this->db->get($this->table_name);
        $main_menus = $query->result_array();
        foreach ($main_menus as $main_menu):
            $this->db->where('status', 'Y');
            $this->db->where('parent_id', $main_menu['id']);
            $this->db->order_by('sort_order', 'ASC');
            $query = $this->db->get($this->table_name);
            $sub_menus = $query->result_array();
            $menu[] = array(
                'id' => $main_menu['id'],
                'name' => $main_menu['name'],
                'class' => $main_menu['class'],
                'link' => $main_menu['link'],
                'sub_menu' => $sub_menus
            );
        endforeach;
        return $menu;
    }
    function get_fullmenu() {
        $menu = array();
        $this->db->where('parent_id', '0');
        $this->db->order_by('sort_order', 'ASC');
        $query = $this->db->get($this->table_name);
        $main_menus = $query->result_array();
        foreach ($main_menus as $main_menu):
            $this->db->where('parent_id', $main_menu['id']);
            $this->db->order_by('sort_order', 'ASC');
            $query = $this->db->get($this->table_name);
            $sub_menus = $query->result_array();
            $menu[] = array(
                'id' => $main_menu['id'],
                'name' => $main_menu['name'],
                'class' => $main_menu['class'],
                'link' => $main_menu['link'],
                'sub_menu' => $sub_menus
            );
        endforeach;
        return $menu;
    }

}
