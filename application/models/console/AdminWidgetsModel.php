<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class AdminWidgetsModel extends CS_Model {

    function __construct() {
        parent::__construct();
        $this->table_name = 'admin_widgets';
        $this->primary_key = 'wid';
    }

    function get_all() {
        $functions = array();
        $query = $this->db->get($this->table_name);
        return $query->result_array();
    }

}
