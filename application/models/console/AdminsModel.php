<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class AdminsModel extends CS_Model {

    function __construct() {
        parent::__construct();
        $this->table_name = 'admins';
        $this->primary_key = 'uid';
    }

    function login_check($user, $pass) {
        $user = $this->db->escape_str($user);
        $cond = array('username' => $user, 'status' => '1');
        $user_row = $this->get_row_cond($cond);
        if($user_row){
            $pass = $this->db->escape_str($pass);
            $pass = sha1($pass.$user_row->salt);
            $cond = array('username' => $user, 'password' => $pass, 'status' => '1');
            $this->db->where($cond);
            $query = $this->db->get($this->table_name);
            $result = $query->num_rows();
            if ($result > 0) {
                return $query->row();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function forgot_check($user) {
            $user = $this->db->escape_str($user);
            $cond = array('username' => $user);
            $orcond = array('email' => $user);
            $this->db->where($cond);
            $this->db->or_where($orcond);
            $query = $this->db->get($this->table_name);
            $result = $query->num_rows();
            if ($result > 0) {
                return $query->row();
            } else {
                return false;
            }
        }
	function password_check($cond)
	{
		$this->db->where($cond);
		$query = $this->db->get($this->table_name);
        $result = $query->num_rows();
		if($result>0){
			return true;
		} else {
			return false;
		}
	}
}
