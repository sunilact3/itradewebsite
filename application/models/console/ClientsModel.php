<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ClientsModel extends CS_Model {

    function __construct() {
        parent::__construct();
        $this->table_name = 'clients';
        $this->primary_key = 'id';
    }
    function get_pagination_count($cond = '') {
        $this->db->select('*');
        if (is_array($cond) && count($cond) > 0) {
            $this->db->where($cond);
        }
        $this->db->from($this->table_name);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_pagination($num, $offset, $cond = '') {
        $this->db->select('*');
        if (is_array($cond) && count($cond) > 0) {
            $this->db->where($cond);
        }
        $this->db->from($this->table_name);
        $this->db->limit($num, $offset);
        $this->db->order_by("sort_order", "asc");
        $query = $this->db->get();
        return $query->result_array();
    }
    function get_array_cond_limit($cond, $limit) {
		if (is_array($cond) && count($cond) > 0) {
		$this->db->where($cond);
		}
		$this->db->from($this->table_name);
        $this->db->limit($limit);
        $this->db->order_by("sort_order", "asc");
		$query = $this->db->get();
		return $query->result_array();
	}

}
