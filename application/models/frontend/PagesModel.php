<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class PagesModel extends CS_Model {

    function __construct() {
        parent::__construct();
        $this->table_name = 'pages';
        $this->primary_key = 'id';
    }

}
