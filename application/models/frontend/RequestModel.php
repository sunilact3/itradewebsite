<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class RequestModel extends CS_Model {

    function __construct() {
        parent::__construct();
        $this->table_name = 'request_demo';
        $this->primary_key = 'id';
    }
}
