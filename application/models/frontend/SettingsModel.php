<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SettingsModel extends CS_Model {

    function __construct() {
        parent::__construct();
        $this->table_name = 'settings';
		$this->desc_table_name='settings_desc';
        $this->primary_key = 'id';
		$this->foreign_key='settings_id';
    }

	function get_array_set()
	{
		$this->db->from($this->table_name);
		$this->db->join($this->desc_table_name, "$this->desc_table_name.$this->foreign_key = $this->table_name.$this->primary_key");
		$query = $this->db->get();
        return $query->result_array();
	}

	function insert_set($maindata,$descdata)
	{
		$this->db->insert($this->table_name,$maindata);
		$prime=$this->db->insert_id();
		$rowdata=$descdata;
		$rowdata[$this->foreign_key]=$prime;
		$this->db->insert($this->desc_table_name,$rowdata);
		unset($rowdata);
		return $prime;
	}

	function update_set($maindata,$descdata,$id)
	{
		$cond[$this->primary_key]=$id;
		$desccond[$this->foreign_key]=$id;
		if(count($descdata)>0){
			$updateid = $this->db->update($this->desc_table_name,$descdata,$desccond);
		}
		if(count($maindata)>0){
			$updateid = $this->db->update($this->table_name,$maindata,$cond);
		}
		return $updateid;
	}

}
