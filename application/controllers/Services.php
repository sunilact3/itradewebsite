<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends CSFront_Controller {

	function __construct() {
		parent::__construct();
	}
	public function detail($slug='')
	{
		$this->output->cache($this->settings['CACHE_TIME']);
		$content['success']='';
		$content['newslettersuccess']='';
        $this->load->model(frontend_model_path('ProductsModel'));
        $this->load->model(frontend_model_path('ServicesModel'));
        $this->load->model(frontend_model_path('BannersModel'));
        $this->load->model(frontend_model_path('PagesModel'));
        $this->load->model(admin_url_string('ConfigurationModel'));
        $main['page_scripts']='';
        $this->pagetitle = 'WHAT WE DO';
        $page=$this->PagesModel->get_row_cond(array('page_key'=>'WHATWEDO_KEY'));
        if($page){
            if($page->meta_title!=''){ $this->pagetitle = $page->meta_title;}
            if($page->meta_keywords!=''){ $this->metakeywords = $page->meta_keywords;}  
            if($page->meta_desc!=''){ $this->metadesc = $page->meta_desc;}
        }
		$main['meta']=$this->frontmetahead();
        $content['page']=$page;
		$headerbanner=$page->image;
        $content['products']=$this->ProductsModel->get_active();
        $content['services']=$this->ServicesModel->get_active();
        $content['services_detail']=$this->ServicesModel->get_row_cond(array('slug'=>$slug));
       //$content['config_itrade']=$this->ConfigurationModel->get_row_cond(array('config_key'=>'home_why_itrade_key'));
		$content['config_request']=$this->ConfigurationModel->get_row_cond(array('config_key'=>'request'));
		$frontcontent=$this->load->view('frontend/services/detail',$content,true);
		$content['requestdemoform']=$this->load->view('frontend/request/request',$content,true);
		$content['newsletterform']=$this->load->view('frontend/newsletter/newsletterform','',true);
        $footercontent = $this->load->view('frontend/content/footerhome',$content,true);
		$main['contents']=$this->frontcontent($frontcontent,false);
		$main['header']=$this->frontheader($headerbanner);
		$main['footer']=$this->fronthomefooter($footercontent);
		$this->load->view('frontend/main',$main);
	}
	}
    
