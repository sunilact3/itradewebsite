<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Configuration extends CSAdmin_Controller {

	function __construct()
    {
		// Call the Model constructor
		parent::__construct();
		$this->load->model(admin_url_string('ConfigurationModel'));
	}

	public function index()
	{
		redirect(admin_url_string('configuration/overview'));
	}

	public function overview()
	{
		$this->mainvars['page_title']= 'Configuration';
		$this->mainvars['page_scripts']='';
		$this->load->library('pagination');
		$config = $this->paginationConfig();
		$config['base_url'] = admin_url('configuration/overview/');
		$config['uri_segment'] = 4;
		$config['total_rows'] = $this->ConfigurationModel->get_pagination_count();
		$config['per_page'] = 20;
		$this->pagination->initialize($config);
		$vars['configs'] = $this->ConfigurationModel->get_pagination($config['per_page'], $this->uri->segment($config['uri_segment']));
		$this->mainvars['content']=$this->load->view(admin_url_string('configuration/overview'),$vars,true);
		$this->load->view(admin_url_string('main'),$this->mainvars);
	}

	function add()
	{
		$this->ckeditorCall();
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('config_key', 'Config Key', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_message('required', 'required');
		$this->form_validation->set_error_delimiters('<span class="red">(', ')</span>');
		if ($this->form_validation->run() == FALSE)
		{
			$this->mainvars['page_title']='Add Config';
			$this->mainvars['page_scripts']='';
			$this->mainvars['content'] = $this->load->view(admin_url_string('configuration/add'), '', true);
			$this->load->view(admin_url_string('main'),$this->mainvars);
		} else {
			$data = array('title'=>$this->input->post('title'),
                          'config_key'=>$this->input->post('config_key'),
				          'short_desc'=>$this->input->post('short_desc'),
                          'status'=>$this->input->post('status'));
			$contentid = $this->ConfigurationModel->insert($data);
			if($contentid){
				 $this->session->set_flashdata('message', array('status'=>'alert-success','message'=>'Added Successfully.'));
				redirect(admin_url_string('configuration/overview/'));
			} else {
				$this->session->set_flashdata('message', array('status'=>'alert-danger','message'=>'Error! - Failed.'));
				redirect(admin_url_string('configuration/overview/'));
			}
		}
	}

	function edit($id)
	{
		$this->ckeditorCall();
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('config_key', 'Config Key', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_message('required', 'required');
		$this->form_validation->set_error_delimiters('<span class="red">(', ')</span>');
		if ($this->form_validation->run() == FALSE)
		{
			$this->mainvars['page_title']='Edit Configuration';
			$this->mainvars['page_scripts']='';
			$edit['config']= $this->ConfigurationModel->load($id);
			$this->mainvars['content'] = $this->load->view(admin_url_string('configuration/edit'), $edit, true);
			$this->load->view(admin_url_string('main'),$this->mainvars);
		} else {
			$data = array('title'=>$this->input->post('title'),
                          'config_key'=>$this->input->post('config_key'),
				          'short_desc'=>$this->input->post('short_desc'),
                          'status'=>$this->input->post('status'));
            
            $cond = array('id' => $id);
			$loginid=$this->ConfigurationModel->update($data,$cond);
			if($loginid){
 				$this->session->set_flashdata('message', array('status'=>'alert-success','message'=>'Updated Successfully.'));
				redirect(admin_url_string('configuration/overview/'));
			} else {
								$this->session->set_flashdata('message', array('status'=>'alert-danger','message'=>'Error! - Failed.'));
				redirect(admin_url_string('configuration/overview/'));
			}
		}
	}

	function delete($id) {
		$data = array('id'=>$id);
		$deleterow = $this->ConfigurationModel->delete($data);
		if ($deleterow) {
			$this->session->set_flashdata('message', array('status'=>'alert-success','message'=>'Deleted successfully.'));
			redirect(admin_url_string('configuration/overview'));
		} else {
			$this->session->set_flashdata('message', array('status'=>'alert-danger','message'=>'Error! - Failed.'));
			redirect(admin_url_string('configuration/overview'));
		}
	}
	}


/* End of file languages.php */
/* Location: ./application/controllers/admin/languages.php */
