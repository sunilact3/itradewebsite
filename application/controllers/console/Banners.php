<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banners extends CSAdmin_Controller {

	function __construct()
    {
		// Call the Model constructor
		parent::__construct();
		$this->load->model(admin_url_string('BannersModel'));
	}

	public function index()
	{
		redirect(admin_url_string('banners/overview'));
	}

	public function overview()
	{
		$this->mainvars['page_title']= 'Banners';
				
		$this->mainvars['page_scripts']='';
		$this->load->library('pagination');
		$config = $this->paginationConfig();
		$config['base_url'] = admin_url('banners/overview/');
		$config['uri_segment'] = 4;
		$config['total_rows'] = $this->BannersModel->get_pagination_count();
		$config['per_page'] = 30;
		$this->pagination->initialize($config);
		$vars['banners'] = $this->BannersModel->get_pagination($config['per_page'], $this->uri->segment($config['uri_segment']));
		$this->mainvars['content']=$this->load->view(admin_url_string('banners/overview'),$vars,true);
		$this->load->view(admin_url_string('main'),$this->mainvars);
	}

	function add()
	{
        if (empty($_FILES['image']['name']))
        {
            $this->form_validation->set_rules('image', 'Image', 'required');
        }
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('caption', 'Caption', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_message('required', 'required');
		$this->form_validation->set_error_delimiters('<span class="red">(', ')</span>');
		if ($this->form_validation->run() == FALSE)
		{
			$this->mainvars['page_title']='Add Banners';
			$this->mainvars['page_scripts']='';
			$this->mainvars['content'] = $this->load->view(admin_url_string('banners/add'), '', true);
			$this->load->view(admin_url_string('main'),$this->mainvars);
		} else {
            $image='';
            if (!is_dir('public/uploads/banners'))
            {
                mkdir( 'public/uploads/banners', 0777, TRUE);
            }
			$config['upload_path'] = 'public/uploads/banners';
			$config['allowed_types'] = 'jpg|jpeg|png|gif|bmp';
			$this->load->library('upload', $config);
			if($this->upload->do_upload('image'))
			{
				$imagedata=$this->upload->data();
				$image=$imagedata['file_name'];
			}
            if (!$this->upload->do_upload('image')) {
                $error = array('error' => $this->upload->display_errors());
                $this->session->set_flashdata('message', array('status'=>'alert-danger','message'=>'Error! - Failed.'.$error['error']));
                redirect(admin_url_string('banners/overview'),'refresh');
                }
			$slugtxt = str_replace('&','and', $this->input->post('title'));
			$slugtxt = str_replace(" ",'_',$slugtxt);
			$slugtxt = str_replace('%','_',$slugtxt);
			$slugtxt = str_replace('-','_',$slugtxt);
			$slugtxt = str_replace('(','_',$slugtxt);
			$slugtxt = str_replace(')','_',$slugtxt);
            $slug=$this->BannersModel->create_slug($slugtxt);
			$data = array('slug'=>$slug,
				          'title'=>$this->input->post('title'),
				          'banner_link'=>$this->input->post('banner_link'),
                          'image'=>$image,
                          'caption'=>$this->input->post('caption'),
                          'status'=>$this->input->post('status'));
			$contentid = $this->BannersModel->insert($data);
			if($contentid){
                 $this->session->set_flashdata('message', array('status'=>'alert-success','message'=>'Banner added Successfully.'));
				redirect(admin_url_string('banners/overview'));
			} else {
				$this->session->set_flashdata('message', array('status'=>'alert-danger','message'=>'Error! - Failed.'));
				redirect(admin_url_string('banners/overview'));
			}
		}
	}

	function edit($id)
	{
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('caption', 'Caption', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_message('required', 'required');
		$this->form_validation->set_error_delimiters('<span class="red">(', ')</span>');
		if ($this->form_validation->run() == FALSE)
		{
			$this->mainvars['page_title']='Edit Banners';
			$this->mainvars['page_scripts']='';
			$edit['banner']= $this->BannersModel->load($id);
			$this->mainvars['content'] = $this->load->view(admin_url_string('banners/edit'), $edit, true);
			$this->load->view(admin_url_string('main'),$this->mainvars);
		} else {
			$slugtxt = str_replace('&','and', $this->input->post('title'));
			$slugtxt = str_replace(" ",'_',$slugtxt);
			$slugtxt = str_replace('%','_',$slugtxt);
			$slugtxt = str_replace('-','_',$slugtxt);
			$slugtxt = str_replace('(','_',$slugtxt);
			$slugtxt = str_replace(')','_',$slugtxt);
			$slug=$this->BannersModel->create_slug($slugtxt);

			$data = array('slug'=>$slug,
                          'title'=>$this->input->post('title'),
				          'banner_link'=>$this->input->post('banner_link'),
                          'caption'=>$this->input->post('caption'),
                          'status'=>$this->input->post('status'));

            $config['upload_path'] = 'public/uploads/banners';
            $config['allowed_types'] = 'jpg|jpeg|png|gif|bmp';
            $this->load->library('upload', $config);
            if($this->upload->do_upload('image'))
            {
                $imagedata=$this->upload->data();
                $data['image']=$imagedata['file_name'];
            }
            $cond = array('id' => $id);
			$loginid=$this->BannersModel->update($data,$cond);
			if($loginid){
				$this->session->set_flashdata('message', array('status'=>'alert-success','message'=>'Banner Updated Successfully.'));
				redirect(admin_url_string('banners/overview/'));
			} else {
				$this->session->set_flashdata('message', array('status'=>'alert-danger','message'=>'Error! - Failed.'));
				redirect(admin_url_string('banners/overview/'));
			}
		}
	}

	function delete($id) {
			$banner_image= $this->BannersModel->load($id)->image;
			$cond=array('id'=>$id);
            $deleterow = $this->BannersModel->delete($cond);
            if ($deleterow) {

                unlink('public/uploads/banners/'.$banner_image);

                $this->session->set_flashdata('message', array('status'=>'alert-success','message'=>'Banner Deleted successfully.'));
                redirect(admin_url_string('banners/overview'));
            } else {
                $this->session->set_flashdata('message', array('status'=>'alert-danger','message'=>'Error! - Failed.'));
                redirect(admin_url_string('banners/overview'));
            }
        }


	 function actions()
	{

		$loginid=false;
		$sort_orders=$this->input->post('sort_order');
 		if(isset($_POST['sortsave'])){
			foreach($sort_orders as $id => $sort_order):
				$data=array('sort_order'=>$sort_order);
                $cond=array('id'=>$id);
				$loginid=$this->BannersModel->update($data,$cond);
				$flashmsg = 'Updated Successfully.';
			endforeach;
		}
		if($loginid){
            $this->session->set_flashdata('message', array('status'=>'alert-success','message'=>$flashmsg));

		}else{
			 $this->session->set_flashdata('message', array('status'=>'alert-danger','message'=>'Error! - Failed.'));
		}
		redirect(admin_url_string('banners/overview'));
	}

}
