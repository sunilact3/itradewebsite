<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Newsletter extends CSAdmin_Controller {

	function __construct()
    {
		// Call the Model constructor
		parent::__construct();
		$this->load->model(admin_url_string('NewslettersModel'));
	}

	public function index()
	{
		redirect(admin_url_string('newsletter/overview'));
	}

	public function overview()
	{
		$this->mainvars['page_title']= 'News Letters';
		$this->load->library('pagination');
		$config = $this->paginationConfig();
		$config['base_url'] = admin_url('newsletter/overview');
		$config['uri_segment'] = 4;
		$config['total_rows'] = $this->NewslettersModel->get_pagination_count();
		$config['per_page'] = 20;
		$this->pagination->initialize($config);
		$this->mainvars['newsletters'] = $this->NewslettersModel->get_pagination($config['per_page'], $this->uri->segment($config['uri_segment']));
		$this->mainvars['content']=$this->load->view(admin_url_string('newsletter/overview'),$this->mainvars,true);
		$this->load->view(admin_url_string('main'),$this->mainvars);
	}

}