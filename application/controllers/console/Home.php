<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CSAdmin_Controller {


	function __construct() {
		parent::__construct();
		$this->load->model(admin_url_string('AdminRolesModel'));
		$this->load->model(admin_url_string('AdminsModel'));
	}

	public function logout() {
		$newdata = array(
			'admin_user_id',
			'admin_user_name',
			'admin_user_email',
			'admin_user_role',
			'admin_user_logged_in'
		);
		$this->session->unset_userdata($newdata);
		redirect(admin_url_string('login'));
	}

	public function index()
	{
		redirect(admin_url_string('home/dashboard'));
	}

	public function dashboard()
	{
		$this->load->model(admin_url_string('RequestDemoModel'));
		$this->load->model(admin_url_string('ContactusModel'));
        
        $vars['newrequests'] = $this->RequestDemoModel->get_array_limit_cond(5,array('status'=>'1'));
		$vars['newenquiries'] = $this->ContactusModel->get_array_limit_cond(5,array('status'=>'1'));
		$vars['widgets'] = $this->AdminRolesModel->getWidgets($this->session->userdata('admin_user_role'));
		$this->mainvars['content'] = $this->load->view(admin_url_string('content/home'),$vars,true);
		$this->load->view(admin_url_string('main'),$this->mainvars);
	}

	public function accessdenied(){
		$this->mainvars['content'] = $this->load->view(admin_url_string('content/accessdenied'),'',true);
        $this->load->view(admin_url_string('main'),$this->mainvars);
	}

	public function editprofile(){
		$current_user=$this->session->userdata('admin_user_id');
		$this->form_validation->set_rules('fullname', 'Full Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|callback_email_exists');
		$this->form_validation->set_rules('phone', 'Phone', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required|alpha_dash|callback_username_exists');
		$this->form_validation->set_message('required', 'required');
		$this->form_validation->set_error_delimiters('<span class="validation-error red">(', ')</span>');
		if ($this->form_validation->run() == FALSE) {
			$vars['user'] =$this->AdminsModel->load($current_user);
			$this->mainvars['content'] = $this->load->view(admin_url_string('profiles/editprofile'), $vars, true);
			$this->load->view(admin_url_string('main'), $this->mainvars);
		} else {
			$data = array(
				'fullname' => $this->input->post('fullname'),
				'email' => $this->input->post('email'),
				'phone' => $this->input->post('phone'),
				'username' => $this->input->post('username'));
			$cond = array('uid'=>$current_user);
			$insertrow = $this->AdminsModel->update($data,$cond);
			if ($insertrow) {
				$this->session->set_flashdata('message', array('status'=>'alert-success','message'=>'Profile edited successfully.'));
				redirect(admin_url_string('home/editprofile'));
			} else {
				$this->session->set_flashdata('message', array('status'=>'alert-danger','message'=>'Error! - Failed.'));
				redirect(admin_url_string('home/editprofile'));
			}
		}
	}

	function username_exists($val) {
		$cond = array('uid !=' => $this->session->userdata('admin_user_logged_in'), 'username' => $val);
		if($this->AdminsModel->row_exists($cond)) {
			$this->form_validation->set_message('username_exists', 'Username - '. $val .' - already exists!!');
			return FALSE;
		} else {
			return TRUE;
		}
	}

	function email_exists($val) {
		$cond = array('uid !=' => $this->session->userdata('admin_user_id'), 'email' => $val);
		if($this->AdminsModel->row_exists($cond)) {
			$this->form_validation->set_message('email_exists', 'Email - '. $val .' - already exists!!');
			return FALSE;
		} else {
			return TRUE;
		}
	}
	public function changepwd(){
		$current_user=$this->session->userdata('admin_user_id');
		$this->form_validation->set_rules('passold', 'Old Password', 'required|callback_password_check');
		$this->form_validation->set_rules('password', 'Password', 'required|matches[confpassword]');
        $this->form_validation->set_rules('confpassword', 'Confirm Password', 'required');
		$this->form_validation->set_message('required', 'required');
		$this->form_validation->set_error_delimiters('<span class="validation-error red">(', ')</span>');
		if ($this->form_validation->run() == FALSE) {
			$vars['user'] =$this->AdminsModel->load($current_user);;
			$this->mainvars['content'] = $this->load->view(admin_url_string('profiles/changepwd'), $vars, true);
			$this->load->view(admin_url_string('main'), $this->mainvars);
		} else {
			$salt = $this->input->post('salt');
			$password = sha1($this->input->post('password').$salt);
			$data = array('password' => $password);
			$cond = array('uid'=>$current_user);
			$insertrow = $this->AdminsModel->update($data,$cond);
			if ($insertrow) {
				$this->session->set_flashdata('message', array('status'=>'alert-success','message'=>'User password changed successfully.'));
				redirect(admin_url_string('home/changepwd'));
			} else {
				$this->session->set_flashdata('message', array('status'=>'alert-danger','message'=>'Error! - Failed.'));
				redirect(admin_url_string('home/changepwd'));
			}
		}
	}

	function password_check($code)
	{
		$salt = $this->input->post('salt');
		$pass = $this->db->escape_str($code);
        $pass = sha1($pass.$salt);
		$cond = array( 'password' => $pass, 'uid' => $this->session->userdata('admin_user_id'));
		if (!$this->AdminsModel->password_check($cond))
		{
			$this->form_validation->set_message('password_check', 'Invalid password');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	function settings()
	{
		$this->form_validation->set_rules('settings', 'Settings', 'required');
		if ($this->form_validation->run() == FALSE) {
			$this->mainvars['page_title']=$this->config->item('site_name').' - Settings';
			$edit['settings'] = $this->SettingsModel->get_array_set();
			$this->mainvars['content']=$this->load->view(admin_url_string('settings/settings'),$edit,true);
			$this->load->view(admin_url_string('main'),$this->mainvars);
		} else {
			foreach($this->input->post('setting') as $identity => $setting):
				$maindata=array();
				$descdata=array('settingvalue'=>$setting);
				$insertrow =$this->SettingsModel->update_set($maindata,$descdata,$identity);
			endforeach;
			if ($insertrow) {
				$this->session->set_flashdata('message', array('status'=>'alert-success','message'=>'Settings updated successfully.'));
				redirect(admin_url_string('home/settings'));
			} else {
				$this->session->set_flashdata('message', array('status'=>'alert-danger','message'=>'Error! - Failed.'));
				redirect(admin_url_string('home/settings'));
			}
		}
	}


	function addsettings()
	{
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('settingkey', 'Key', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_message('required', 'required');
		$this->form_validation->set_error_delimiters('<span class="red">(', ')</span>');
		if ($this->form_validation->run() == FALSE)
		{
		$this->mainvars['page_title']=$this->config->item('site_name').' - Settings';
		$this->mainvars['content']=$this->load->view(admin_url_string('settings/addsettings'),'',true);
		$this->load->view(admin_url_string('main'),$this->mainvars);
		} else {
			$maindata = array(
				'title' => $this->input->post('title'),
				'settingkey' =>  $this->input->post('settingkey'),
				'settingtype' =>  $this->input->post('settingtype'),
				'status' => $this->input->post('status'));
			$descdata=array('settingvalue'=>$this->input->post('settingvalue'));
			$insertrow =$this->SettingsModel->insert_set($maindata,$descdata);
			if ($insertrow) {
				$this->session->set_flashdata('message', array('status'=>'alert-success','message'=>'Settings added successfully.'));
				redirect(admin_url_string('home/settings'));
			} else {
				$this->session->set_flashdata('message', array('status'=>'alert-danger','message'=>'Error! - Failed.'));
				redirect(admin_url_string('home/settings'));
			}
		}
	}


    function clearcache(){
		$this->output->clear_all_cache();
		$this->session->set_flashdata('message', array('status'=>'alert-success','message'=>'Cache cleared successfully.'));
		redirect(admin_url_string('home/dashboard'));
	}

	function updatedb(){
		$this->load->helper('directory');
		$this->load->helper('file');
		$pending_updates = array();
		$updates = directory_map('updates/data', 1);
		$this->load->model(admin_url_string('UpdatesModel'));
		foreach($updates as $update){
			if($update!='.gitkeep'){
				$uploadstat = $this->UpdatesModel->check_update($update);
				if(!$uploadstat){
					$pending_updates[] = $update;
				}
			}
		}
		if(count($pending_updates)>0){
			$this->load->dbutil();
			$backup = $this->dbutil->backup();
			$backup_stat = write_file('updates/backups/backup-'.date('Ymdhis').'.gz', $backup);
			if($backup_stat){
				$message = 'Following updates has executed:';
				$update_stats = array();
				foreach($pending_updates as $pending_update){
					$update_string = read_file('updates/data/'.$pending_update);
					$updatestat = $this->UpdatesModel->update_database($update_string,$pending_update);
					$update_stats = $update_stats+$updatestat;
				}
				$resultstat = true;
				foreach($update_stats as $update_stat){
					if($update_stat['status']=='true'){
						$message .= '<br/> SUCCESS - '.$update_stat['statement'];
					} else {
						$resultstat = false;
						$message .= '<br/> ERROR - '.$update_stat['statement'];
					}
				}
				if($resultstat){
					$this->session->set_flashdata('message', array('status'=>'alert-success','message'=>$message));
				} else {
					$this->session->set_flashdata('message', array('status'=>'alert-warning','message'=>$message));
				}
			} else {
				$this->session->set_flashdata('message', array('status'=>'alert-error','message'=>'Backup Failed.'));
			}
		} else {
			$this->session->set_flashdata('message', array('status'=>'alert-success','message'=>'No pending updates.'));
		}
		redirect(admin_url_string('home/dashboard'));
	}

}
