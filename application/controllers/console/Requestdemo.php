<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Requestdemo extends CSAdmin_Controller {

	function __construct()
    {
		// Call the Model constructor
		parent::__construct();
		$this->load->model(admin_url_string('RequestDemoModel'));
	}

	public function index()
	{
		redirect(admin_url_string('requestdemo/overview'));
	}

	public function overview()
	{
		$this->mainvars['page_title']= 'Requests';
		$this->load->library('pagination');
		$config = $this->paginationConfig();
		$config['base_url'] = admin_url('requestdemo/overview');
		$config['uri_segment'] = 4;
		$config['total_rows'] = $this->RequestDemoModel->get_pagination_count();
		$config['per_page'] = 20;
		$this->pagination->initialize($config);
		$this->mainvars['requestdemos'] = $this->RequestDemoModel->get_pagination($config['per_page'], $this->uri->segment($config['uri_segment']));
		$this->mainvars['content']=$this->load->view(admin_url_string('requestdemo/overview'),$this->mainvars,true);
		$this->load->view(admin_url_string('main'),$this->mainvars);
	}
	function update_status($status,$id)
	{
		if($status==1)
		{
			$data = array('status'=>'0');
		}
		else
		{
			$data = array('status'=>'1');
		}
		
		$cond=(array('id'=>$id));
			$loginid=$this->RequestDemoModel->update($data,$cond);
			if($loginid){
				$this->session->set_flashdata('message', array('status'=>'alert-success','message'=>'Updated Successfully.'));
				redirect(admin_url_string('requestdemo/overview/'));
			} else {
				$this->session->set_flashdata('message', array('status'=>'alert-danger','message'=>'Error! - Failed.'));
				redirect(admin_url_string('requestdemo/overview/'));
			}
	}

}