<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Services extends CSAdmin_Controller {

	function __construct()
    {
		// Call the Model constructor
		parent::__construct();
		$this->load->model(admin_url_string('ServicesModel'));
	}

	public function index()
	{
		redirect(admin_url_string('services/overview'));
	}

	public function overview()
	{
		$this->mainvars['page_title']= 'Services';
		$this->load->library('pagination');
		$config = $this->paginationConfig();
		$config['base_url'] = admin_url('services/overview');
		$config['uri_segment'] = 4;
		$config['total_rows'] = $this->ServicesModel->get_pagination_count();
		$config['per_page'] = 20;
		$this->pagination->initialize($config);
		$this->mainvars['services'] = $this->ServicesModel->get_pagination($config['per_page'], $this->uri->segment($config['uri_segment']));
		$this->mainvars['content']=$this->load->view(admin_url_string('services/overview'),$this->mainvars,true);
		$this->load->view(admin_url_string('main'),$this->mainvars);
	}
	function add()
	{
        $this->ckeditorCall();
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_message('required', 'required');
		$this->form_validation->set_error_delimiters('<span class="red">(', ')</span>');
		if ($this->form_validation->run() == FALSE)
		{
			$this->mainvars['page_title']='Add Services';
			$this->mainvars['content'] = $this->load->view(admin_url_string('services/add'), $this->mainvars, true);
			$this->load->view(admin_url_string('main'), $this->mainvars);
		} else {
            $image='';
            if (!is_dir('public/uploads/services'))
            {
                mkdir( 'public/uploads/services', 0777, TRUE);
            }
            $config['upload_path'] = 'public/uploads/services';
			$config['allowed_types'] = 'jpg|jpeg|png|gif|bmp';
			$this->load->library('upload', $config);
			if($this->upload->do_upload('image'))
			{
				$imagedata=$this->upload->data();
				$image=$imagedata['file_name'];
			}
            $slugtxt = str_replace('&','and',$this->input->post('title'));
            $slugtxt = str_replace(" ",'_',$slugtxt);
            $slugtxt = str_replace('%','_',$slugtxt);
            $slugtxt = str_replace('-','_',$slugtxt);
            $slugtxt = str_replace('(','_',$slugtxt);
            $slugtxt = str_replace(')','_',$slugtxt);
			$slug=$this->ServicesModel->create_slug($slugtxt);

			$data = array('slug'=>$slug,
						  'image'=>$image,
						  'status'=>$this->input->post('status'),
						  'title'=>$this->input->post('title'),
						  'short_desc'=>$this->input->post('short_desc'),
						  'description'=>$this->input->post('description'),
						  'meta_title'=>$this->input->post('meta_title'),
						  'meta_keywords'=>$this->input->post('meta_keywords'),
						  'meta_desc'=>$this->input->post('meta_desc'));
			$contentid = $this->ServicesModel->insert($data);
			if($contentid){
				$this->session->set_flashdata('message', array('status'=>'alert-success','message'=>'Added successfully.'));
				redirect(admin_url_string('services/overview'));
			} else {
				$this->session->set_flashdata('message', array('status'=>'alert-danger','message'=>'Error! - Failed.'));
				redirect(admin_url_string('services/overview'));
			}
		}
	}
	function edit($id)
	{
		$this->ckeditorCall();
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_message('required', 'required');
		$this->form_validation->set_error_delimiters('<span class="red">(', ')</span>');
		if($this->form_validation->run() == FALSE)
		{
			$this->mainvars['page_title']='Edit Service';
			$edit['service']= $this->ServicesModel->load($id);
			$this->mainvars['content'] = $this->load->view(admin_url_string('services/edit'), $edit, true);
			$this->load->view(admin_url_string('main'), $this->mainvars);
		} else {

            $ser_slug=$this->ServicesModel->load($id)->slug;
            $slug=$this->ServicesModel->update_slug($ser_slug,$id);

			$data = array('slug'=>$slug,
                          'status'=>$this->input->post('status'),
						  'title'=>$this->input->post('title'),
						  'short_desc'=>$this->input->post('short_desc'),
						  'description'=>$this->input->post('description'),
						  'meta_title'=>$this->input->post('meta_title'),
						  'meta_keywords'=>$this->input->post('meta_keywords'),
						  'meta_desc'=>$this->input->post('meta_desc'));
			
                            $config['upload_path'] = 'public/uploads/services';
                            $config['allowed_types'] = 'jpg|jpeg|png|gif|bmp';
                            $this->load->library('upload', $config);
                            if($this->upload->do_upload('image'))
                            {
                                $imagedata=$this->upload->data();
                                $data['image']=$imagedata['file_name'];
                            }
			$cond=array('id'=>$id);
			$updateid=$this->ServicesModel->update($data,$cond);
			if($updateid){
				$this->session->set_flashdata('message', array('status'=>'alert-success','message'=>'Edited successfully.'));
				redirect(admin_url_string('services/overview'));
			} else {
				$this->session->set_flashdata('message', array('status'=>'alert-danger','message'=>'Error! - Failed.'));
				redirect(admin_url_string('services/overview/'));
			}
		}
	}

    function delete($id) 
    {
        $service_image= $this->ServicesModel->load($id)->image;
		$cond=array('id'=>$id);
        $deleterow = $this->ServicesModel->delete($cond);
        if ($deleterow) {
            
            unlink('public/uploads/services/'.$service_image);
            $this->session->set_flashdata('message', array('status'=>'alert-success','message'=>'Service deleted successfully.'));
            redirect(admin_url_string('services/overview'));
        } else {
            $this->session->set_flashdata('message', array('status'=>'alert-danger','message'=>'Error! - Failed.'));
            redirect(admin_url_string('services/overview'));
        }
    }


	 function actions()
	{

		$loginid=false;
		$sort_orders=$this->input->post('sort_order');
 		if(isset($_POST['sortsave'])){
			foreach($sort_orders as $id => $sort_order):
				$data=array('sort_order'=>$sort_order);
				$cond=array('id'=>$id);
				$loginid=$this->ServicesModel->update($data,$cond);
				$flashmsg = 'Updated Successfully.';
			endforeach;
		}
		if($loginid){
            $this->session->set_flashdata('message', array('status'=>'alert-success','message'=>$flashmsg));

		}else{
			 $this->session->set_flashdata('message', array('status'=>'alert-danger','message'=>'Error! - Failed.'));
		}
		redirect(admin_url_string('services/overview'));
	}


}
