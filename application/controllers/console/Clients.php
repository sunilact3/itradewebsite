<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clients extends CSAdmin_Controller {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->model(admin_url_string('ClientsModel'));
    }
    
    public function index()
    {
        redirect(admin_url_string('clients/overview'));
    }
    
    public function overview()
    {
        $this->mainvars['page_title']= 'Clients';
        $this->mainvars['page_scripts']='';
        $this->load->library('pagination');
        $config = $this->paginationConfig();
        $config['base_url'] = admin_url('clients/overview/');
        $config['uri_segment'] = 4;
        $config['total_rows'] = $this->ClientsModel->get_pagination_count();
        $config['per_page'] = 20;
        $this->pagination->initialize($config);
        $vars['clients'] = $this->ClientsModel->get_pagination($config['per_page'], $this->uri->segment($config['uri_segment']));
        $this->mainvars['content']=$this->load->view(admin_url_string('clients/overview'),$vars,true);
        $this->load->view(admin_url_string('main'),$this->mainvars);
    }
    
    function add()
    {
        if (empty($_FILES['image']['name']))
        {
            $this->form_validation->set_rules('image', 'Image', 'required');
        }
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('status','Status', 'required');
        $this->form_validation->set_message('required', 'required');
        $this->form_validation->set_error_delimiters('<span class="red">(', ')</span>');
        if ($this->form_validation->run() == FALSE)
        {
            $this->mainvars['page_title']='Add Clients';
            $this->mainvars['page_scripts']='';
            $this->mainvars['content'] = $this->load->view(admin_url_string('clients/add'), '', true);
            $this->load->view(admin_url_string('main'),$this->mainvars);
        } else {
            $image='';
            if (!is_dir('public/uploads/clients'))
            {
                mkdir( 'public/uploads/clients', 0777, TRUE);
            }
            $config['upload_path'] = 'public/uploads/clients';
            $config['allowed_types'] = 'jpg|jpeg|png|gif|bmp';
            $this->load->library('upload', $config);
            if($this->upload->do_upload('image'))
            {
                $imagedata=$this->upload->data();
                $image=$imagedata['file_name'];
            }
            $data = array('title'=>$this->input->post('title'),
						  'link'=>$this->input->post('link'),
                          'image'=>$image,
                          'status'=>$this->input->post('status'));
            $contentid = $this->ClientsModel->insert($data);
            if($contentid){
                $this->session->set_flashdata('message', array('status'=>'alert-success','message'=>'Client added Successfully.'));
                redirect(admin_url_string('clients/overview/'));
            } else {
                $this->session->set_flashdata('message', array('status'=>'alert-danger','message'=>'Error! - Failed.'));
                redirect(admin_url_string('clients/overview/'));
            }
        }
    }

    function edit($id)
    {
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
        $this->form_validation->set_message('required', 'required');
        $this->form_validation->set_error_delimiters('<span class="red">(', ')</span>');
        if ($this->form_validation->run() == FALSE)
        {
            $this->mainvars['page_title']='Edit Clients';
            $this->mainvars['page_scripts']='';
            $edit['clients']= $this->ClientsModel->load($id);
            $this->mainvars['content'] = $this->load->view(admin_url_string('clients/edit'), $edit, true);
            $this->load->view(admin_url_string('main'),$this->mainvars);
        } else {
            $data = array('title'=>$this->input->post('title'),
						  'link'=>$this->input->post('link'),
                          'status'=>$this->input->post('status'));
            $config['upload_path'] = 'public/uploads/clients';
            $config['allowed_types'] = 'jpg|jpeg|png|gif|bmp';
            $this->load->library('upload', $config);
            if($this->upload->do_upload('image'))
            {
                $imagedata=$this->upload->data();
                $data['image']=$imagedata['file_name'];
            }
            $cond = array('id' => $id);
            $updateid=$this->ClientsModel->update($data,$cond);
            if($updateid){
                $this->session->set_flashdata('message', array('status'=>'alert-success','message'=>'Clients Updated Successfully.'));
                redirect(admin_url_string('clients/overview/'));
            } else {
                $this->session->set_flashdata('message', array('status'=>'alert-danger','message'=>'Error! - Failed.'));
                redirect(admin_url_string('clients/overview/'));
            }
        }
	}

    function delete($id)
    {
        $client_image= $this->ClientsModel->load($id)->image;
        $cond=array('id'=>$id); 
        $deleterow = $this->ClientsModel->delete($cond);
        if ($deleterow) {
            unlink('public/uploads/clients/'.$client_image);
            $this->session->set_flashdata('message', array('status'=>'alert-success','message'=>'Client Deleted successfully.'));
            redirect(admin_url_string('clients/overview'));
        } else {
            $this->session->set_flashdata('message', array('status'=>'alert-danger','message'=>'Error! - Failed.'));
            redirect(admin_url_string('clients/overview'));
        }
    }
    
    function actions()
    {
        $loginid=false;
        $sort_orders=$this->input->post('sort_order');
        if(isset($_POST['sortsave'])){
            foreach($sort_orders as $id => $sort_order):
            $data=array('sort_order'=>$sort_order);
            $cond=array('id'=>$id);
            $loginid=$this->ClientsModel->update($data,$cond);
            $flashmsg = 'Updated Successfully.';
            endforeach;
        }
        if($loginid){
            $this->session->set_flashdata('message', array('status'=>'alert-success','message'=>$flashmsg));
        }else{
            $this->session->set_flashdata('message', array('status'=>'alert-danger','message'=>'Error! - Failed.'));
        }
        redirect(admin_url_string('clients/overview'));
    }

}


