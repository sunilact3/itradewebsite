<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contactus extends CSAdmin_Controller {

	function __construct()
    {
		// Call the Model constructor
		parent::__construct();
		$this->load->model(admin_url_string('ContactusModel'));
	}

	public function index()
	{
		redirect(admin_url_string('contactus/overview'));
	}

	public function overview()
	{
		$this->mainvars['page_title']= 'Contact Us';
		$this->load->library('pagination');
		$config = $this->paginationConfig();
		$config['base_url'] = admin_url('contactus/overview');
		$config['uri_segment'] = 4;
		$config['total_rows'] = $this->ContactusModel->get_pagination_count();
		$config['per_page'] = 20;
		$this->pagination->initialize($config);
		$this->mainvars['contactus'] = $this->ContactusModel->get_pagination($config['per_page'], $this->uri->segment($config['uri_segment']));
		$this->mainvars['content']=$this->load->view(admin_url_string('contactus/overview'),$this->mainvars,true);
		$this->load->view(admin_url_string('main'),$this->mainvars);
	}

	function view($id)
	{
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_message('required', 'required');
		$this->form_validation->set_error_delimiters('<span class="red">(', ')</span>');
		if($this->form_validation->run() == FALSE)
		{
			$this->mainvars['page_title']='View Contacts';
			$edit['contact']= $this->ContactusModel->load($id);
			$this->mainvars['content'] = $this->load->view(admin_url_string('contactus/view'), $edit, true);
			$this->load->view(admin_url_string('main'), $this->mainvars);
		} else {
			$data = array('status'=>$this->input->post('status'));
            $cond = array('id' => $id);
			$updateid=$this->ContactusModel->update($data,$cond);
			if($updateid){
                $this->session->set_flashdata('message', array('status'=>'alert-success','message'=>'Updated successfully.'));
				redirect(admin_url_string('contactus/overview/'));
			} else {
                
				$this->session->set_flashdata('message', array('status'=>'alert-danger','message'=>'Error! - Failed.'));
				redirect(admin_url_string('contactus/overview/'));
			}
		}
	}

}