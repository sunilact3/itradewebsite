<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends CSAdmin_Controller {

	function __construct()
    {
		// Call the Model constructor
		parent::__construct();
		$this->load->model(admin_url_string('PagesModel'));
	}

	public function index()
	{
		redirect(admin_url_string('pages/overview'));
	}

	public function overview()
	{
		$this->mainvars['page_title']= 'Pages';
		$this->mainvars['page_scripts']='';
		$this->load->library('pagination');
		$config = $this->paginationConfig();
		$config['base_url'] = admin_url('pages/overview/');
		$config['uri_segment'] = 4;
		$config['total_rows'] = $this->PagesModel->get_pagination_count();
		$config['per_page'] = 20;
		$this->pagination->initialize($config);
		$vars['pages'] = $this->PagesModel->get_pagination($config['per_page'], $this->uri->segment($config['uri_segment']));
		$this->mainvars['content']=$this->load->view(admin_url_string('pages/overview'),$vars,true);
		$this->load->view(admin_url_string('main'),$this->mainvars);
	}

	function add()
	{
		$this->ckeditorCall();
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('page_key', 'Page Key', 'required');
		$this->form_validation->set_rules('meta_title', 'Meta Title', 'required');
		$this->form_validation->set_rules('body', 'Body', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_message('required', 'required');
		$this->form_validation->set_error_delimiters('<span class="red">(', ')</span>');
		$this->form_validation->set_message('is_unique', 'already exists');
		if ($this->form_validation->run() == FALSE)
		{
			$this->mainvars['page_title']='Add Page';
			$this->mainvars['page_scripts']='';
			$this->mainvars['content'] = $this->load->view(admin_url_string('pages/add'), '', true);
			$this->load->view(admin_url_string('main'),$this->mainvars);
		} else {
            $image='';
            if (!is_dir('public/uploads/pages'))
            {
                mkdir( 'public/uploads/pages', 0777, TRUE);
            }
			$config['upload_path'] = 'public/uploads/pages';
			$config['allowed_types'] = 'jpg|jpeg|png|gif|bmp';
			$this->load->library('upload', $config);
			if($this->upload->do_upload('image'))
			{
				$imagedata=$this->upload->data();
				$image=$imagedata['file_name'];
			}
			$data = array('title'=>$this->input->post('title'),
                          'page_key'=>$this->input->post('page_key'),
				          'meta_title'=>$this->input->post('meta_title'),
                          'meta_keywords'=>$this->input->post('meta_keywords'),
                          'meta_desc'=>$this->input->post('meta_desc'),
						  'short_desc'=>$this->input->post('short_desc'),
                          'body'=>$this->input->post('body'),
                          'image'=>$image,
                          'status'=>$this->input->post('status'));
			$contentid = $this->PagesModel->insert($data);
			if($contentid){
				 $this->session->set_flashdata('message', array('status'=>'alert-success','message'=>'Page added Successfully.'));
				redirect(admin_url_string('pages/overview/'));
			} else {
				$this->session->set_flashdata('message', array('status'=>'alert-danger','message'=>'Error! - Failed.'));
				redirect(admin_url_string('pages/overview/'));
			}
		}
	}

	function edit($id)
	{
		$this->ckeditorCall();
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('page_key', 'Page Key', 'required');
		$this->form_validation->set_rules('meta_title', 'Meta Title', 'required');
	  $this->form_validation->set_rules('body', 'Body', 'required');
	  $this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_message('required', 'required');
		$this->form_validation->set_error_delimiters('<span class="red">(', ')</span>');
		if ($this->form_validation->run() == FALSE)
		{
			$this->mainvars['page_title']='Edit Page';
			$this->mainvars['page_scripts']='';
			$edit['page']= $this->PagesModel->load($id);
			$this->mainvars['content'] = $this->load->view(admin_url_string('pages/edit'), $edit, true);
			$this->load->view(admin_url_string('main'),$this->mainvars);
		} else {
			$data = array('title'=>$this->input->post('title'),
						  'meta_title'=>$this->input->post('meta_title'),
                          'meta_keywords'=>$this->input->post('meta_keywords'),
                          'meta_desc'=>$this->input->post('meta_desc'),
						  'short_desc'=>$this->input->post('short_desc'),
                          'body'=>$this->input->post('body'),
                          'status'=>$this->input->post('status'));

            $config['upload_path'] = 'public/uploads/pages';
            $config['allowed_types'] = 'jpg|jpeg|png|gif|bmp';
            $this->load->library('upload', $config);
            if($this->upload->do_upload('image'))
            {
                $imagedata=$this->upload->data();
                $data['image']=$imagedata['file_name'];
            }
            $cond = array('id' => $id);
			$loginid=$this->PagesModel->update($data,$cond);
			if($loginid){
 				$this->session->set_flashdata('message', array('status'=>'alert-success','message'=>'Page Updated Successfully.'));
				redirect(admin_url_string('pages/overview/'));
			} else {
								$this->session->set_flashdata('message', array('status'=>'alert-danger','message'=>'Error! - Failed.'));
				redirect(admin_url_string('pages/overview/'));
			}
		}
	}

	function delete($id) {
		$data = array('id'=>$id);
		$deleterow = $this->PagesModel->delete($data);
		if ($deleterow) {
			$this->session->set_flashdata('message', array('status'=>'alert-success','message'=>'Page Deleted successfully.'));
			redirect(admin_url_string('pages/overview'));
		} else {
			$this->session->set_flashdata('message', array('status'=>'alert-danger','message'=>'Error! - Failed.'));
			redirect(admin_url_string('pages/overview'));
		}
	}
	}


/* End of file languages.php */
/* Location: ./application/controllers/admin/languages.php */
