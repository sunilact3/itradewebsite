<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CSAdmin_Controller {
	function __construct()
    {
		// Call the Model constructor
		parent::__construct();
		$this->load->model(admin_url_string('ProductsModel'));
	}

	public function index()
	{
		redirect(admin_url_string('products/overview'));
	}

	public function overview() {
		$this->mainvars['page_title']= 'Products';
		$this->mainvars['page_scripts']='';
		$this->load->library('pagination');
		$config = $this->paginationConfig();
        $config['base_url'] = admin_url('products/overview');
        $config['uri_segment'] = 4;
        $config['total_rows'] = $this->ProductsModel->get_pagination_count();
        $config['per_page'] = 30;
        $this->pagination->initialize($config);
           
        $vars['products'] = $this->ProductsModel->get_pagination($config['per_page'], $this->uri->segment($config['uri_segment']));
		$this->mainvars['content']=$this->load->view(admin_url_string('products/overview'),$vars,true);
		$this->load->view(admin_url_string('main'),$this->mainvars);
	}

	function add() {
        $this->ckeditorCall();
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_message('required', 'required');
		$this->form_validation->set_error_delimiters('<span class="validation-error red">(', ')</span>');
		if ($this->form_validation->run() == FALSE) {
            $this->mainvars['page_title']= 'Add Product';
			$this->mainvars['page_scripts']='';
			$this->mainvars['content'] = $this->load->view(admin_url_string('products/add'), '', true);
			$this->load->view(admin_url_string('main'),$this->mainvars);
		} else {
            $icon='';
            if (!is_dir('public/uploads/products/icon'))
            {
                mkdir( 'public/uploads/products/icon', 0777, TRUE);
            }
			$config['upload_path'] = 'public/uploads/products/icon';
			$config['allowed_types'] = 'jpg|jpeg|png|gif|bmp';
			$this->load->library('upload', $config);
			if($this->upload->do_upload('icon'))
			{
				$icondata=$this->upload->data();
				$icon=$icondata['file_name'];
			}
            $slugtxt = str_replace('&','and', $this->input->post('title'));
            $slugtxt = str_replace(" ",'_',$slugtxt);
						$slugtxt = str_replace('%','_',$slugtxt);
						$slugtxt = str_replace('-','_',$slugtxt);
						$slugtxt = str_replace('(','_',$slugtxt);
						$slugtxt = str_replace(')','_',$slugtxt);
			$slug=$this->ProductsModel->create_slug($slugtxt);
			$data = array('slug'=>$slug,
                          'title' => $this->input->post('title'),
                          'short_desc' => $this->input->post('short_desc'),
                          'description' => $this->input->post('description'),
                          'meta_title'=>$this->input->post('meta_title'),
                          'meta_keywords'=>$this->input->post('meta_keywords'),
                          'meta_desc'=>$this->input->post('meta_desc'),
						  'icon'=>$icon,
                          'status' => $this->input->post('status'));
			$insertrow = $this->ProductsModel->insert($data);
			if ($insertrow) {
				$this->session->set_flashdata('message', array('status'=>'alert-success','message'=>'Added successfully.'));
				redirect(admin_url_string('products/overview'));
			} else {
				$this->session->set_flashdata('message', array('status'=>'alert-danger','message'=>'Error! - Failed.'));
                redirect(admin_url_string('products/overview'));
			}
		}
	}

	public function edit($id){
        $this->ckeditorCall();
        $this->form_validation->set_rules('slug', 'Slug', 'required');
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_message('required', 'required');
		$this->form_validation->set_error_delimiters('<span class="validation-error red">(', ')</span>');
		if ($this->form_validation->run() == FALSE) {
            $this->mainvars['page_title']= 'Edit Product';
			$this->mainvars['page_scripts']='';

			$vars['product'] =$this->ProductsModel->load($id);
			$this->mainvars['content'] = $this->load->view(admin_url_string('products/edit'), $vars, true);
			$this->load->view(admin_url_string('main'),$this->mainvars);
		} else {
            $slugtxt = str_replace('&','and', $this->input->post('slug'));
            $slugtxt = str_replace(" ",'_',$slugtxt);
						$slugtxt = str_replace('%','_',$slugtxt);
						$slugtxt = str_replace('-','_',$slugtxt);
						$slugtxt = str_replace('(','_',$slugtxt);
						$slugtxt = str_replace(')','_',$slugtxt);
			$slug=$this->ProductsModel->create_slug($slugtxt);

			$data = array('slug'=>$slug,
                          'title' => $this->input->post('title'),
                          'short_desc' => $this->input->post('short_desc'),
                          'description' => $this->input->post('description'),
                          'meta_title'=>$this->input->post('meta_title'),
                          'meta_keywords'=>$this->input->post('meta_keywords'),
                          'meta_desc'=>$this->input->post('meta_desc'),
                          'status' => $this->input->post('status'));

            $config['upload_path'] = 'public/uploads/products/icon';
            $config['allowed_types'] = 'jpg|jpeg|png|gif|bmp';
            $this->load->library('upload', $config);
            if($this->upload->do_upload('icon'))
            {
                $icondata=$this->upload->data();
                $data['icon']=$icondata['file_name'];
            }
			$cond = array('id'=>$id);
			$updaterow = $this->ProductsModel->update($data,$cond);
			if ($updaterow) {
				$this->session->set_flashdata('message', array('status'=>'alert-success','message'=>'Edited successfully.'));
				redirect(admin_url_string('products/overview'));
			} else {
				$this->session->set_flashdata('message', array('status'=>'alert-danger','message'=>'Error! - Failed.'));
				redirect(admin_url_string('products/overview'));
			}
		}
	}

	function delete($id) {
		$icon= $this->ProductsModel->load($id)->icon;
		$data = array('id'=>$id);
        $deleterow = $this->ProductsModel->delete($data);
		if ($deleterow) {

            unlink('public/uploads/products/icon/'.$icon);
			$this->session->set_flashdata('message', array('status'=>'alert-success','message'=>'Deleted successfully.'));
			redirect(admin_url_string('products/overview'));
		} else {
			$this->session->set_flashdata('message', array('status'=>'alert-danger','message'=>'Error! - Failed.'));
			redirect(admin_url_string('products/overview'));
		}
	}


    function actions()
	{
        $loginid=false;
			$sort_orders=$this->input->post('sort_order');
			if(isset($_POST['sortsave']))
			{
				foreach($sort_orders as $id=>$sort_order):
					$data=array('sort_order'=>$sort_order);
					$cond=array('id'=>$id);
					$loginid=$this->ProductsModel->update($data,$cond);
					$flashmsg='Sorted Successfully';
				endforeach;
			}


	if($loginid)
			{
				$this->session->set_flashdata('message',array('status'=>'alert-success','message'=>$flashmsg));
			}else
			{
				$this->session->set_flashdata('message',array('status'=>'alert-danger','message'=>'Error! - Failed.'));
			}
			redirect(admin_url_string('products/overview'));
	}
}
