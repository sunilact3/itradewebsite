<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct() {
		// Call the Model constructor
		parent::__construct();
		if ($this->session->userdata('admin_user_logged_in')) {
			redirect(admin_url_string('home'));
		}
		$this->load->model(admin_url_string('AdminsModel'));
	}

	public function index() {
		$this->form_validation->set_rules('username', 'Username', 'required|callback_login_check');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_message('required', '(required)');
		if ($this->form_validation->run() == FALSE) {
			$login['content'] = $this->load->view(admin_url_string('login/login'),'', true);
			$this->load->view(admin_url_string('login'), $login);
		} else {
			$this->load->model(admin_url_string('AdminHistoryModel'));
			$user = $this->db->escape_str($this->input->post('username'));
			$pass = $this->db->escape_str($this->input->post('password'));
			$user_row = $this->AdminsModel->login_check($user, $pass);
			if($user_row) {
				$logindata = array(
					'uid' => $user_row->uid,
					'timestamp' => time(),
					'ipaddress' => $this->input->ip_address());
				$this->AdminHistoryModel->insert($logindata);
				$newdata = array(
					'admin_user_id' => $user_row->uid,
					'admin_user_name' => $user_row->fullname,
					'admin_user_email' => $user_row->email,
					'admin_user_role' => $user_row->role,
					'admin_user_logged_in' => TRUE
				);

				$this->session->set_userdata($newdata);
			}
            $target = $this->session->userdata('admin_user_destination');
            $baseurl = base_url();
            if(strpos($target, $baseurl) !== false) {
                $this->session->unset_userdata('admin_user_destination');
                redirect($target);
            }else{
                redirect(admin_url_string('home'));
            }

		}
	}

	function login_check($user) {
		$pass = $this->input->post('password');
		if(!$this->AdminsModel->login_check($user, $pass)) {
			$this->form_validation->set_message('login_check', 'Invalid Login');
			return FALSE;
		} else {
			return TRUE;
		}
	}

	public function forgot() {
		$this->form_validation->set_rules('username', 'Username or Email Address', 'required|callback_forgot_check');
		$this->form_validation->set_message('required', '(required)');
		if ($this->form_validation->run() == FALSE) {
			$login['content'] = $this->load->view(admin_url_string('login/forgot'), '', true);
			$this->load->view(admin_url_string('login'), $login);
		} else {
			$user = $this->db->escape_str($this->input->post('username'));
			$user_row = $this->AdminsModel->forgot_check($user);
			if ($user_row) {
				$this->load->model(admin_url_string('AdminResetModel'));
				$this->load->helper('string');
				$resetdata = array('user_id' => $user_row->uid, 'user_key' => random_string('alnum', 16));
				$this->AdminResetModel->insert($resetdata);
				$message = $this->load->view(admin_url_string('mail/passwordrequest'), $resetdata, TRUE);
				$this->sendfromadmin($user_row->email, $this->config->item('site_name').' - Password Reset Request', $message);
				$this->session->set_flashdata('message', array('status'=>'alert-success','message'=>'Password reset link sent to your email.'));
				redirect(admin_url_string('login/forgot'));
			} else {
				redirect(admin_url_string('login/forgot'));
			}
		}
	}

	function forgot_check($user) {
		if (!$this->AdminsModel->forgot_check($user)) {
			$this->form_validation->set_message('forgot_check', 'Invalid Username or Email Address');
			return FALSE;
		} else {
			return TRUE;
		}
	}

	public function resetpwd($id, $key) {
		$this->load->model(admin_url_string('AdminResetModel'));
		$resetcond = array('user_id' => $id, 'user_key' => $key);
		if ($this->AdminResetModel->exists($resetcond)) {
			$this->form_validation->set_rules('pass', 'Password', 'required|matches[passconf]');
			$this->form_validation->set_rules('passconf', 'Confirm Password', 'required');
			$this->form_validation->set_message('required', '(required)');
			$this->form_validation->set_message('matches', 'Password Mismatch');
			if ($this->form_validation->run() == FALSE) {
				$reset['id'] = $id;
				$reset['key'] = $key;
				$login['content'] = $this->load->view(admin_url_string('login/reset'), $reset, true);
				$this->load->view(admin_url_string('login'), $login);
			} else {
				$cond = array('uid' => $id, 'status' => '1');
				$user_row = $this->AdminsModel->get_row_cond($cond);
				if($user_row){
					$passtext = $this->db->escape_str($this->input->post('pass'));
					$pass = sha1($passtext.$user_row->salt);
					$data = array('password' => $pass);
					$cond = array('uid' => $id);
					$this->AdminsModel->update($data, $cond);
					$resetdata = array('user_id' => $id, 'user_key' => $key);
					$this->AdminResetModel->delete($resetdata);
					redirect(admin_url_string('login/resetsuccess'));
				}
			}
		} else {
			redirect(admin_url_string('login/forgot'));
		}
	}

	public function resetsuccess() {
		$login['content'] = $this->load->view(admin_url_string('login/resetsuccess'), '', true);
		$this->load->view(admin_url_string('login'), $login);
	}

	function sendfromadmin($to, $subject, $message, $attachment = '') {
		$this->load->library('email');
		$config['mailtype'] = 'html';
		$config['charset'] = 'utf-8';
		$config['wordwrap'] = TRUE;
		$this->email->initialize($config);
		$this->email->from ($this->settings['FROM_MAIL'],$this->settings['FROM_NAME']);
		$this->email->to($to);
		$this->email->subject($subject);
		if ($attachment != '') {
			$this->email->attach($attachment);
		}
		$this->email->message($message);
		if ($this->email->send()) {
			return true;
		} else {
			return false;
		}
	}
}
