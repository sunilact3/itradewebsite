<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Widgets extends CSAdmin_Controller {

	function __construct()
    {
		// Call the Model constructor
		parent::__construct();
		$this->load->model(admin_url_string('AdminWidgetsModel'));
	}
		
	public function index()
	{
		redirect(admin_url_string('widgets/overview'));
	}
	
	public function overview()
	{
		
		$this->mainvars['page_title']= 'Widgets';
				
		$this->mainvars['page_scripts']='';
		$this->load->library('pagination');
		$config = $this->paginationConfig();
		$config['base_url'] = admin_url('widgets/overview/');
		$config['uri_segment'] = 5;
		$config['total_rows'] = $this->AdminWidgetsModel->get_pagination_count();
		$config['per_page'] = 20;
		$this->pagination->initialize($config);
		$vars['widgets'] = $this->AdminWidgetsModel->get_pagination($config['per_page'], $this->uri->segment($config['uri_segment']));
		$this->mainvars['content']=$this->load->view(admin_url_string('widgets/overview'),$vars,true);
		$this->load->view(admin_url_string('main'),$this->mainvars);
	}
	
	function add()
	{
		$this->form_validation->set_rules('widget_name', 'Widget Name', 'required');
		$this->form_validation->set_rules('widget_key', 'Key', 'required');
		$this->form_validation->set_message('required', 'required');
		$this->form_validation->set_error_delimiters('<span class="red">(', ')</span>');
		if ($this->form_validation->run() == FALSE)
		{
			$this->mainvars['page_title']='Add Widgets';
			$this->mainvars['page_scripts']='';
			$this->mainvars['content'] = $this->load->view(admin_url_string('widgets/add'), '', true);
			$this->load->view(admin_url_string('main'),$this->mainvars);
		} else {
			$data = array('widget_name'=>$this->input->post('widget_name'),
                          'description'=>$this->input->post('description'),
                          'widget_key'=>$this->input->post('widget_key'));
			$contentid = $this->AdminWidgetsModel->insert($data);
			if($contentid){
				$this->session->set_flashdata('message', '<div class="n_ok flash_messages"><p>Added Successfully.</p></div>');
				redirect(admin_url_string('widgets/overview'));
			} else {
				$this->session->set_flashdata('message', '<div class="n_error flash_messages"><p>Error!! - Not added.</p></div>');
				redirect(admin_url_string('widgets/overview'));
			}
		}
	}
	
	function edit($id)
	{
		$this->form_validation->set_rules('widget_name', 'Widget Name', 'required');
		$this->form_validation->set_rules('widget_key', 'Key', 'required');
		$this->form_validation->set_message('required', 'required');
		$this->form_validation->set_error_delimiters('<span class="red">(', ')</span>');
		if ($this->form_validation->run() == FALSE)
		{
			
			$this->mainvars['page_title']='Edit Banners';
			$this->mainvars['page_scripts']='';
			$edit['widget']= $this->AdminWidgetsModel->load($id);
			$this->mainvars['content'] = $this->load->view(admin_url_string('widgets/edit'), $edit, true);
			$this->load->view(admin_url_string('main'),$this->mainvars);
		} else {
			$data = array('widget_name'=>$this->input->post('widget_name'),
                          'description'=>$this->input->post('description'),
                          'widget_key'=>$this->input->post('widget_key'));
            $cond = array('wid' => $id);
			$loginid=$this->AdminWidgetsModel->update($data,$cond);
			if($loginid){
				$this->session->set_flashdata('message', '<div class="n_ok flash_messages"><p>Updated Successfully.</p></div>');
				redirect(admin_url_string('widgets/overview/'));
			} else {
				$this->session->set_flashdata('message', '<div class="n_error flash_messages"><p>Error!! - Page not updated.</p></div>');
				redirect(admin_url_string('widgets/overview/'));
			}
		}
	}
	
}

/* End of file languages.php */
/* Location: ./application/controllers/admin/languages.php */
