<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contactus extends CSFront_Controller {

	function __construct() {
		parent::__construct();
	}
	public function index()
	{
		$this->output->cache($this->settings['CACHE_TIME']);
		$content['newslettersuccess']='';
        $this->load->model(frontend_model_path('ProductsModel'));
        $this->load->model(frontend_model_path('ServicesModel'));
        $this->load->model(frontend_model_path('BannersModel'));
        $this->load->model(frontend_model_path('PagesModel'));
        $this->load->model(frontend_model_path('ContactusModel'));
        $this->load->config('recaptcha');
        $main['page_scripts']='';
        $this->pagetitle = 'Contact Us';
        $page=$this->PagesModel->get_row_cond(array('page_key'=>'CONTACT_PAGE'));
        if($page){
            if($page->meta_title!=''){ $this->pagetitle = $page->meta_title;}
            if($page->meta_keywords!=''){ $this->metakeywords = $page->meta_keywords;}  
            if($page->meta_desc!=''){ $this->metadesc = $page->meta_desc;}
        }
		$main['meta']=$this->frontmetahead();
		$vars['site_key'] = $this->config->item('recaptcha_site_key');
        $content['page']=$page;
		$headerbanner=$page->image;
        $content['products']=$this->ProductsModel->get_active();
        $content['services']=$this->ServicesModel->get_active();
		$vars['countries'] = $this->ContactusModel->get_countries();
		$vars['leads'] = $this->ContactusModel->get_leadtype();
		$vars['success_contact']='';
		$content['contactform']=$this->load->view('frontend/contact/contactform',$vars,true);
		$content['newsletterform']=$this->load->view('frontend/newsletter/newsletterform',$content,true);
		$frontcontent=$this->load->view('frontend/content/contactus',$content,true);
        $footercontent = $this->load->view('frontend/include/footercontact',$content,true);
		$main['contents']=$this->frontcontent($frontcontent,false);
		$main['header']=$this->frontheader($headerbanner);
		$main['footer']=$this->fronthomefooter($footercontent);
		$this->load->view('frontend/main',$main);
	}
	function contactus()
	{
		$this->output->cache($this->settings['CACHE_TIME']);
		$this->load->config('recaptcha');
		$this->load->model(frontend_model_path('ContactusModel'));
		$result = array();
		$this->form_validation->set_rules('name', 'Full Name', 'required');
		$this->form_validation->set_rules('email', 'Email Address', 'required|valid_email');
		$this->form_validation->set_rules('phone', 'Mobile', 'required');
		$this->form_validation->set_rules('message', 'Message', 'required');
        $this->form_validation->set_rules('g-recaptcha-response', 'Captcha','required|callback_captcha_check');
		$this->form_validation->set_message('required', 'required');
		$this->form_validation->set_message('valid_email', 'invalid email');
		$this->form_validation->set_error_delimiters('<span class="contact-validation-error red">', '</span>');
		if ($this->form_validation->run() == FALSE) {
            $vars['site_key'] = $this->config->item('recaptcha_site_key');
			$vars['success_contact']='';
			$result['status'] = '0';
			$vars['countries'] = $this->ContactusModel->get_countries();
			$vars['leads'] = $this->ContactusModel->get_leadtype();
	  		$result['data'] = $this->load->view('frontend/contact/contactform',$vars,TRUE);
		} else {
            $vars['site_key'] = $this->config->item('recaptcha_site_key');
			$maindata = array('name' => $this->db->escape_str($this->input->post('name')),
						  'email' => $this->db->escape_str($this->input->post('email')),
						  'phone' => $this->db->escape_str($this->input->post('phone')),
						  'country' => $this->db->escape_str($this->input->post('country')),
						  'city' => $this->db->escape_str($this->input->post('city')),
						  'lead_type' => $this->db->escape_str($this->input->post('lead_type')),
						  'message' => $this->db->escape_str($this->input->post('message')),
						  'contact_date' => date('Y-m-d h:i:s'),
						  'status'=>'1');
			$this->ContactusModel->insert($maindata);
			$subject = 'Enquery From Website';
			$view['data'] = (object) $maindata;
			$message = $this->load->view('frontend/mail/contactusmail', $view, TRUE);
			$from_name=$this->db->escape_str($this->input->post('name'));
	        $this->sendtoadmin($this->settings['ADMIN_EMAIL'],$from_name,$attachment='',$subject,$message);
			$vars['success_contact']=$this->load->view('frontend/contact/success','',TRUE);
			$vars['countries'] = $this->ContactusModel->get_countries();
			$vars['leads'] = $this->ContactusModel->get_leadtype();
			$result['data'] = $this->load->view('frontend/contact/contactform',$vars,TRUE);
            $result['status'] = '1';
		}
			$json=json_encode($result);
			exit($json);
		
	}

    
     function captcha_check($captcha){
    	$this->load->config('recaptcha');
		//your site secret key
		$secret = $this->config->item('recaptcha_secret_key');
		//get verify response data
		$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$captcha);
		$responseData = json_decode($verifyResponse);
		if($responseData->success):
			return TRUE;
		else:
			$error = 'Captcha failed.';
			$this->form_validation->set_message('captcha_check', $error);
            return FALSE;
		endif;
    }
}
    