<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CSFront_Controller {

	function __construct() {
		parent::__construct();
	}
	public function privacy()
	{
		$this->output->cache($this->settings['CACHE_TIME']);
		$content['success']='';
		$content['newslettersuccess']='';
        $this->load->model(frontend_model_path('ProductsModel'));
        $this->load->model(frontend_model_path('ServicesModel'));
        $this->load->model(frontend_model_path('BannersModel'));
        $this->load->model(frontend_model_path('PagesModel'));
        $this->load->model(admin_url_string('ConfigurationModel'));
        $main['page_scripts']='';
        $this->pagetitle = 'Privacy';
        $page=$this->PagesModel->get_row_cond(array('page_key'=>'PRIVACY_KEY'));
        if($page){
            if($page->meta_title!=''){ $this->pagetitle = $page->meta_title;}
            if($page->meta_keywords!=''){ $this->metakeywords = $page->meta_keywords;}  
            if($page->meta_desc!=''){ $this->metadesc = $page->meta_desc;}
        }
		$main['meta']=$this->frontmetahead();
        $content['page']=$page;
		$headerbanner=$page->image;
        $content['products']=$this->ProductsModel->get_active();
        $content['services']=$this->ServicesModel->get_active();
       //$content['config_itrade']=$this->ConfigurationModel->get_row_cond(array('config_key'=>'home_why_itrade_key'));
		$content['config_request']=$this->ConfigurationModel->get_row_cond(array('config_key'=>'request'));
		$frontcontent=$this->load->view('frontend/pages/page',$content,true);
		$content['requestdemoform']=$this->load->view('frontend/request/request',$content,true);
		$content['newsletterform']=$this->load->view('frontend/newsletter/newsletterform','',true);
        $footercontent = $this->load->view('frontend/content/footerhome',$content,true);
		$main['contents']=$this->frontcontent($frontcontent,false);
		$main['header']=$this->frontheader($headerbanner);
		$main['footer']=$this->fronthomefooter($footercontent);
		$this->load->view('frontend/main',$main);
	}
	public function terms()
	{
		$this->output->cache($this->settings['CACHE_TIME']);
		$content['success']='';
		$content['newslettersuccess']='';
        $this->load->model(frontend_model_path('ProductsModel'));
        $this->load->model(frontend_model_path('ServicesModel'));
        $this->load->model(frontend_model_path('BannersModel'));
        $this->load->model(frontend_model_path('PagesModel'));
        $this->load->model(admin_url_string('ConfigurationModel'));
        $main['page_scripts']='';
        $this->pagetitle = 'Terms and Conditions';
        $page=$this->PagesModel->get_row_cond(array('page_key'=>'TERMS_KEY'));
        if($page){
            if($page->meta_title!=''){ $this->pagetitle = $page->meta_title;}
            if($page->meta_keywords!=''){ $this->metakeywords = $page->meta_keywords;}  
            if($page->meta_desc!=''){ $this->metadesc = $page->meta_desc;}
        }
		$main['meta']=$this->frontmetahead();
        $content['page']=$page;
		$headerbanner=$page->image;
        $content['products']=$this->ProductsModel->get_active();
        $content['services']=$this->ServicesModel->get_active();
       //$content['config_itrade']=$this->ConfigurationModel->get_row_cond(array('config_key'=>'home_why_itrade_key'));
		$content['config_request']=$this->ConfigurationModel->get_row_cond(array('config_key'=>'request'));
		$frontcontent=$this->load->view('frontend/pages/page',$content,true);
		$content['requestdemoform']=$this->load->view('frontend/request/request',$content,true);
		$content['newsletterform']=$this->load->view('frontend/newsletter/newsletterform','',true);
        $footercontent = $this->load->view('frontend/content/footerhome',$content,true);
		$main['contents']=$this->frontcontent($frontcontent,false);
		$main['header']=$this->frontheader($headerbanner);
		$main['footer']=$this->fronthomefooter($footercontent);
		$this->load->view('frontend/main',$main);
	}
	public function profile()
	{
		$this->output->cache($this->settings['CACHE_TIME']);
		$content['success']='';
		$content['newslettersuccess']='';
        $this->load->model(frontend_model_path('ProductsModel'));
        $this->load->model(frontend_model_path('ServicesModel'));
        $this->load->model(frontend_model_path('BannersModel'));
        $this->load->model(frontend_model_path('PagesModel'));
        $this->load->model(admin_url_string('ConfigurationModel'));
        $main['page_scripts']='';
        $this->pagetitle = 'Company Profile';
        $page=$this->PagesModel->get_row_cond(array('page_key'=>'COMPANY_PROFILE_PAGE'));
        if($page){
            if($page->meta_title!=''){ $this->pagetitle = $page->meta_title;}
            if($page->meta_keywords!=''){ $this->metakeywords = $page->meta_keywords;}  
            if($page->meta_desc!=''){ $this->metadesc = $page->meta_desc;}
        }
		$main['meta']=$this->frontmetahead();
        $content['page']=$page;
		$headerbanner=$page->image;
        $content['products']=$this->ProductsModel->get_active();
        $content['services']=$this->ServicesModel->get_active();
       //$content['config_itrade']=$this->ConfigurationModel->get_row_cond(array('config_key'=>'home_why_itrade_key'));
		$content['config_request']=$this->ConfigurationModel->get_row_cond(array('config_key'=>'request'));
		$frontcontent=$this->load->view('frontend/pages/page',$content,true);
		$content['requestdemoform']=$this->load->view('frontend/request/request',$content,true);
		$content['newsletterform']=$this->load->view('frontend/newsletter/newsletterform','',true);
        $footercontent = $this->load->view('frontend/content/footerhome',$content,true);
		$main['contents']=$this->frontcontent($frontcontent,false);
		$main['header']=$this->frontheader($headerbanner);
		$main['footer']=$this->fronthomefooter($footercontent);
		$this->load->view('frontend/main',$main);
	}
	public function whyus()
	{
		$this->output->cache($this->settings['CACHE_TIME']);
		$content['success']='';
		$content['newslettersuccess']='';
        $this->load->model(frontend_model_path('ProductsModel'));
        $this->load->model(frontend_model_path('ServicesModel'));
        $this->load->model(frontend_model_path('BannersModel'));
        $this->load->model(frontend_model_path('PagesModel'));
        $this->load->model(admin_url_string('ConfigurationModel'));
        $main['page_scripts']='';
        $this->pagetitle = 'Why Us';
        $page=$this->PagesModel->get_row_cond(array('page_key'=>'WHY_US_PAGE'));
        if($page){
            if($page->meta_title!=''){ $this->pagetitle = $page->meta_title;}
            if($page->meta_keywords!=''){ $this->metakeywords = $page->meta_keywords;}  
            if($page->meta_desc!=''){ $this->metadesc = $page->meta_desc;}
        }
		$main['meta']=$this->frontmetahead();
        $content['page']=$page;
		$headerbanner=$page->image;
        $content['products']=$this->ProductsModel->get_active();
        $content['services']=$this->ServicesModel->get_active();
       //$content['config_itrade']=$this->ConfigurationModel->get_row_cond(array('config_key'=>'home_why_itrade_key'));
		$content['config_request']=$this->ConfigurationModel->get_row_cond(array('config_key'=>'request'));
		$frontcontent=$this->load->view('frontend/pages/page',$content,true);
		$content['requestdemoform']=$this->load->view('frontend/request/request',$content,true);
		$content['newsletterform']=$this->load->view('frontend/newsletter/newsletterform','',true);
        $footercontent = $this->load->view('frontend/content/footerhome',$content,true);
		$main['contents']=$this->frontcontent($frontcontent,false);
		$main['header']=$this->frontheader($headerbanner);
		$main['footer']=$this->fronthomefooter($footercontent);
		$this->load->view('frontend/main',$main);
	}
	public function quality()
	{
		$this->output->cache($this->settings['CACHE_TIME']);
		$content['success']='';
		$content['newslettersuccess']='';
        $this->load->model(frontend_model_path('ProductsModel'));
        $this->load->model(frontend_model_path('ServicesModel'));
        $this->load->model(frontend_model_path('BannersModel'));
        $this->load->model(frontend_model_path('PagesModel'));
        $this->load->model(admin_url_string('ConfigurationModel'));
        $main['page_scripts']='';
        $this->pagetitle = 'Quality Profile';
        $page=$this->PagesModel->get_row_cond(array('page_key'=>'QUALITY_PROFILE_PAGE'));
        if($page){
            if($page->meta_title!=''){ $this->pagetitle = $page->meta_title;}
            if($page->meta_keywords!=''){ $this->metakeywords = $page->meta_keywords;}  
            if($page->meta_desc!=''){ $this->metadesc = $page->meta_desc;}
        }
		$main['meta']=$this->frontmetahead();
        $content['page']=$page;
		$headerbanner=$page->image;
        $content['products']=$this->ProductsModel->get_active();
        $content['services']=$this->ServicesModel->get_active();
       //$content['config_itrade']=$this->ConfigurationModel->get_row_cond(array('config_key'=>'home_why_itrade_key'));
		$content['config_request']=$this->ConfigurationModel->get_row_cond(array('config_key'=>'request'));
		$frontcontent=$this->load->view('frontend/pages/page',$content,true);
		$content['requestdemoform']=$this->load->view('frontend/request/request',$content,true);
		$content['newsletterform']=$this->load->view('frontend/newsletter/newsletterform','',true);
        $footercontent = $this->load->view('frontend/content/footerhome',$content,true);
		$main['contents']=$this->frontcontent($frontcontent,false);
		$main['header']=$this->frontheader($headerbanner);
		$main['footer']=$this->fronthomefooter($footercontent);
		$this->load->view('frontend/main',$main);
	}
	public function whatwedo()
	{
		$this->output->cache($this->settings['CACHE_TIME']);
		$content['success']='';
		$content['newslettersuccess']='';
        $this->load->model(frontend_model_path('ProductsModel'));
        $this->load->model(frontend_model_path('ServicesModel'));
        $this->load->model(frontend_model_path('BannersModel'));
        $this->load->model(frontend_model_path('PagesModel'));
        $this->load->model(admin_url_string('ConfigurationModel'));
        $main['page_scripts']='';
        $this->pagetitle = 'What We Do';
        $page=$this->PagesModel->get_row_cond(array('page_key'=>'WHATWEDO_KEY'));
        if($page){
            if($page->meta_title!=''){ $this->pagetitle = $page->meta_title;}
            if($page->meta_keywords!=''){ $this->metakeywords = $page->meta_keywords;}  
            if($page->meta_desc!=''){ $this->metadesc = $page->meta_desc;}
        }
		$main['meta']=$this->frontmetahead();
        $content['page']=$page;
		$headerbanner=$page->image;
        $content['products']=$this->ProductsModel->get_active();
        $content['services']=$this->ServicesModel->get_active();
       //$content['config_itrade']=$this->ConfigurationModel->get_row_cond(array('config_key'=>'home_why_itrade_key'));
		$content['config_request']=$this->ConfigurationModel->get_row_cond(array('config_key'=>'request'));
		$frontcontent=$this->load->view('frontend/pages/page',$content,true);
		$content['requestdemoform']=$this->load->view('frontend/request/request',$content,true);
		$content['newsletterform']=$this->load->view('frontend/newsletter/newsletterform','',true);
        $footercontent = $this->load->view('frontend/content/footerhome',$content,true);
		$main['contents']=$this->frontcontent($frontcontent,false);
		$main['header']=$this->frontheader($headerbanner);
		$main['footer']=$this->fronthomefooter($footercontent);
		$this->load->view('frontend/main',$main);
	}
	}
    
