<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CSFront_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{ 
		$this->output->cache($this->settings['CACHE_TIME']);
        $main['page_scripts']='';
        $this->pagetitle = 'Home';
		$content['success']='';
		$content['newslettersuccess']='';
        $this->load->model(frontend_model_path('BannersModel'));
        $this->load->model(frontend_model_path('ProductsModel'));
        $this->load->model(frontend_model_path('ServicesModel'));
        $this->load->model(frontend_model_path('ClientsModel'));
        $this->load->model(admin_url_string('ConfigurationModel'));
        $main['meta']=$this->frontmetahead();
        
        $content['banners']=$this->BannersModel->get_active();
		$content['products']=$this->ProductsModel->get_array_limit(6);
		$content['services']=$this->ServicesModel->get_array_limit(8);
		$content['clients']=$this->ClientsModel->get_array_limit(15);
		$content['config_product']=$this->ConfigurationModel->get_row_cond(array('config_key'=>'home_product_key'));
		$content['config_service']=$this->ConfigurationModel->get_row_cond(array('config_key'=>'home_services_key'));
		$content['config_client']=$this->ConfigurationModel->get_row_cond(array('config_key'=>'home_client_key'));
	    $content['config_itrade']=$this->ConfigurationModel->get_row_cond(array('config_key'=>'home_why_itrade_key'));
		$content['config_request']=$this->ConfigurationModel->get_row_cond(array('config_key'=>'request'));
        $frontcontent=$this->load->view('frontend/content/home',$content,true);
		$content['requestdemoform']=$this->load->view('frontend/request/request','',true);
		$content['newsletterform']=$this->load->view('frontend/newsletter/newsletterform','',true);
		
        $footercontent = $this->load->view('frontend/content/footerhome',$content,true);
		$main['contents']=$this->frontcontent($frontcontent,false);
		$main['header']=$this->fronthomeheader();
		$main['footer']=$this->fronthomefooter($footercontent);
		$this->load->view('frontend/main',$main);
	}
	function requestdemo()
	{
		$this->output->cache($this->settings['CACHE_TIME']);
		$this->load->model(frontend_model_path('RequestModel'));
        $this->load->model(admin_url_string('ConfigurationModel'));
		$var['config_request']=$this->ConfigurationModel->get_row_cond(array('config_key'=>'request'));
		$result = array();
		$this->form_validation->set_rules('name', 'Full Name', 'required');
		$this->form_validation->set_rules('email', 'Email Address', 'required|valid_email');
		$this->form_validation->set_rules('phone', 'Mobile', 'required');
		$this->form_validation->set_message('required', 'required');
		$this->form_validation->set_message('valid_email', 'invalid email');
		$this->form_validation->set_error_delimiters('<span class="validation-error red">', '</span>');
		if ($this->form_validation->run() == FALSE) {
			$var['success']='';
			$result['status'] = '0';
	  		$result['data'] = $this->load->view('frontend/request/request',$var,TRUE);
		} else {
			$data = array(
			'name' => $this->db->escape_str($this->input->post('name')),
			'email' => $this->db->escape_str($this->input->post('email')),
			'phone' => $this->db->escape_str($this->input->post('phone')),
			'request_date' => date('Y-m-d h:i:s'),
			'status'=>'1');
			$this->RequestModel->insert($data);
			$subject = '----Request For Demo----';
			$from_name=$this->db->escape_str($this->input->post('name'));
			$view['data'] = (object) $data;
			$message = $this->load->view('frontend/mail/requestmail', $view, TRUE);
			$this->sendtoadmin($this->settings['ADMIN_EMAIL'],$from_name,$attachment='',$subject,$message);
			$var['success']=$this->load->view('frontend/request/success','',TRUE);
			$result['data'] = $this->load->view('frontend/request/request',$var,TRUE);
            $result['status'] = '1';
		}
			$json=json_encode($result);
			exit($json);
		
	}
	function newsletter()
	{
		$this->output->cache($this->settings['CACHE_TIME']);
		$this->load->model(admin_url_string('NewslettersModel'));
		$result = array();
		$this->form_validation->set_rules('email', 'Email Address', 'required|valid_email');
		$this->form_validation->set_message('required', 'required');
		$this->form_validation->set_message('valid_email', 'invalid email');
		$this->form_validation->set_error_delimiters('<span class="validation-error red">', '</span>');
		if ($this->form_validation->run() == FALSE) {
			$var['newslettersuccess']='';
			$result['status'] = '0';
	  		$result['data'] = $this->load->view('frontend/newsletter/newsletterform',$var,TRUE);
		} else {
			$data = array('email' => $this->db->escape_str($this->input->post('email')),
						  'request_date' => date('Y-m-d h:i:s'),
						  'status'=>'1');
			$this->NewslettersModel->insert($data);
			$subject = 'Newsletter';
			$view['data'] = (object) $data;
			$from_name='iTrade Website';
			$message = $this->load->view('frontend/mail/newslettermail', $view, TRUE);
	$this->sendtoadmin($this->settings['ADMIN_EMAIL'],$from_name,$attachment='',$subject,$message);
			$msg['newslettersuccess']=$this->load->view('frontend/newsletter/success','',TRUE);
			$result['data'] = $this->load->view('frontend/newsletter/newsletterform',$msg,TRUE);
            $result['status'] = '1';
		}
			$json=json_encode($result);
			exit($json);
		
	}
	function popuprequestdemo()
	{
		$this->output->cache($this->settings['CACHE_TIME']);
		$this->load->model(frontend_model_path('RequestModel'));
		$result = array();
		$this->form_validation->set_rules('name', 'Full Name', 'required');
		$this->form_validation->set_rules('email', 'Email Address', 'required|valid_email');
		$this->form_validation->set_rules('phone', 'Mobile', 'required');
		$this->form_validation->set_message('required', 'required');
		$this->form_validation->set_message('valid_email', 'invalid email');
		$this->form_validation->set_error_delimiters('<span class="validation-error red">', '</span>');
		if ($this->form_validation->run() == FALSE) {
			$var['success']='';
			$result['status'] = '0';
	  		$result['data'] = $this->load->view('frontend/request/popup_request_form',$var,TRUE);
		} else {
			$data = array(
			'name' => $this->db->escape_str($this->input->post('name')),
			'email' => $this->db->escape_str($this->input->post('email')),
			'phone' => $this->db->escape_str($this->input->post('phone')),
			'request_date' => date('Y-m-d h:i:s'),
			'status'=>'1');
			$this->RequestModel->insert($data);
			$subject = 'Request For Demo';
			$view['data'] = (object) $data;
			$from_name=$this->db->escape_str($this->input->post('name'));
			$message = $this->load->view('frontend/mail/requestmail', $view, TRUE);
	$this->sendtoadmin($this->settings['ADMIN_EMAIL'],$from_name,$attachment='',$subject,$message);
			$var['success']=$this->load->view('frontend/request/success','',TRUE);
			$result['data'] = $this->load->view('frontend/request/popup_request_form',$var,TRUE);
            $result['status'] = '1';
		}
			$json=json_encode($result);
			exit($json);
		
	}
	
    
}
