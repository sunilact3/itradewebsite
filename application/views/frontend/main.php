<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php echo $meta; ?>
    <base href="<?php echo base_url('/'); ?>" />
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('public/frontend/img/favicon.ico'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/frontend/font/stylesheet.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/frontend/css/font-awesome.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/frontend/css/bootstrap.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/frontend/css/style.css'); ?>" />
	
</head>
<body>
	<div class="pageloader"></div>
	<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.3"></script>
	
	<div class="main_wrap">
    
		<?php echo $header; ?>
		<?php echo $contents; ?>
		<?php echo $footer; ?>

            

	<script type="text/javascript" src="<?php echo base_url('public/frontend/js/jquery.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('public/frontend/js/bootstrap.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('public/frontend/js/script.js'); ?>"></script>
		
		
	<script>
	$("#request_wrap" ).on( "click", "#request_form_submit", function(e) { 
		e.preventDefault();
		$(this).prop('disabled', true);
		$(this).html('"Request Submit" <i class="fa fa-refresh fa-spin fa-fw"></i>');
		var dataString = $('#request_form').serialize();
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('home/requestdemo');?>",
			data: dataString,
			dataType: "json",
			success: function(data){
			  if(data.status==1){
				$("#request_wrap").html(data.data);
				$(this).prop('disabled', false);
				$(this).html('Next');
			  }
			  else {
				$("#request_wrap").html(data.data);
				$(this).prop('disabled', false);
				$(this).html('Next');
			  }
			}
		});
	});
		
		
	$("#newsletter_wrap" ).on( "click", "#newsletter_form_submit", function(e) { 
		e.preventDefault();
		$(this).prop('disabled', true);
		$(this).html('"Submit" <i class="fa fa-refresh fa-spin fa-fw"></i>');
		var dataString = $('#newsletter_form').serialize();
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('home/newsletter');?>",
			data: dataString,
			dataType: "json",
			success: function(data){
			  if(data.status==1){
				$("#newsletter_wrap").html(data.data);
				$(this).prop('disabled', false);
				$(this).html('Next');
			  }
			  else {
				$("#newsletter_wrap").html(data.data);
				$(this).prop('disabled', false);
				$(this).html('Next');
			  }
			}
		});
	});
		
		
	$("#contactus_wrapper" ).on( "click", "#contactus_form_submit", function(e) {  
		e.preventDefault();
		$(this).prop('disabled', true);
		$(this).html('"Submit" <i class="fa fa-refresh fa-spin fa-fw"></i>');
		var dataString = $('#contactus_form').serialize();
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('contactus/contactus');?>",
			data: dataString,
			dataType: "json",
			success: function(data){
			  if(data.status==1){
				$("#contactus_wrapper").html(data.data);
				$(this).prop('disabled', false);
				$(this).html('Next');
			  }
			  else {
				$("#contactus_wrapper").html(data.data);
				$(this).prop('disabled', false);
				$(this).html('Next');
			  }
			}
		});
	});
		
	$("#popup_request_wrap" ).on( "click", "#popup_request_form_submit", function(e) { 
		e.preventDefault();
		$(this).prop('disabled', true);
		$(this).html('"Request Submit" <i class="fa fa-refresh fa-spin fa-fw"></i>');
		var dataString = $('#popup_request_form').serialize();
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('home/popuprequestdemo');?>",
			data: dataString,
			dataType: "json",
			success: function(data){
			  if(data.status==1){
				$("#popup_request_wrap").html(data.data);
				$(this).prop('disabled', false);
				$(this).html('Next');
			  }
			  else {
				$("#popup_request_wrap").html(data.data);
				$(this).prop('disabled', false);
				$(this).html('Next');
			  }
			}
		});
	});
	</script>
		
	
		
 <?php /* <script type="text/javascript" src="<?php echo base_url('public/frontend/js/instafeed.min.js'); ?>"></script>
  <script>
    var feed = new Instafeed({
      get: 'user',
      userId: '<?php echo $this->settings['INSTAGRAM_USER_ID'] ?>', // Ex: 1374300081
      accessToken: '<?php echo $this->settings['INSTAGRAM_ACCESS_TOKEN'] ?>',
	  limit:18
		
    });
    feed.run();
  </script> */ ?>
 	</div>
        <script>
			$(".p").click(function(){
			  document.getElementById("about_video").style.backgroundImage = "none";
			  document.getElementById("iframevideo").style.display = "block";
			  document.getElementById("ybutton").style.display = "none";
			  document.getElementById("pro_caption").style.display = "none";
			  document.getElementById("about_video").style.padding = "0";
    			$("#iframevideo")[0].src += "?autoplay=1";
			}); 
		</script>
	<script type="text/javascript">window.PixleeAsyncInit = function() {Pixlee.init({apiKey:'d4qRx3uTf69TDSnixzWi'});Pixlee.addSimpleWidget({widgetId:'23104'});};</script><script src="//instafeed.assets.pixlee.com/assets/pixlee_widget_1_0_0.js"></script>
</body>
</html>