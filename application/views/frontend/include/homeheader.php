
        <?php echo $headerbar; ?>    
            
            
            <!---   Slider   --->    

            <div class="slider_main">
                <div class="Carousel">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
                        <ol class="carousel-indicators">
							<?php for($i=0;$i<count($banners);$i++) { ?>
                            <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" class="<?php if($i==0) echo ' active'; ?>">
                                <div class="asdf"></div>
                            </li>
							<?php };?>
                        </ol>
                        <div class="carousel-inner">
							<?php foreach($banners as $i=>$banner): ?>
                            <div class="item <?php if($i==0) echo ' active'; ?> ">
                                <img class="slider_img" src="<?php if($banner['image']!='') echo base_url('public/uploads/banners/'.$banner['image']); else echo base_url('public/frontend/images/slider-01.jpg'); ?>" style="width:100%">
                                <div class="container">
                                    <div class="caption">
                                        <h1>
                                            <?php echo $banner['title']; ?>
                                        </h1>
                                        <p>
                                            <?php echo $banner['caption']; ?>
                                        </p>
                                        <div class="slider_btn">
                                            <a href="<?php echo $banner['banner_link']; ?>" target="_blank">
                                               Learn More &nbsp; <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                           </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
							<?php endforeach; ?>
                        </div>
                    </div>
                </div> 
            </div>

            <!---   Slider   --->     
            
            
            
            <!---   Offers   --->      

            <div class="offers">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <div style="display: inline-block; border-bottom: 4px solid rgb(255, 255, 255); margin-bottom: 20px;">
                                <h3>
                                    special offers
                                </h3>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="row">
                                <div class="ofr_btls">
                                    <div class="col-sm-5">
                                        <h4>
                                            Simple solution for your business
                                        </h4>
                                        <img class="ofr_logo" src="<?php echo base_url('public/frontend/img/ofr-logo.png'); ?>">
                                    </div>
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-5">
                                        <h1>
                                            <?php echo $this->settings['OFFER_PERCENTAGE']; ?>
                                        </h1>
                                        <small>
                                            <?php echo $this->settings['OFFER_DESC']; ?>
                                        </small>
                                        <div class="ofr_btn">
                                            <a href="<?php echo site_url('contactus') ?>">
                                                Contact now
                                            </a>
                                        </div>
                                    </div>
                                    <div style="clear:both"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <!---   Offers   --->