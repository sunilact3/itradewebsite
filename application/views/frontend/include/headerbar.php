       
<!---   Nav Bar   --->

<div class="nav_bar nav_bar_home">
	<nav class="navbar navbarhome">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="<?php echo site_url(); ?>">
				<img class="logo" src="<?php echo base_url('public/frontend/img/logo.png'); ?>">
			</a>
		</div>
		<div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="javascript:void(0);"><span> <img src="<?php echo base_url('public/frontend/img/phone-icon.png'); ?>"> </span> &nbsp; <?php echo $this->settings['PHONE_ONE']; ?></a></li>
				<li><a href="javascript:void(0);"><span> <img src="<?php echo base_url('public/frontend/img/msg-icon.png'); ?>"> </span> &nbsp; <?php echo $this->settings['ADMIN_EMAIL']; ?></a></li>
				<li><a href="<?php echo site_url('aboutus') ?>">About Us</a></li>
				<li><a href="<?php echo site_url('products') ?>">Products</a></li>
				<li><a href="<?php echo site_url('pages/whatwedo') ?>">what we do</a></li>
				<li><a href="<?php echo site_url('contactus') ?>">contact us</a></li>
				<li><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">request quote</button>
				</li>
			</ul>
		</div>
	</nav>
</div>
<!---   Nav Bar   --->

<!-- Modal -->
<div style="position:relative;">
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Request a Quote</h4>
        </div>
        <div class="modal-body" id="popup_request_wrap">
                            
                            <div class="row popup-form">
                                <?php echo form_open('',array('id'=>'popup_request_form','class'=>'')); ?>
                                    <div class="col-sm-6 req-container">
                                        <input class="reqst_form" type="text" name="name" value="" placeholder="Your Name">
                						<?php echo form_error('name'); ?>
                                    </div>
                                    <div class="col-sm-6 req-container">
                                        <input class="reqst_form req-container" type="text" name="email" value="" placeholder="Your Email ID">
                						<?php echo form_error('email'); ?>
                                    </div>
                                    <div class="col-sm-6 req-container">
                                        <input class="reqst_form req-container" type="text" name="phone" value="" placeholder="Your Contact Number">
                						<?php echo form_error('phone'); ?>
                                    </div>
                                    <div class="col-sm-6 req-container">
                                        <button class="reqst_form_btn" id="popup_request_form_submit">
                                            Request Now
                                        </button>
                                    </div>
                                <?php echo form_close(); ?>
                            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          
        </div>
      </div>
      
    </div>
  </div>
	</div>