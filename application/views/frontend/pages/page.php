

<div class="inner_about">
                <div class="container">              
                    <div class="pro_caption">                      
                        <h1>
                            <?php echo $page->title; ?>
                        </h1>
                        <p>
                             <?php echo $page->short_desc; ?> 
                        </p>
                        <div class="pro_caption_line"></div>
                    </div>
                    <div class="row inner_about_cnt">
                        <div class="col-sm-12">
                            <p>
                                <?php echo $page->body; ?> 
                            </p>
                        </div>
                    </div>
                </div>  
            </div>
            
            <!---   Inner About   --->