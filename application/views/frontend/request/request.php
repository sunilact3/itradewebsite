
                <div class="container">
                    <div class="row">
                        <div class="col-md-7 success-container">
							<h6 class="home-success"><?php echo $success; ?></h6>
                            <h3>
                                <?php echo $config_request->title; ?>
                            </h3>
                            <p>
                                <?php echo $config_request->short_desc; ?>
                            </p>
                            
                            <div class="row" >
                                <?php echo form_open('',array('id'=>'request_form','class'=>'')); ?>
                                    <div class="col-sm-6 req-container">
                                        <input class="reqst_form" type="text" name="name" value="" placeholder="Your Name">
                						<?php echo form_error('name'); ?>
                                    </div>
                                    <div class="col-sm-6 req-container">
                                        <input class="reqst_form req-container" type="text" name="email" value="" placeholder="Your Email ID">
                						<?php echo form_error('email'); ?>
                                    </div>
                                    <div class="col-sm-6 req-container">
                                        <input class="reqst_form req-container" type="text" name="phone" value="" placeholder="Your Contact Number">
                						<?php echo form_error('phone'); ?>
                                    </div>
                                    <div class="col-sm-6 req-container">
                                        <button class="reqst_form_btn" id="request_form_submit">
                                            Request Now
                                        </button>
                                    </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>