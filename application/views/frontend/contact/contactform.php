
                <div class="container success-msg-cont">
                    <div class="pro_caption">   <div class="success-msg"><?php echo $success_contact; ?></div>                   
                        <h1>
                            write to us
                        </h1>
                    </div>
                    <?php echo form_open('',array('id'=>'contactus_form','class'=>'')); ?>
                        <div class="row">
                            <div class="col-md-4 col-sm-6">
                                <p>
                                    Full Name
                                </p>
								<div class="contact-input">
                                <input class="inner_contact_form_input"  type="text" name="name" value="" placeholder="Please Enter your name here">
								<?php echo form_error('name'); ?></div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <p>
                                    Email Address
                                </p>
								<div class="contact-input">
                                <input class="inner_contact_form_input"  type="text" name="email" value="" placeholder="Please Enter a valid email address">
								<?php echo form_error('email'); ?>
								</div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <p>
                                    Contact Number
                                </p>
								<div class="contact-input">
                                <input class="inner_contact_form_input"  type="text" name="phone" value="" placeholder="Please Enter your contact number">
								<?php echo form_error('phone'); ?>
								</div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <p>
                                    Country
                                </p>
								<div class="contact-input">
              					<?php echo form_error('country'); ?>
                                <select class="inner_contact_form_input" name="country">
                                    <option value="">-- Select --</option>
									<?php foreach($countries as $country): ?>
                                    <option value="<?php echo $country;?>"><?php echo $country;?></option>
									<?php endforeach; ?>
                                </select>
								</div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <p>
                                    City
                                </p>
								<div class="contact-input">
                                <input class="inner_contact_form_input"  type="text" name="city" value="" placeholder="Please enter city here">
								</div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <p>
                                    Lead Type
                                </p>
								<div class="contact-input">
                                <select class="inner_contact_form_input" name="lead_type">
                                    <option value="select">-- Lead Type --</option>
									<?php foreach($leads as $lead): ?>
                                    <option value="<?php echo $lead;?>"><?php echo $lead;?></option>
									<?php endforeach; ?>
                                </select><?php echo form_error('lead_type'); ?>
								</div>
                            </div>
                        </div>
                        <p>
                            Your Message Here
                        </p>
						<div class="contact-input">
                        <textarea rows="5" class="inner_contact_form_textarea"  type="text" name="message" value="" placeholder="Your message here"></textarea><?php echo form_error('message'); ?>
						</div>
                        <div class="row">
                            <div class="col-sm-9">
					<?php echo form_error('g-recaptcha-response'); ?>
					<div class="g-recaptcha" name="g-recaptcha-response" data-sitekey="<?php echo $site_key; ?>"></div>
                            </div>
                            <div class="col-sm-3">
                                <div class="inner_cntact_btn">
                                    <a href="#" id="contactus_form_submit">
                                        send
                                    </a>
                                </div>
                            </div>
                        </div>
                    	<?php echo form_close(); ?>
                </div>
<script src='https://www.google.com/recaptcha/api.js'></script>