<div class="inner_about">
	<div class="container">              
		<div class="pro_caption">                      
			<h1>
				<?php echo $services_detail->title; ?>
			</h1>
			<p>
				 <?php echo $services_detail->short_desc; ?> 
			</p>
			<div class="pro_caption_line"></div>
		</div>

		<div class="row inner_about_cnt">
			<div class="col-sm-6">
				<img width="100%;" src="<?php if($services_detail->image!='') echo base_url('public/uploads/services/'.$services_detail->image); else  echo base_url('public/frontend/images/noimage.jpg'); ?>">
			</div>
			<div class="col-sm-6">
				<p>
					<?php echo $services_detail->description; ?> 
				</p>
			</div>
		</div>

	</div>  
</div>