            
           <!---   Inner Contact   --->      

            <div class="inner_contact">
                <div class="container">              
                    <div class="pro_caption">                      
                        <h1>
                            <?php echo $page->title; ?> 
                        </h1>
                        <p>
                            <?php echo $page->short_desc; ?> 
                        </p>
                        <div class="pro_caption_line"></div>
                    </div>
                    <div class="row inner_Contact_cnt">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-4">
                            <img class="contact_msg_icon" src="<?php echo base_url('public/frontend/img/contact_msg_icon.png'); ?>">
                            <p class="inner_Contact_adrs">
                                <?php echo $this->settings['ADDRESS_KEY']; ?>
                            </p>
                        </div>
                        <div class="col-sm-4">
                            <img class="contact_ph_icon" src="<?php echo base_url('public/frontend/img/contact_ph_icon.png'); ?>">
                            <h4 class="inner_Contact_phone">
                               <?php echo $this->settings['PHONE_ONE']; ?>
                            </h4>
                            <p class="inner_Contact_mail">
                                <?php echo $this->settings['ADMIN_EMAIL']; ?>
                            </p>
                        </div>
                        <div class="col-sm-2"></div>
                    </div>
                </div>  
            </div>
            
            <!---   Inner Contact   --->
            
            
            <!---   Inner Contact Form   --->      

            <div class="inner_contact_form" id="contactus_wrapper">
            <?php echo $contactform; ?>
            </div>
            
            <!---   Inner Contact Form   --->
            
            
            
 