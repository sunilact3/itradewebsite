 <!---   Products   --->      

            <div class="products">
                <div class="container">
                    <div class="pro_caption">                      
                        <h1>
                            <?php echo $config_product->title; ?>
                        </h1>
                        <p>
                            <?php echo $config_product->short_desc; ?>
                        </p>
                        <div class="pro_caption_line"></div>
                    </div>
                    <ul class="pro_cnt_items">
					<?php foreach($products as $product): ?>	
                        <li>
                            <div class="pro_items">
                                <img src="<?php if($product['icon']!='') echo base_url('public/uploads/products/icon/'.$product['icon']); else  echo base_url('public/frontend/images/noimage.jpg');   ?>">
                                <h4>
                                    <?php echo $product['title']; ?>
                                </h4>
                                <p>
                                    <?php echo $product['short_desc']; ?>
                                </p>
                            </div>
                        </li>
					<?php endforeach; ?>
                    </ul>
                </div>   
            </div>
            
            <!---   Products   --->
            
            
            
            <!---   Services   --->      

            <div class="services">
                <div class="container">
                    <div class="pro_caption">                      
                        <h1>
                            <?php echo $config_service->title; ?>
                        </h1>
                        <p>
                            <?php echo $config_service->short_desc; ?>
                        </p>
                        <div class="pro_caption_line"></div>
                    </div>
                </div>
                <div class="services_items">
					<?php foreach($services as $service): ?>
					<a href="<?php echo site_url('services/detail/'.$service['slug']); ?>" >
                    <div class="services_item">
                        <img class="srv_img" src="<?php if($service['image']!='') echo base_url('public/uploads/services/'.$service['image']); else  echo base_url('public/frontend/images/noimage.jpg'); ?>">
                        <div class="srv_overlay">
                            <h4>
                                <?php echo $service['title']; ?>
                            </h4>
                            <img class="srv_overlay_icon" src="<?php echo base_url('public/frontend/img/srv_overlay_icon.png'); ?>">
                        </div>
                    </div>
					</a>
					<?php endforeach; ?>
                    <div style="clear:both"></div>
                </div>
            </div>
            
            <!---   Services   --->
            
            
            
            <!---   Clients   --->      

            <div class="clients">
                <div class="container">
                    <div class="pro_caption">                      
                        <h1>
                            <?php echo $config_client->title; ?>
                        </h1>
                        <p>
                           <?php echo $config_client->short_desc; ?>
                        </p>
                        <div class="pro_caption_line"></div>
                    </div>
                </div>
                <div class="clients_items">
                    <div class="container">
					<?php foreach($clients as $client): ?>
						<a href="<?php if($client['link']!='') { echo $client['link']; }  ?>" target="_blank">
                        <div class="clients_item"> <img src="<?php if($client['image']!='') echo base_url('public/uploads/clients/'.$client['image']); else echo base_url('public/frontend/images/noimage.jpg'); ?>"> </div>
						</a>
					<?php endforeach; ?>
                    </div>
                </div>
            </div>
            
            <!---   Clients   --->
            
            
            
            <!---   Social Media   --->      

            <div class="soc_media">
               <?php /* <div class="instagram_media">
                    <img class="media_icon" src="<?php echo base_url('public/frontend/img/insta_icon.png'); ?>">
                    <div class="insta_media">
                     <div id="instafeed" class="instafeed"></div>
                    </div>
                </div> */ ?>
              <div class="instagram_media">
                    <img class="media_icon" src="<?php echo base_url('public/frontend/img/insta_icon.png'); ?>">
                    <div class="insta_media">
                     <div id="pixlee_container"></div>
                    </div>
                </div>
				
                <div class="facebok_media">
                    <img class="media_icon" src="<?php echo base_url('public/frontend/img/fb_icon.png'); ?>">
                    <div class="fb_media">
                        <div class="fb-page" data-href="https://www.facebook.com/iTradeerpSolutions/" data-tabs="timeline" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/iTradeerpSolutions/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/iTradeerpSolutions/">ITrade ERP Solutions</a></blockquote></div>
                    </div>
                </div>
                <div style="clear:both"></div>
            </div>
            
            <!---   Social Media   --->   
            
            
            
            <!---   ITrade   --->      

            <div class="itrade">
                <div class="container">
                    <div class="pro_caption">                      
                        <h1>
                           <?php echo $config_itrade->title; ?>
                        </h1>
                        <p>
                            <?php echo $config_itrade->short_desc; ?>
                        </p>
                        <div class="pro_caption_line"></div>
                    </div>
                    <div class="itrade_list_left">
                        <div class="itrade_list_left_1">
                            <img src="<?php echo base_url('public/frontend/img/itrade_01.png'); ?>">
                            <h4>
                                Lorem ipsum 
                            </h4>
                            <p>
                                duafjLorem ipsum dolor sit amet consect ultrices ijncivamus vaacfd 
                            </p>
                        </div>
                        <div class="itrade_list_left_1">
                            <img src="<?php echo base_url('public/frontend/img/itrade_02.png'); ?>">
                            <h4>
                                Lorem ipsum 
                            </h4>
                            <p>
                                duafjLorem ipsum dolor sit amet consect ultrices ijncivamus vaacfd 
                            </p>
                        </div>
                        <div class="itrade_list_left_1">
                            <img src="<?php echo base_url('public/frontend/img/itrade_03.png'); ?>">
                            <h4>
                                Lorem ipsum 
                            </h4>
                            <p>
                                duafjLorem ipsum dolor sit amet consect ultrices ijncivamus vaacfd 
                            </p>
                        </div>
                    </div>
                    <div class="itrade_list_center">
                        <img class="itrade_img" src="<?php echo base_url('public/frontend/img/itrade_img.png'); ?>">
                    </div>
                    <div class="itrade_list_right">
                        <div class="itrade_list_right_1">
                            <img src="<?php echo base_url('public/frontend/img/itrade_04.png'); ?>">
                            <h4>
                                Lorem ipsum 
                            </h4>
                            <p>
                                duafjLorem ipsum dolor sit amet consect ultrices ijncivamus vaacfd 
                            </p>
                        </div>
                        <div class="itrade_list_right_1">
                            <img src="<?php echo base_url('public/frontend/img/itrade_05.png'); ?>">
                            <h4>
                                Lorem ipsum 
                            </h4>
                            <p>
                                duafjLorem ipsum dolor sit amet consect ultrices ijncivamus vaacfd 
                            </p>
                        </div>
                        <div class="itrade_list_right_1">
                            <img src="<?php echo base_url('public/frontend/img/itrade_06.png'); ?>">
                            <h4>
                                Lorem ipsum 
                            </h4>
                            <p>
                                duafjLorem ipsum dolor sit amet consect ultrices ijncivamus vaacfd 
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            
            <!---   ITrade   --->   
            