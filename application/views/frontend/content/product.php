            
            <!---   Inner Product   --->
            
            <div class="inner_products_drop">
                <div class="container">
                    <div class="pro_caption">                      
                        <h1>
                           <?php echo $page->title; ?>
                        </h1>
                        <p>
                             <?php echo $page->short_desc; ?> 
                        </p>
                        <div class="pro_caption_line"></div>
                    </div>
                </div>
                <div class="extra_pad">
                    <section class="image-grid">
                        <!-- item 01 -->
						<?php foreach($products as $product): ?>
                        <div class="image__cell is-collapsed">
                            <div class="image--basic open_img_first">
                                <a id="<?php echo 'expand-jump-'.$product['slug'];?>" href="<?php echo site_url('products').'#expand-jump-'.$product['slug'];?>">
                                    <div class="pro_items">
                                        <img src="<?php if($product['icon']!='') echo base_url('public/uploads/products/icon/'.$product['icon']); else  echo base_url('public/frontend/images/noimage.jpg');   ?>">
                                        <h4>
                                            <?php echo $product['title']; ?>
                                        </h4>
                                        <p>
                                            <?php echo $product['short_desc']; ?>
                                        </p>
                                    </div>
                                </a>
                                <div class="<?php echo 'arrow--up arrow--up--'.$product['slug']; ?>"></div>
                            </div>
                            <div class="image--expand">
                                <div class="pro_dtls_open">
                                    <img class="pro_dtls_open_img" src="<?php echo base_url('public/frontend/img/about_banner.jpg'); ?>">
                                    <div id="<?php echo site_url('products').'#expand-jump-'.$product['slug'];?>" class="pro_dtls_open_overlay pro_dtls_open_overlay_02">
                                        <div class="pro_dtls_cnt">
                                            <h4>
                                                <?php echo $product['title']; ?>
                                            </h4>
                                            <p>
                                               <?php echo $product['description']; ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						<?php endforeach; ?>                       
                        <div style="clear:both"></div>
                    </section>
                </div>
            </div>

            <!---   Inner Product   --->
            
            
            
 