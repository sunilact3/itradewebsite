 <!---   Request   --->      

            <div class="request" id="request_wrap">
                <?php echo $requestdemoform; ?>
            </div>
            
            <!---   Request   --->   
            
            
            
            <!---   Footer   --->      

            <div class="footer">
                <div class="container">
                    <div class="footer_cnt">
                        <div class="footer_about">
                            <h4> About  </h4>
                            <ul>
                                <li> <a href="<?php echo site_url('pages/profile') ?>"> Company Profile </a> </li>
                                <li> <a href="<?php echo site_url('pages/whyus') ?>"> Why Us? </a> </li>
                                <li> <a href="<?php echo site_url('pages/quality') ?>"> Quality Profile </a> </li>
                            </ul>
                        </div>
                        <div class="footer_product" id="footer_product">
                            <h4> Products </h4>
                            <ul>
								<?php foreach($products as $product): ?>
								<div class="image--basic">
                                <li> <a href="<?php echo site_url('products').'#expand-jump-'.$product['slug'];?>"><?php echo $product['title']; ?></a> </li>
								</div>
								<?php endforeach; ?>
                            </ul>
                        </div>
                        <div class="footer_srv_1">
                            <h4> Services </h4>
                            <ul>
							<?php foreach($services as $service): ?>
                                <li> <a href="<?php echo site_url('services/detail/'.$service['slug']); ?>"><?php echo $service['title']; ?></a> </li>
							<?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                    <div class="newsletter" id="newslettersucces">
                        <h3> Newsletter </h3>
                        <p> Join our newsletter to keep be informed about offers and news </p>
                        <div class="row" id="newsletter_wrap">
						
                            <?php echo $newsletterform; ?>
                        </div>
                        <ul class="soc_icon">
                            <li> <a href="<?php echo $this->settings['FACEBOOK_LINK']; ?>" target="_blank"><img src="<?php echo base_url('public/frontend/img/soc_01.png'); ?>"></a> </li>
                            <li> <a href="<?php echo $this->settings['TWITTER_LINK']; ?>" target="_blank"><img src="<?php echo base_url('public/frontend/img/soc_02.png'); ?>"></a> </li>
                            <li> <a href="<?php echo $this->settings['LINKED_IN']; ?>" target="_blank"><img src="<?php echo base_url('public/frontend/img/soc_03.png'); ?>"></a> </li>
                            <li> <a href="<?php echo $this->settings['INSTA_LINK']; ?>" target="_blank"><img src="<?php echo base_url('public/frontend/img/soc_04.png'); ?>"></a> </li>
                            <li> <a href="<?php echo $this->settings['YOUTUBE_LINK']; ?>" target="_blank"><img src="<?php echo base_url('public/frontend/img/soc_05.png'); ?>"></a> </li>
                        </ul>
                    </div>
                    <div style="clear:both"></div>
                    <div class="copy">
                        <small>© 2019 itrade.ae. All Rights Reserved. </small> &nbsp; &nbsp; &nbsp; 
                        <small><a href="<?php echo site_url('pages/privacy') ?>">Privacy policy</a> &nbsp; &nbsp; l &nbsp; &nbsp; <a href="<?php echo site_url('pages/terms') ?>">Terms and conditions</a></small>
                    </div>
                </div>
            </div>
            
            <!---   Footer   --->   
