

<div class="inner_about">
                <div class="container">              
                    <div class="pro_caption">                      
                        <h1>
                            <?php echo $page->title; ?>
                        </h1>
                        <p>
                             <?php echo $page->short_desc; ?> 
                        </p>
                        <div class="pro_caption_line"></div>
                    </div>
                    <div class="row inner_about_cnt">
                        <div class="col-sm-6">
                            <h4>
                                iTrade ERP Solutions is the most sophisticated, powerful and user-friendly software with advance features and supports different types of trading and retail environment which can also be further customized to your business operations and needs.
                            </h4>
                            <p>
                                iTrade ERP Solutions FZCO is one of the leading Customized ERP solution provider for retail and trading industry in the middle east. We are located in Dubai Silicon Oasis and have operations in UAE, Qatar and Oman in the middle-east and having installations of iTrade software in almost all of the GCC countries. The company was established a decade ago to provide well organized, dynamic and robust Software solution to various kinds of business.
                            </p>
                        </div>
                        <div class="col-sm-6">
                            <p>
                                Since there has been a major breakthrough in the Technology and economic situation in the Middle East, there was a tremendous need for Robust Software solutions which can be customized as per the operations of the business nature. In this context, the employers of these countries are now switching their focus to adopt a system where they can manage all the day to day procedures through a single ERP system and monitor all the activities of the company. So to guarantee appropriate selection and convenient deployment of the Software system, major companies generally select an organization that are considerably, professionally experienced and thoroughly familiarized with developing Customized Software solutions in co-relation with swiftness and efficiency.
                            </p>
                        </div>
                    </div>
                </div>  
            </div>
            
            <!---   Inner About   --->
            
            
            
            <!---   Mission Vision Values   --->      

            <div class="mission">
                <div class="container">
                    <div class="mission_all">
                        <div class="mison_rount mission_cnt">
                            <div class="mission_cnnt">
                                <h4>
                                    mission
                                </h4>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo 
                                </p>
                            </div>
                        </div>
                        <div class="mison_rount vision_cnt">
                            <div class="mission_cnnt">
                                <h4>
                                    Vision
                                </h4>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo 
                                </p>
                            </div>
                        </div>
                        <div class="mison_rount value_cnt">
                            <div class="mission_cnnt">
                                <h4>
                                    values
                                </h4>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo 
                                </p>
                            </div>
                        </div>
						<div style="clear:both"></div>
                    </div>
                </div>
            </div>
            
            <!---   Mission Vision Values   --->
            
            
            <?php /*
            <!---   About Video   --->      

            <div class="about_video">
				<div class="video_link">
					<iframe class="video_iframe" src="https://www.youtube.com/embed/VaRO5-V1uK0" frameborder="0" allowfullscreen></iframe>
				</div>
				
				
				
				
				
                
            </div>
            
            <!---   About Video   --->
*/ ?>
 <!---   About Video   --->      

            <div id="about_video" class="about_video">
                <div class="container">              
                    <div id="pro_caption" class="pro_caption">                      
                        <h1>
                            What is itrade erp?
                        </h1>
                        <p>
                           A video presentation about the software
                        </p>
                    </div>
                    
                </div>
				<div class="video_link">
                        <a class="p" href="javascript:void(0);">
                            <i id="ybutton" class="youtube_icon fa fa-youtube-play"></i>
							<iframe class="video_iframe" src="https://www.youtube.com/embed/VaRO5-V1uK0" frameborder="0" allowfullscreen style="display:none;" id="iframevideo"></iframe>
                        </a>
                    </div>
            </div>
            
            <!---   About Video   --->

 <!---   ITrade   --->      

            <div class="itrade">
                <div class="container">
                    <div class="pro_caption">                      
                        <h1>
                           <?php echo $config_itrade->title; ?>

                        </h1>
                        <p>
                             <?php echo $config_itrade->short_desc; ?>
                        </p>
                        <div class="pro_caption_line"></div>
                    </div>
                    <div class="itrade_list_left">
                        <div class="itrade_list_left_1">
                            <img src="<?php echo base_url('public/frontend/img/itrade_01.png') ?>">
                            <h4>
                                Lorem ipsum 
                            </h4>
                            <p>
                                duafjLorem ipsum dolor sit amet consect ultrices ijncivamus vaacfd 
                            </p>
                        </div>
                        <div class="itrade_list_left_1">
                            <img src="<?php echo base_url('public/frontend/img/itrade_02.png') ?>">
                            <h4>
                                Lorem ipsum 
                            </h4>
                            <p>
                                duafjLorem ipsum dolor sit amet consect ultrices ijncivamus vaacfd 
                            </p>
                        </div>
                        <div class="itrade_list_left_1">
                            <img src="<?php echo base_url('public/frontend/img/itrade_03.png') ?>">
                            <h4>
                                Lorem ipsum 
                            </h4>
                            <p>
                                duafjLorem ipsum dolor sit amet consect ultrices ijncivamus vaacfd 
                            </p>
                        </div>
                    </div>
                    <div class="itrade_list_center">
                        <img class="itrade_img" src="<?php echo base_url('public/frontend/img/itrade_img.png') ?>">
                    </div>
                    <div class="itrade_list_right">
                        <div class="itrade_list_right_1">
                            <img src="<?php echo base_url('public/frontend/img/itrade_04.png') ?>">
                            <h4>
                                Lorem ipsum 
                            </h4>
                            <p>
                                duafjLorem ipsum dolor sit amet consect ultrices ijncivamus vaacfd 
                            </p>
                        </div>
                        <div class="itrade_list_right_1">
                            <img src="<?php echo base_url('public/frontend/img/itrade_05.png') ?>">
                            <h4>
                                Lorem ipsum 
                            </h4>
                            <p>
                                duafjLorem ipsum dolor sit amet consect ultrices ijncivamus vaacfd 
                            </p>
                        </div>
                        <div class="itrade_list_right_1">
                            <img src="<?php echo base_url('public/frontend/img/itrade_06.png') ?>">
                            <h4>
                                Lorem ipsum 
                            </h4>
                            <p>
                                duafjLorem ipsum dolor sit amet consect ultrices ijncivamus vaacfd 
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            
            <!---   ITrade   --->   