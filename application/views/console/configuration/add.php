<div class="right_col" role="main">
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Add Config</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br />
        <?php
        $attributes = array('class' => 'form-vertical form-label-right', 'id' => 'config-add');
        echo form_open(admin_url_string('configuration/add/'),$attributes);
        ?>
          <div class="form-group">
            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="label">Title<span class="required">*</span>
            </label>
            <div class="col-md-12 col-sm-12 col-xs-12">
            <?php echo form_error('title'); ?>
              <input type="text" id="title" name="title" required="required" value="<?php echo set_value('title'); ?>" class="form-control col-md-7 col-xs-12">
            </div>
            <div class="clearfix"></div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="config_key">Config Key<span class="required">*</span>
            </label>
            <div class="col-md-12 col-sm-12 col-xs-12">
            <?php echo form_error('config_key'); ?>
              <input type="text" id="config_key" name="config_key" required="required" value="<?php echo set_value('config_key'); ?>" class="form-control col-md-7 col-xs-12">
            </div>
            <div class="clearfix"></div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="short_desc">Short Desc
            </label>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <textarea id="short_desc" class="form-control col-md-7 col-xs-12" name="short_desc"><?php echo set_value('short_desc'); ?></textarea>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-12 col-sm-12 col-xs-12">Status <span class="required">*</span></label>
            <div class="col-md-12 col-sm-12 col-xs-12">
            <?php echo form_error('status'); ?>
              <div id="status" class="btn-group" data-toggle="buttons">
                <label class="btn btn-default <?php if(set_value('status')=='1') { echo 'active'; } ?>">
                  <input type="radio" required="required" name="status" value="1" <?php if(set_value('status')=='1') { echo 'checked="checked"'; } ?> /> &nbsp; Enabled &nbsp;
                </label>
                <label class="btn btn-default <?php if(set_value('status')=='0') { echo 'active'; } ?>">
                  <input type="radio" required="required" name="status" value="0" <?php if(set_value('status')=='0') { echo 'checked="checked"'; } ?> /> Disabled
                </label>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12">

              <button type="submit" class="btn btn-success">Submit</button>
              <a class="btn btn-primary" href="<?php echo admin_url('configuration/overview/'); ?>">Cancel</a>
            </div>
          </div>

        <?php echo form_close(); ?>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>
</div>
