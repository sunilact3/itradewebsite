<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
	<base href="<?php echo base_url('/'); ?>" />
    <title><?php echo $this->config->item('site_name'); ?> - Console Application</title>
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('public/console/images/favicon.ico'); ?>">
    <!-- Bootstrap -->
    <link href="<?php echo public_admin_base_url('vendors/bootstrap/dist/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo public_admin_base_url('vendors/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo public_admin_base_url('vendors/nprogress/nprogress.css'); ?>" rel="stylesheet">
	  <link href="<?php echo public_admin_base_url('vendors/select2/dist/css/select2.min.css'); ?>" rel="stylesheet">
	  <link href="<?php echo public_admin_base_url('vendors/bootstrap-daterangepicker/daterangepicker.css'); ?>" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?php echo public_admin_base_url('build/css/custom.css'); ?>" rel="stylesheet">
    <link href="<?php echo public_admin_base_url('css/celiums.css'); ?>" rel="stylesheet">
    <!-- jQuery -->
    <script src="<?php echo public_admin_base_url('vendors/jquery/dist/jquery.min.js'); ?>"></script>
  </head>

  <body class="nav-md footer_fixed">
    <div class="container body">
      <div class="main_container">
	  
		<?php echo $side_menu; ?>
		<?php echo $top_menu; ?>
		<?php echo $content; ?>
		<?php echo $footer; ?>
		
      </div>
    </div>
    <div id="confirm-delete" class="modal fade">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        Are you sure?
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Delete</button>
        <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
      </div>
    </div>
    </div>
    </div>
	  
	<div id="confirm-approve" class="modal fade">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        Are you sure?
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-primary" id="approve">Approve</button>
        <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
      </div>
    </div>
    </div>
    </div>
      
    <div id="confirm-schedule-approve" class="modal fade">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        Are you sure?
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-primary" id="schedule-approve">Approve</button>
        <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
      </div>
    </div>
    </div>
    </div>

    <div id="confirm-reject" class="modal fade">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        Are you sure?
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-primary" id="reject">Reject</button>
        <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
      </div>
    </div>
    </div>
    </div>
	  
	<div id="confirm-start" class="modal fade">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        Are you sure?
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-primary" id="start">Start</button>
        <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
      </div>
    </div>
    </div>
    </div>
	  
	<div id="confirm-stop" class="modal fade">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        Are you sure?
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-primary" id="stop">Stop</button>
        <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
      </div>
    </div>
    </div>
    </div>
	
	<div id="confirm-cancel" class="modal fade">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        Are you sure?
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-primary" id="cancel">Proceed</button>
        <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
      </div>
    </div>
    </div>
    </div>
	  
	<div id="confirm-close" class="modal fade">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        Are you sure?
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-primary" id="close">Completed</button>
        <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
      </div>
    </div>
    </div>
    </div>
	  
	 <div id="confirm-reschedule" class="modal fade">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        Are you sure?
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-primary" id="reschedule">Reschedule</button>
        <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
      </div>
    </div>
    </div>
    </div>
      
    <div id="confirm-contract" class="modal fade">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        Are you sure?
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-primary" id="contract">Proceed</button>
        <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
      </div>
    </div>
    </div>
    </div>
    
    <!-- Bootstrap -->
    <script src="<?php echo public_admin_base_url('vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
    <!-- FastClick -->  
    <script src="<?php echo public_admin_base_url('vendors/fastclick/lib/fastclick.js'); ?>"></script>
    <!-- NProgress -->
    <script src="<?php echo public_admin_base_url('vendors/nprogress/nprogress.js'); ?>"></script>
	<!-- bootstrap-progressbar -->
    <script src="<?php echo public_admin_base_url('vendors/bootstrap-progressbar/bootstrap-progressbar.min.js'); ?>" ></script>
     <!-- iCheck -->
    <script src="<?php echo public_admin_base_url('vendors/iCheck/icheck.min.js'); ?>"></script>
	  <!-- bootstrap-daterangepicker -->
	<script src="<?php echo public_admin_base_url('vendors/select2/dist/js/select2.full.min.js'); ?>"></script>
    <script src="<?php echo public_admin_base_url('vendors/moment/moment.min.js'); ?>"></script>
    <script src="<?php echo public_admin_base_url('vendors/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?php echo public_admin_base_url('build/js/custom.js'); ?>"></script>
    <script src="<?php echo public_admin_base_url('js/celiums.js'); ?>"></script>
	  
    <?php echo $page_scripts; ?>
    <canvas id="canvas" width="1" height="1">

  </body>
</html>
