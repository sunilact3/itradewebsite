<!-- page content -->
<div class="right_col" role="main">
<?php if($this->session->flashdata('message')){ ?>
  <div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
  <div class="alert alert-dismissible fade in <?php if(isset($this->session->flashdata('message')['status'])){ echo $this->session->flashdata('message')['status'];} ?>" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
      </button>
      <?php if(isset($this->session->flashdata('message')['message'])){ echo $this->session->flashdata('message')['message']; } ?>
  </div>
  </div>
  </div>
  <?php } ?>
  <div class="clearfix"></div>
  <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Request Demo</h2>
            <div class="clearfix"></div>
          </div>

          <?php
            $attributes = array('class' => 'form-horizontal form-label-left', 'id' => 'filter');
            echo form_open(admin_url_string('requestdemo/overview'),$attributes);  ?>
         
            <div class="x_content">
            <div class="table-responsive">
              <table class="table table-striped jambo_table">
                <thead>
                  <tr class="headings">
                     <th class="column-title">Name</th>
                     <th class="column-title">Email</th>
                     <th class="column-title">Phone</th>
                     <th class="column-title">Req.Date</th>
                     <th class="column-title last">Status</th>
                </thead>

                <tbody>
                <?php $i=0; foreach($requestdemos as $requestdemo):?>
                  <tr class="even pointer">
                    <td><?php echo $requestdemo['name'];?></td> 
                    <td><?php echo $requestdemo['email'];?></td> 
                    <td><?php echo $requestdemo['phone'];?></td> 
                    <td><?php if($requestdemo['request_date'] !='0000-00-00') echo date('M j, Y',strtotime($requestdemo['request_date'])); ?></td> 
                    <td class="last"><a href="<?php echo admin_url('Requestdemo/update_status/'.$requestdemo['status'].'/'.$requestdemo['id']); ?>"> <?php if($requestdemo['status']=='0') echo '<strong style="color:green;">Delivered</strong>'; else echo '<strong style="color:red;">Pending</strong>';?></a></td>
                  </tr>
                <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
           <?php echo form_close(); ?>
        </div>
      </div>
    </div>
    <div class="pagination_wrap">
    <ul class="pagination"><?php echo $this->pagination->create_links(); ?></ul>
    </div>
  </div>
<!-- /page content -->
