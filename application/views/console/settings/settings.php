<div class="right_col" role="main">
	<?php if($this->session->flashdata('message')){ ?>
	<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
	<div class="alert alert-dismissible fade in <?php echo $this->session->flashdata('message')['status']; ?>" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
	  </button>
	  <?php echo $this->session->flashdata('message')['message']; ?>
	</div>
	</div>
	</div>
	<?php } ?>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>General Settings</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br />
        <?php
        $attributes = array('class' => 'form-horizontal form-label-left', 'id' => 'settings-add');
        echo form_open(admin_url_string('home/settings'),$attributes);
        ?><input type="hidden" id="settings" name="settings" value="1"  >
		  
			<?php foreach($settings as $setting): ?>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">
			<?php echo $setting['title']; ?><span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <?php if($setting['settingtype']=='dashboard'){ ?>
			<input type="radio" name="setting[<?php echo $setting['id']; ?>]" <?php if($setting['settingvalue']=='W'){ echo 'checked="checked"';} ?> value="W" checked="checked" /> Weekly 
            <input type="radio" name="setting[<?php echo $setting['id']; ?>]" <?php if($setting['settingvalue']=='M'){ echo 'checked="checked"';} ?> value="M" /> Monthly
            <input type="radio" name="setting[<?php echo $setting['id']; ?>]" <?php if($setting['settingvalue']=='Y'){ echo 'checked="checked"';} ?> value="Y"  /> Yearly 
            <input type="radio" name="setting[<?php echo $setting['id']; ?>]" <?php if($setting['settingvalue']=='A'){ echo 'checked="checked"';} ?> value="A"  /> All 
			<?php } else if($setting['settingtype']=='radio'){ ?>
			<input type="radio" name="setting[<?php echo $setting['id']; ?>]" value="Y" <?php if($setting['settingvalue']=='Y'){ echo 'checked="checked"';} ?> /> Yes 
            <input type="radio" name="setting[<?php echo $setting['id']; ?>]" value="N" <?php if($setting['settingvalue']=='N'){ echo 'checked="checked"';} ?> /> No
			<?php } else if($setting['settingtype']=='textarea'){  ?>
			<textarea  name="setting[<?php echo $setting['id']; ?>]"  class="form-control col-md-7 col-xs-12" ><?php echo $setting['settingvalue']; ?></textarea>
			<?php } else {  ?>
			<input  name="setting[<?php echo $setting['id']; ?>]" type="text" class="form-control col-md-7 col-xs-12" value="<?php echo $setting['settingvalue']; ?>" />
			<?php } ?>
		</div>
            </div>
	<?php endforeach; ?>
          
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">

              <button type="submit" class="btn btn-success">Submit</button>
              <a class="btn btn-primary" href="<?php echo admin_url('home/settings'); ?>">Cancel</a>
            </div>
          </div>

        <?php echo form_close(); ?>
      </div>
    </div>
  </div>
</div>
</div>