<!-- page content -->
<div class="right_col" role="main">
<?php if($this->session->flashdata('message')){ ?>
  <div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
  <div class="alert alert-dismissible fade in <?php if(isset($this->session->flashdata('message')['status'])){ echo $this->session->flashdata('message')['status'];} ?>" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
      </button>
      <?php if(isset($this->session->flashdata('message')['message'])){ echo $this->session->flashdata('message')['message']; } ?>
  </div>
  </div>
  </div>
  <?php } ?>
  <div class="clearfix"></div>
  <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Services</h2>
            <ul class="nav navbar-right panel_toolbox">
                     <li><a class="btn btn-default btn-xs" href="<?php echo admin_url('services/add'); ?>" ><i class="fa fa-plus-square-o" aria-hidden="true"></i> &nbsp;Add New </a> </li>
             </ul>
            <div class="clearfix"></div>
          </div>
          <?php
                      $attributes = array('class' => 'form-horizontal form-label-left', 'id' => 'services-add');
                      echo form_open(admin_url_string('services/actions'),$attributes);  ?>

          <div class="x_content">

            <div class="table-responsive">
              <table class="table table-striped jambo_table">
                <thead>
                  <tr class="headings">
                    <th class="column-title">Title</th>
                    <th class="column-title">Preview</th>
                     <th class="column-title" >Sort Order
                      <button class="btn btn-default btn-xs"   style="margin:0px;" type="submit" name="sortsave" value="Save" >Save</button></th>
                    <th class="column-title">Status</th>
                    <th class="column-title no-link last"><span class="nobr">Action</span></th>

                  </tr>
                </thead>

                <tbody>
                <?php foreach($services as $service):?>
                  <tr class="even pointer">
                    <td class=" "><?php echo $service['title'];?></td>
                    <td><?php if($service['image']!='') { ?>
                        <img height="50px" src="<?php echo base_url('public/uploads/services/'.$service['image']);?>" />
                      <?php } else { ?>
                      <img height="50px" src="<?php echo base_url('public/frontend/img/noimage.jpg');?>" />
                        <?php } ?>
                      </td>
                      
                    <td><input style="text-align:center;padding:0; line-height:1;" type="text" size="2" name="sort_order[<?php echo $service['id']; ?>]" value="<?php echo $service['sort_order']; ?>" /></td>
                    <td><?php $status = array('0' => 'Disabled','1' => 'Enabled'); echo $status[$service['status']];?></td>
					<td class="last">
					<a href="<?php echo admin_url('services/edit/'.$service['id']); ?>" title="Edit"><i class="fa fa-edit"></i></a>
          <a class="confirmDelete" href="<?php echo admin_url('services/delete/'.$service['id']); ?>" title="Delete"><i   class="fa fa-trash-o"></i></a>
					</td>

                  </tr>
                <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
            <?php echo form_close(); ?>
        </div>
      </div>
    </div>
    <div class="pagination_wrap">
    <ul class="pagination"><?php echo $this->pagination->create_links(); ?></ul>
    </div>
  </div>
<!-- /page content -->
