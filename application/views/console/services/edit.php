<div class="right_col" role="main">
<div class="clearfix"></div>
    <br>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Edit Service</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br />
        <?php
        $attributes = array('class' => 'form-vertical form-label-left', 'id' => 'service-edit');
        echo form_open_multipart(admin_url_string('services/edit/'.$service->id),$attributes);?>
       <input type="hidden" name="id" value="<?php echo $service->id; ?>" />

            <div class="form-group">
			  <label class="control-label col-md-12 col-sm-12 col-xs-12" for="label">Title<span class="required">*</span>
			  </label>
			  <div class="col-md-12 col-sm-12 col-xs-12">
			  <?php echo form_error('title'); ?>
				<input type="text" id="title" name="title" required="required" value="<?php echo $service->title; ?>" class="form-control col-md-7 col-xs-12">
			  </div>
			  <div class="clearfix"></div>
			</div>
         
          
          <div class="form-group">
            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="label">Slug<span class="required">*</span>
            </label>
            <div class="col-md-12 col-sm-12 col-xs-12">
            <?php echo form_error('slug'); ?>
              <input type="text" id="slug" name="slug" required="required" value="<?php echo $service->slug; ?>" class="form-control col-md-7 col-xs-12">
            </div>
            <div class="clearfix"></div>
          </div>

          <div class="form-group">
              <label class="control-label col-md-12 col-sm-12 col-xs-12" for="short_desc">Short Description
              </label>
              <div class="col-md-12 col-sm-12 col-xs-12">
                  <textarea id="short_desc" class="form-control col-md-7 col-xs-12" name="short_desc"><?php echo $service->short_desc; ?></textarea>
              </div>
          </div>

			<div class="form-group">
			  <label class="control-label col-md-12 col-sm-12 col-xs-12" for="description">Body<span class="required">*</span> 
			  </label>
			  <div class="col-md-12 col-sm-12 col-xs-12">
			  <?php echo $this->ckeditor->editor("description",html_entity_decode($service->description)); ?>
			  </div>
			  <div class="clearfix"></div>
			</div>

           <div class="form-group">
			  <label class="control-label col-md-12 col-sm-12 col-xs-12" for="label">Metat Title
			  </label>
			  <div class="col-md-12 col-sm-12 col-xs-12">
				<input type="text" id="meta_title" name="meta_title" value="<?php echo $service->meta_title; ?>" class="form-control col-md-7 col-xs-12">
			  </div>
			  <div class="clearfix"></div>
			</div>

           <div class="form-group">
			  <label class="control-label col-md-12 col-sm-12 col-xs-12" for="label">Meta Keyword
			  </label>
			  <div class="col-md-12 col-sm-12 col-xs-12">
				<input type="text" id="meta_keywords" name="meta_keywords" value="<?php echo $service->meta_keywords; ?>" class="form-control col-md-7 col-xs-12">
			  </div>
			  <div class="clearfix"></div>
			</div>
          
          <div class="form-group">
              <label class="control-label col-md-12 col-sm-12 col-xs-12" for="meta_desc">Meta Description
              </label>
              <div class="col-md-12 col-sm-12 col-xs-12">
                  <textarea id="meta_desc" class="form-control col-md-7 col-xs-12" name="meta_desc"><?php echo $service->meta_desc; ?></textarea>
              </div>
          </div>
          <div class="clearfix"></div>
        </div>
          
          <div class="form-group">
              <label class="control-label col-md-12 col-sm-12 col-xs-12" for="label">Image <?php if($service->image!='') { echo '<img src="'.base_url('public/uploads/services/'.$service->image).'" width="50px" />'; } ?>
              </label>
              <div class="col-md-12 col-sm-12 col-xs-12">
                  <input id="image" name="image" class=" col-md-7 col-xs-12" style="padding:0px;"  type="file">
              </div>
              <div class="clearfix"></div>
          </div>
          
          <div class="form-group">
            <label class="control-label col-md-12 col-sm-12 col-xs-12">Status <span class="required">*</span></label>
            <div class="col-md-12 col-sm-12 col-xs-12">
            <?php echo form_error('status'); ?>
              <div id="status" class="btn-group" data-toggle="buttons">
                <label class="btn btn-default <?php if($service->status=='1') { echo 'active'; } ?>">
                  <input type="radio" required="required" name="status" value="1" <?php if($service->status=='1') { echo 'checked="checked"'; } ?>> &nbsp; Enabled &nbsp;
                </label>
                <label class="btn btn-default <?php if($service->status=='0') { echo 'active'; } ?>">
                  <input type="radio" required="required" name="status" value="0" <?php if($service->status=='0') { echo 'checked="checked"'; } ?>> Disabled
                </label>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12">

              <button type="submit" class="btn btn-success">Submit</button>
              <a class="btn btn-primary" href="<?php echo admin_url('services/overview/'); ?>">Cancel</a>
            </div>
          </div>

        <?php echo form_close(); ?>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>
</div>
