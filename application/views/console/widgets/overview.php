<!-- page content -->
<div class="right_col" role="main">
<?php if($this->session->flashdata('message')){ ?>
  <div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
  <div class="alert alert-dismissible fade in <?php if(isset($this->session->flashdata('message')['status'])){ echo $this->session->flashdata('message')['status'];} ?>" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
      </button>
      <?php if(isset($this->session->flashdata('message')['message'])){ echo $this->session->flashdata('message')['message']; } ?>
  </div>
  </div>
  </div>
  <?php } ?>
  <div class="clearfix"></div>
  <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Widgets</h2>
            <ul class="nav navbar-right panel_toolbox">
                     <li><a class="btn btn-default btn-xs" href="<?php echo admin_url('widgets/add'); ?>" ><i class="fa fa-plus-square-o" aria-hidden="true"></i> &nbsp;Add New </a> </li>
             </ul>
            <div class="clearfix"></div>
          </div>

          <div class="x_content">

            <div class="table-responsive">
              <table class="table table-striped jambo_table">
                <thead>
                  <tr class="headings">
                    <th class="column-title">Widget Name</th>
                    <th class="column-title">Key</th>
                    <th class="column-title no-link last"><span class="nobr">Action</span></th>

                  </tr>
                </thead>

                <tbody>
                <?php foreach($widgets as $widget):?>
                  <tr class="even pointer">
                    <td class=" "><?php echo $widget['widget_name'];?></td>
                    <td class=" "><?php echo $widget['widget_key'];?></td>
					<td class="last">
					<a href="<?php echo admin_url('widgets/edit/'.$widget['wid']); ?>" title="Edit"><i class="fa fa-edit"></i></a>
					</td>

                  </tr>
                <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="pagination_wrap">
    <ul class="pagination"><?php echo $this->pagination->create_links(); ?></ul>
    </div>
  </div>
</div>
<!-- /page content -->
