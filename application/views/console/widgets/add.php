<div class="right_col" role="main">
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Add Widget</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br />
        <?php
        $attributes = array('class' => 'form-vertical form-label-right', 'id' => 'widget-add');
        echo form_open_multipart(admin_url_string('widgets/add/'),$attributes);
        ?>
          <div class="form-group">
            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="widget_name">Widget Name<span class="required">*</span>
            </label>
            <div class="col-md-12 col-sm-12 col-xs-12">
            <?php echo form_error('widget_name'); ?>
              <input type="text" id="widget_name" name="widget_name" required="required" value="<?php echo set_value('widget_name'); ?>" class="form-control col-md-7 col-xs-12">
            </div>
            <div class="clearfix"></div>
          </div>
          
          <div class="form-group">
            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="description">Description
            </label>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <textarea id="description" class="form-control col-md-7 col-xs-12" name="description"><?php echo set_value('description'); ?></textarea>
            </div>
          </div>
          
          <div class="form-group">
            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="widget_key">Key<span class="required">*</span>
            </label>
            <div class="col-md-12 col-sm-12 col-xs-12">
            <?php echo form_error('widget_key'); ?>
              <input type="text" id="widget_key" name="widget_key" required="required" value="<?php echo set_value('widget_key'); ?>" class="form-control col-md-7 col-xs-12">
            </div>
            <div class="clearfix"></div>
          </div>
          
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12">

              <button type="submit" class="btn btn-success">Submit</button>
              <a class="btn btn-primary" href="<?php echo admin_url('widgets/overview/'); ?>">Cancel</a>
            </div>
          </div>

        <?php echo form_close(); ?>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>
</div>
