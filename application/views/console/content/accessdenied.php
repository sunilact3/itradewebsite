<div class="right_col" role="main">
<div class="clearfix"></div>
  <br/>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Access Denied</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
          <div class="form-group">
          <p>You do not have enough permission to access this page.</p>
          </div>
          <div class="ln_solid"></div>
          <div class="form-group" style="text-align:right;">
            <a class="btn btn-primary" href="<?php echo admin_url('home'); ?>">Back</a>
            <div class="clearfix"></div>
            <br/>
         </div>

      </div>
    </div>
  </div>
</div>
</div>