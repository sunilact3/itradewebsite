<!-- page content -->
<div class="right_col" role="main">
<?php if($this->session->flashdata('message')){ ?>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="alert alert-dismissible fade in <?php echo $this->session->flashdata('message')['status']; ?>" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <?php echo $this->session->flashdata('message')['message']; ?>
            </div>
        </div>
    </div>
<?php } ?>
<div class="clearfix"></div>
<br/>
  
    
<div class="col-md-12 col-sm-12 col-xs-12 dash-widgets">
<div class="col-md-6 col-sm-6 col-xs-12">
<?php if(in_array('new_request_lists',$widgets)){ ?>
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>New Requests</h2>
                    <div class="all-link"><a class="btn btn-primary" href="<?php echo admin_url('requestdemo'); ?>">View All</a></div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <!-- start project list -->
                    <table class="table table-striped projects">
                      <thead>
                        <tr>
                          <th style="">Name</th>
                          <th style="">Request Date</th>
                          <th style="">Status</th>
                        </tr>
                      </thead>
                      <tbody id="pending_table" >
                      <?php
                       if(count($newrequests)==0){
                       ?>
                       <tr><td colspan="4">No New Requests ...</td></tr>
                       <?php
                       } else {
                      foreach($newrequests as $newrequest):$i=0; ?>
                        <tr>
                          <td><?php echo $newrequest['name']; ?></td>
                          <td><?php echo date('M j, Y H:i A',strtotime($newrequest['request_date']));?></td>
                          <td><?php if($newrequest['status']==0) echo 'Delivered';else echo '<span style="color:#0f8334">Pending</span>'; ?></td>
                        </tr>
                      <?php endforeach; } ?>
                      </tbody>
                    </table>
                    <!-- end project list -->

                  </div>
                </div>
              </div>
              <?php }  ?>

            <div class="clearfix"></div>
</div>
<div class="col-md-6 col-sm-6 col-xs-12">
<?php if(in_array('new_enquiry_lists',$widgets)){ ?>
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>New Enquiries</h2>
                    <div class="all-link"><a class="btn btn-primary" href="<?php echo admin_url('contactus'); ?>">View All</a></div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <!-- start project list -->
                    <table class="table table-striped projects">
                      <thead>
                        <tr>
                          <th style="">Name</th>
                          <th style="">Enquiry Date</th>
                          <th style="">Status</th>
                          <th style="width: 20%">#View</th>
                        </tr>
                      </thead>
                      <tbody id="pending_table" >
                      <?php
                       if(count($newenquiries)==0){
                       ?>
                       <tr><td colspan="4">No New Enquiries to display...</td></tr>
                       <?php
                       } else {
                      foreach($newenquiries as $ne):$i=0; ?>
                        <tr>
                          <td><?php echo $ne['name']; ?></td>
                          <td><?php echo date('M j, Y H:i A',strtotime($ne['contact_date']));?></td>
                          <td><?php if($ne['status']==0) echo 'Closed';else echo '<span style="color:#0f8334">Open</span>'; ?></td>
                          <td>
                            <a href="<?php echo admin_url('contactus/view/'.$ne['id']); ?>" class="btn btn-primary btn-xs"><i class="fa fa-folder"></i> View </a>
                          </td>
                        </tr>
                      <?php endforeach; } ?>
                      </tbody>
                    </table>
                    <!-- end project list -->

                  </div>
                </div>
              </div>
              <?php }  ?>

            <div class="clearfix"></div>
</div>
</div>
