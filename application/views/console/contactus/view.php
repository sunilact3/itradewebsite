<div class="right_col" role="main">
<div class="clearfix"></div>
    
  <br/>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Contact Details</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br />
        <?php
        $attributes = array('class' => 'form-vertical form-label-left', 'id' => 'view-det');
        echo form_open(admin_url_string('contactus/view/'.$contact->id),$attributes);?>
        <input type="hidden" name="id" value="<?php echo $contact->id; ?>" />
        
			
            
            <div class="form-group">
			  <label class="control-label col-md-12 col-sm-12 col-xs-12" for="label">Name
			  </label>
			  <div class="col-md-12 col-sm-12 col-xs-12">
				<p><?php echo $contact->name; ?></p>
			  </div>
			  <div class="clearfix"></div>
			</div>
			
			<div class="form-group">
			  <label class="control-label col-md-12 col-sm-12 col-xs-12" for="label">Phone
			  </label>
			  <div class="col-md-12 col-sm-12 col-xs-12">
				<p><?php echo $contact->phone; ?></p>
			  </div>
			  <div class="clearfix"></div>
			</div>
			
			<div class="form-group">
			  <label class="control-label col-md-12 col-sm-12 col-xs-12" for="label">Email
			  </label>
			  <div class="col-md-12 col-sm-12 col-xs-12">
				<p><?php echo $contact->email; ?></p>
			  </div>
			  <div class="clearfix"></div>
			</div>
            
            <div class="form-group">
			  <label class="control-label col-md-12 col-sm-12 col-xs-12" for="label">Message
			  </label>
			  <div class="col-md-12 col-sm-12 col-xs-12">
				<p><?php echo $contact->message; ?></p>
			  </div>
			  <div class="clearfix"></div>
			</div>
          
           <div class="form-group">
            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="label">Enquiry Date
            </label>
            <div class="col-md-12 col-sm-12 col-xs-12">
               <p><?php if($contact->contact_date !='0000-00-00') echo  date('M j, Y',strtotime($contact->contact_date)); ?></p>
            </div>
            <div class="clearfix"></div>
          </div>         
          
          
          <div class="form-group">
            <label class="control-label col-md-12 col-sm-12 col-xs-12">Status</label>
            <div class="col-md-12 col-sm-12 col-xs-12">
            <?php echo form_error('status'); ?>
              <div id="status" class="btn-group" data-toggle="buttons">
                <label class="btn btn-default <?php if($contact->status=='1') { echo 'active'; } ?>">
                  <input type="radio" required="required" name="status" value="1" <?php if($contact->status=='1') { echo 'checked="checked"'; } ?>> &nbsp; Open &nbsp;
                </label>
                <label class="btn btn-default <?php if($contact->status=='0') { echo 'active'; } ?>">
                  <input type="radio" required="required" name="status" value="0" <?php if($contact->status=='0') { echo 'checked="checked"'; } ?>> Closed
                </label>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12">

              <button type="submit" class="btn btn-success">Submit</button>
              <a class="btn btn-primary" href="<?php echo admin_url('contactus/overview'); ?>">Cancel</a>
            </div>
          </div>

        <?php echo form_close(); ?>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>
</div>
