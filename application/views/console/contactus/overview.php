<!-- page content -->
<div class="right_col" role="main">
<?php if($this->session->flashdata('message')){ ?>
  <div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
  <div class="alert alert-dismissible fade in <?php if(isset($this->session->flashdata('message')['status'])){ echo $this->session->flashdata('message')['status'];} ?>" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
      </button>
      <?php if(isset($this->session->flashdata('message')['message'])){ echo $this->session->flashdata('message')['message']; } ?>
  </div>
  </div>
  </div>
  <?php } ?>
  <div class="clearfix"></div>
  <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Contact Us</h2>
            <div class="clearfix"></div>
          </div>

          <?php
            $attributes = array('class' => 'form-horizontal form-label-left', 'id' => 'filter');
            echo form_open(admin_url_string('contactus/overview'),$attributes);  ?>
         
            <div class="x_content">
            <div class="table-responsive">
              <table class="table table-striped jambo_table">
                <thead>
                  <tr class="headings">
                     <th class="column-title">Name</th>
                     <th class="column-title">Email</th>
                     <th class="column-title">Phone</th>
                     <th class="column-title">Contact.Date</th>
                     <th class="column-title">Status</th>
                    <th class="column-title no-link last"><span class="nobr">Action</span></th>
                </thead>

                <tbody>
                <?php $i=0; foreach($contactus as $contact):?>
                  <tr class="even pointer">
                    <td><?php echo $contact['name'];?></td> 
                    <td><?php echo $contact['email'];?></td> 
                    <td><?php echo $contact['phone'];?></td> 
                    <td><?php if($contact['contact_date'] !='0000-00-00') echo date('M j, Y',strtotime($contact['contact_date'])); ?></td>  
                    <td><?php $status = array('0' => 'Disabled','1' => 'Enabled'); echo $status[$contact['status']];?></td>
					<td class="last">
					<a href="<?php echo admin_url('contactus/view/'.$contact['id']); ?>"title="View"><i class="fa fa-eye"></i></a>
					</td>
                  </tr>
                <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
           <?php echo form_close(); ?>
        </div>
      </div>
    </div>
    <div class="pagination_wrap">
    <ul class="pagination"><?php echo $this->pagination->create_links(); ?></ul>
    </div>
  </div>
<!-- /page content -->
