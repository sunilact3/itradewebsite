<div class="right_col" role="main">
<div class="clearfix"></div>
  <br/>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Add Banner</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br />
        <?php

        $attributes = array('class' => 'form-vertical form-label-right', 'id' => 'banners-add');
        echo form_open_multipart(admin_url_string('banners/add'),$attributes);
        ?>
         


          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Title<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <?php echo form_error('title'); ?>
              <input type="text" id="title" name="title" required="required" value="<?php echo set_value('title'); ?>" class="form-control col-md-7 col-xs-12">
            </div>
          <div class="clearfix"></div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="banner_link">Link
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" id="banner_link" name="banner_link" value="<?php echo set_value('banner_link'); ?>" class="form-control col-md-7 col-xs-12">
            </div>
          <div class="clearfix"></div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="caption">Caption<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <?php echo form_error('caption'); ?>
              <input type="text" id="caption" name="caption" required="required" value="<?php echo set_value('caption'); ?>" class="form-control col-md-7 col-xs-12">
            </div>
          <div class="clearfix"></div>
          </div>



          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="label">Image<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <?php echo form_error('image'); ?>
               <input id="image" name="image" class="col-md-7 col-xs-12" style="padding:0px;"  type="file">
            </div>
            <div class="clearfix"></div>
          </div>


          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="status">Status<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <?php echo form_error('status'); ?>
              <div id="status" class="btn-group" data-toggle="buttons">
                <label class="btn btn-default <?php if(set_value('status')=='1') { echo 'active'; } ?>">
                  <input type="radio" required="required" name="status" value="1" <?php if(set_value('status')=='1') { echo 'checked="checked"'; } ?> /> &nbsp; Enabled &nbsp;
                </label>
                <label class="btn btn-default <?php if(set_value('status')=='0') { echo 'active'; } ?>">
                  <input type="radio" required="required" name="status" value="0" <?php if(set_value('status')=='0') { echo 'checked="checked"'; } ?> /> Disabled
                </label>
              </div>
            </div>
          <div class="clearfix"></div>
          </div>
          
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">

              <button type="submit" class="btn btn-success">Submit</button>
              <a class="btn btn-primary" href="<?php echo admin_url('banners/overview'); ?>">Cancel</a>
            </div>
          </div>

        <?php echo form_close(); ?>
      </div>
    </div>
  </div>
</div>
</div>
