<!-- page content -->
<div class="right_col" role="main">
<?php if($this->session->flashdata('message')){ ?>
  <div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
  <div class="alert alert-dismissible fade in <?php echo $this->session->flashdata('message')['status']; ?>" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
      </button>
      <?php echo $this->session->flashdata('message')['message']; ?>
  </div>
  </div>
  </div>
  <?php } ?>
  <div class="clearfix"></div>
  <br/>
  <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Widgets for <?php echo $role->name; ?></h2>
            <div class="clearfix"></div>
          </div>

          <div class="x_content">
            <?php
            $attributes = array('class' => '', 'id' => 'categories-add');
            echo form_open(admin_url_string('users/widgets/'.$role->rid),$attributes);
            ?>
            <div class="table-responsive">

              <table class="table table-striped jambo_table">
                <thead>
                  <tr class="headings">
                    <th class="column-title">Widget Name </th>
                    <th class="column-title" style="width:3%;">Permission </th>
                    </th>
                  </tr>
                </thead>

                <tbody>
                <?php $i=0; foreach($permissions as $menu): $i++; ?>
                <tr class="<?php if($i%2==0){ echo 'even';} else { echo 'odd';}?> pointer">
                <td><strong class="func_title"><?php echo $menu['widget_name']; ?></strong><br/><span class="func_desc"><?php echo $menu['description']; ?></span></td>
                <td class="last" style="text-align:center;">
                <input type="checkbox" name="widgets[<?php echo $menu['wid']; ?>]" value="1" <?php if(in_array($menu['wid'],$widgetspermission)) { echo 'checked="checked"'; } ?> /></td>
                </tr>
                <?php endforeach; ?>
                </tbody>
              </table>
              </div>
              <div class="form-group" style="text-align:right;">
                  <a class="btn btn-primary perm-back" href="<?php echo admin_url('users/roles'); ?>">Back</a>
                  <button type="submit" class="btn btn-success">Submit</button>
                  <div class="clearfix"></div>
                  <br/>
              </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->
