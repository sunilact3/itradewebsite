<?php
$status = array('0' => 'Disabled','1' => 'Enabled');
?>
<!-- page content -->
<div class="right_col" role="main">
<?php if($this->session->flashdata('message')){ ?>
  <div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
  <div class="alert alert-dismissible fade in <?php echo $this->session->flashdata('message')['status']; ?>" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
      </button>
      <?php echo $this->session->flashdata('message')['message']; ?>
  </div>
  </div>
  </div>
  <?php } ?>
  <div class="clearfix"></div>
  <br/>
  <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Roles</h2>
            <div class="clearfix"></div>
          </div>

          <div class="x_content">

            <div class="table-responsive">
              <table class="table table-striped jambo_table">
                <thead>
                  <tr class="headings">
                    <th class="column-title">Role Name </th>
                    <th class="column-title">Status </th>
                    <th class="column-title no-link last"><span class="nobr">Action</span>
                    </th>
                  </tr>
                </thead>

                <tbody>
                <?php foreach($roles as $role):?>
                  <tr class="even pointer">
                    <td class=" "><?php echo $role['name'];?></td>
                    <td class=" "><?php echo $status[$role['status']];?></td>
                    <td class=" last">
                    <?php if($role['rid']!='1'){ ?>
                    <a href="<?php echo admin_url('users/editrole/'.$role['rid']); ?>">Edit</a> |
                    <a href="<?php echo admin_url('users/menupermission/'.$role['rid']); ?>">Menu Permission</a> |
                    <a href="<?php echo admin_url('users/permission/'.$role['rid']); ?>">Permission</a> |
                    <a href="<?php echo admin_url('users/widgets/'.$role['rid']); ?>">Widgets</a>
                    <?php } ?>
                    </td>
                  </tr>
                <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="pagination_wrap">
    <ul class="pagination"><?php echo $this->pagination->create_links(); ?></ul>
    </div>
  </div>
</div>
<!-- /page content -->
