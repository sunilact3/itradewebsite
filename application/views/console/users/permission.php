<!-- page content -->
<div class="right_col" role="main">
<?php if($this->session->flashdata('message')){ ?>
  <div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
  <div class="alert alert-dismissible fade in <?php echo $this->session->flashdata('message')['status']; ?>" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
      </button>
      <?php echo $this->session->flashdata('message')['message']; ?>
  </div>
  </div>
  </div>
  <?php } ?>
  <div class="clearfix"></div>
  <br/>
  <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Permissions for <?php echo $role->name; ?></h2>
            <div class="clearfix"></div>
          </div>

          <div class="x_content">
            <?php
            $attributes = array('class' => '', 'id' => 'categories-add');
            echo form_open(admin_url_string('users/permission/'.$role->rid),$attributes);
            ?>
            <div class="table-responsive">

              <table class="table table-striped jambo_table">
                <thead>
                  <tr class="headings">
                    <th class="column-title">Function Name </th>
                    <th class="column-title" style="width:3%;">Permission </th>
                    </th>
                  </tr>
                </thead>

                <tbody>
                <?php $i=0; foreach($permissions as $menu): $submenu_count =count($menu['sub_menu']); $i++; ?>
                <tr class="<?php if($i%2==0){ echo 'even';} else { echo 'odd';}?> pointer">
                <td><strong class="func_title"><?php echo $menu['function_name']; ?></strong><br/><span class="func_desc"><?php echo $menu['description']; ?></span></td>
                <td class="last" style="text-align:center;">
                <input type="checkbox" name="functions[<?php echo $menu['id']; ?>]" value="1" <?php if(in_array($menu['id'],$functionpermission)) { echo 'checked="checked"'; } ?> /></td>
                </tr>
                <?php if($submenu_count>0){?>
                    <?php foreach($menu['sub_menu'] as $submenu): $i++; ?>
                    <tr class="<?php if($i%2==0){ echo 'even';} else { echo 'odd';}?> pointer">
                    <td>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong class="func_title"><?php echo $submenu['function_name']; ?></strong><br/>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="func_desc"><?php echo $submenu['description']; ?></span>
                    </td>
                    <td class=" last" style="text-align:center;">
                    <input type="checkbox" name="functions[<?php echo $submenu['id']; ?>]" value="1" <?php if(in_array($submenu['id'],$functionpermission)) { echo 'checked="checked"'; } ?> /></td>
                    </tr>
                    <?php endforeach; ?>
                <?php } ?>
                <?php endforeach; ?>
                </tbody>
              </table>
              </div>
              <div class="form-group" style="text-align:right;">
                  <a class="btn btn-primary perm-back" href="<?php echo admin_url('users/roles'); ?>">Back</a>
                  <button type="submit" class="btn btn-success">Submit</button>
                  <div class="clearfix"></div>
                  <br/>
              </div>

            <?php echo form_close(); ?>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->
