<div class="right_col" role="main">
<div class="clearfix"></div>
  <br/>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Change Password for <?php echo $user->fullname; ?></h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br />
        <?php
        $attributes = array('class' => 'form-horizontal form-label-left', 'id' => 'location-add');
        echo form_open(admin_url_string('users/changepwd/'.$user->uid),$attributes);
        ?>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Password<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <?php echo form_error('password'); ?>
              <input type="password" id="password" name="password" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="confpassword">Confirm Password<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <?php echo form_error('confpassword'); ?>
              <input type="password" id="confpassword" name="confpassword" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">

              <button type="submit" class="btn btn-success">Submit</button>
              <a class="btn btn-primary" href="<?php echo admin_url('users/overview'); ?>">Cancel</a>
            </div>
          </div>

        <?php echo form_close(); ?>
      </div>
    </div>
  </div>
</div>
</div>
