
        <!-- footer content -->
        <footer>

          <div class="footer-netfix-logo pull-right">
             &copy <?php echo date('Y'); ?> All Rights Reserved. <?php echo $this->config->item('site_name'); ?>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->