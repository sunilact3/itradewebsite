<div class="login-form">
<h4>Console Login</h4>
<?php
    echo form_open(admin_url_string('login')); ?>
  <div>
    <span class="validation_error"><?php echo form_error('username'); ?></span>
    <!--<label class="left" for="passconf">Username</label>-->
    <input type="text" name="username" class="form-control" placeholder="Username" />
  </div>
  <div>
    <span class="validation_error"><?php echo form_error('password'); ?></span>
    <!--<label class="left" for="passconf">Password</label>-->
    <input type="password" class="form-control" placeholder="Password" name="password" />
  </div>
  <div>
      <input type="hidden" class="form-control" placeholder="Password" name="target" value="<?php echo $this->session->userdata('admin_user_destination'); ?>" />
    <input type="submit" class="btn btn-default submit" name="Login" value="Login" />
    <a class="reset_pass" href="<?php echo admin_url('login/forgot'); ?>">Lost your password?</a>
  </div>

  <div class="clearfix"></div>
<?php echo form_close(); ?>
</div>