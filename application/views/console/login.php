<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $this->config->item('site_name'); ?> - Console Application</title>
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('public/console/images/favicon.ico'); ?>">
    <!-- Bootstrap -->
    <link href="<?php echo public_admin_base_url('vendors/bootstrap/dist/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo public_admin_base_url('vendors/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo public_admin_base_url('vendors/nprogress/nprogress.css'); ?>" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?php echo public_admin_base_url('css/animate.min.css'); ?>" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo public_admin_base_url('build/css/custom.css'); ?>" rel="stylesheet">
    <link href="<?php echo public_admin_base_url('css/celiums.css'); ?>" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>
      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <div class="login-logo"><img src="<?php echo public_admin_base_url('images/logo.png'); ?>" /></div>
            <?php echo $content; ?>
            <div class="separator">
            <p>&copy; <?php echo date('Y'); ?> All Rights Reserved. <br/><?php echo $this->config->item('site_name'); ?></p>
            <div>
          </section>
        </div>
      </div>
    </div>
  </body>
</html>
