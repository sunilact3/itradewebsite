<div class="right_col" role="main">
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Edit Page</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br />
        <?php
        $attributes = array('class' => 'form-vertical form-label-left', 'id' => 'page-edit');
        echo form_open_multipart(admin_url_string('pages/edit/'.$page->id),$attributes);?>
       <input type="hidden" name="id" value="<?php echo $page->id; ?>" />

      <div class="form-group">
			  <label class="control-label col-md-12 col-sm-12 col-xs-12" for="label">Title<span class="required">*</span>
			  </label>
			  <div class="col-md-12 col-sm-12 col-xs-12">
			  <?php echo form_error('title'); ?>
				<input type="text" id="title" name="title" required="required" value="<?php echo $page->title; ?>" class="form-control col-md-7 col-xs-12">
			  </div>
			  <div class="clearfix"></div>
			</div>

			<div class="form-group">
			  <label class="control-label col-md-12 col-sm-12 col-xs-12" for="page_key">Page Key<span class="required">*</span>
			  </label>
			  <div class="col-md-12 col-sm-12 col-xs-12">
			  <?php echo form_error('page_key'); ?>
				<input type="text" id="page_key" name="page_key" required="required" readonly value="<?php echo $page->page_key; ?>" class="form-control col-md-7 col-xs-12">
			  </div>
			  <div class="clearfix"></div>
			</div>

      <div class="form-group">
			  <label class="control-label col-md-12 col-sm-12 col-xs-12" for="meta_title">Meta Title<span class="required">*</span>
			  </label>
			  <div class="col-md-12 col-sm-12 col-xs-12">
			  <?php echo form_error('meta_title'); ?>
				<input type="text" id="meta_title" name="meta_title" required="required" value="<?php echo $page->meta_title; ?>" class="form-control col-md-7 col-xs-12">
			  </div>
			  <div class="clearfix"></div>
			</div>

          <div class="form-group">
            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="meta_keywords">Meta Keywords
            </label>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <textarea id="meta_keywords" class="form-control col-md-7 col-xs-12" name="meta_keywords"><?php echo $page->meta_keywords; ?></textarea>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="meta_desc">Meta Desc
            </label>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <textarea id="meta_desc" class="form-control col-md-7 col-xs-12" name="meta_desc"><?php echo $page->meta_desc; ?></textarea>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="short_desc">Short Desc
            </label>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <textarea id="short_desc" class="form-control col-md-7 col-xs-12" name="short_desc"><?php echo $page->short_desc; ?></textarea>
            </div>
          </div>

			<div class="form-group">
			  <label class="control-label col-md-12 col-sm-12 col-xs-12" for="label">Body<span class="required">*</span>
			  </label>
			  <div class="col-md-12 col-sm-12 col-xs-12">
			  <?php echo form_error('body'); ?>
			  <?php echo $this->ckeditor->editor("body",html_entity_decode($page->body)); ?>
			  </div>
			  <div class="clearfix"></div>
			</div>

          <div class="form-group">
            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="label">Banner <?php if($page->image!='') { echo '<img src="'.base_url('public/uploads/pages/'.$page->image).'" width="50px" />'; } ?>
            </label>
            <div class="col-md-12 col-sm-12 col-xs-12">
               <input id="image" name="image" class=" col-md-7 col-xs-12" style="padding:0px;"  type="file">
            </div>
            <div class="clearfix"></div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-12 col-sm-12 col-xs-12">Status <span class="required">*</span></label>
            <div class="col-md-12 col-sm-12 col-xs-12">
            <?php echo form_error('status'); ?>
              <div id="status" class="btn-group" data-toggle="buttons">
                <label class="btn btn-default <?php if($page->status=='1') { echo 'active'; } ?>">
                  <input type="radio" required="required" name="status" value="1" <?php if($page->status=='1') { echo 'checked="checked"'; } ?>> &nbsp; Enabled &nbsp;
                </label>
                <label class="btn btn-default <?php if($page->status=='0') { echo 'active'; } ?>">
                  <input type="radio" required="required" name="status" value="0" <?php if($page->status=='0') { echo 'checked="checked"'; } ?>> Disabled
                </label>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12">

              <button type="submit" class="btn btn-success">Submit</button>
              <a class="btn btn-primary" href="<?php echo admin_url('pages/overview/'); ?>">Cancel</a>
            </div>
          </div>

        <?php echo form_close(); ?>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>
</div>
