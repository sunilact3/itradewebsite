<!-- page content -->
<div class="right_col" role="main">
<?php if($this->session->flashdata('message')){ ?>
  <div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
  <div class="alert alert-dismissible fade in <?php if(isset($this->session->flashdata('message')['status'])){ echo $this->session->flashdata('message')['status'];} ?>" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
      </button>
      <?php if(isset($this->session->flashdata('message')['message'])){ echo $this->session->flashdata('message')['message']; } ?>
  </div>
  </div>
  </div>
  <?php } ?>
  <div class="clearfix"></div>
  <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Pages</h2>
            <div class="clearfix"></div>
          </div>

          <div class="x_content">

            <div class="table-responsive">
              <table class="table table-striped jambo_table">
                <thead>
                  <tr class="headings">
                    <th class="column-title">Title</th>
                    <th class="column-title">Page Key</th>
                    <th class="column-title last">Status</th>
                    <th class="column-title no-link last"><span class="nobr">Action</span></th>

                  </tr>
                </thead>

                <tbody>
                <?php foreach($pages as $page):?>
                  <tr class="even pointer">
                    <td class=" "><?php echo $page['title'];?></td>
                    <td class=" "><?php echo $page['page_key'];?></td>
                    <td class="last"><?php $status = array('0' => 'Disabled','1' => 'Enabled'); echo $status[$page['status']];?></td>
					<td class="last">
					<a href="<?php echo admin_url('pages/edit/'.$page['id']); ?>" title="Edit"><i class="fa fa-edit"></i></a>
					</td>

                  </tr>
                <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="pagination_wrap">
    <ul class="pagination"><?php echo $this->pagination->create_links(); ?></ul>
    </div>
  </div>
</div>
<!-- /page content -->
