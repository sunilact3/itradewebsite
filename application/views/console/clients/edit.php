<div class="right_col" role="main">
    <div class="clearfix"></div>
    <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
    <div class="x_title">
    <h2>Edit Clients</h2>
    <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <br />
        <?php
        $attributes = array('class' => 'form-vertical form-label-left', 'id' => 'banner-edit');
        echo form_open_multipart(admin_url_string('clients/edit/'.$clients->id),$attributes);?>
        <input type="hidden" name="id" value="<?php echo $clients->id; ?>" />
        
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="label">Title<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <?php echo form_error('title'); ?>
                <input type="text" id="title" name="title" required="required" value="<?php echo $clients->title; ?>" class="form-control col-md-7 col-xs-12">
            </div>
            <div class="clearfix"></div>
        </div>
        
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="link">Link
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="link" name="link" value="<?php echo $clients->link; ?>" class="form-control col-md-7 col-xs-12">
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="label">Image<span class="required">*</span> 
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <?php if($clients->image!='') { echo '<img src="'.base_url('public/uploads/clients/'.$clients->image).'" width="50px" />'; } ?>
                <input id="image" name="image" class=" col-md-12 col-xs-12" style="padding:0px; margin-top:10px;"  type="file">
            </div>
            <div class="clearfix"></div>
        </div>
        
        <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Status <span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <?php echo form_error('status'); ?>
                <div id="status" class="btn-group" data-toggle="buttons">
                    <label class="btn btn-default <?php if($clients->status=='1') { echo 'active'; } ?>">
                        <input type="radio" required="required" name="status" value="1" <?php if($clients->status=='1') { echo 'checked="checked"'; } ?>> &nbsp; Enabled &nbsp;
                    </label>
                    <label class="btn btn-default <?php if($clients->status=='0') { echo 'active'; } ?>">
                        <input type="radio" required="required" name="status" value="0" <?php if($clients->status=='0') { echo 'checked="checked"'; } ?>> Disabled
                    </label>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        
        <div class="ln_solid"></div>
        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                 <button type="submit" class="btn btn-success">Submit</button>
                <a class="btn btn-primary" href="<?php echo admin_url('clients/overview/'); ?>">Cancel</a>
            </div>
        </div>
        
        <?php echo form_close(); ?>
        <div class="clearfix"></div>
        </div>
        </div>
        </div>
    </div>
</div>
