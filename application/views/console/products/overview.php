<?php
$status = array('0' => 'Disabled','1' => 'Enabled');
?>
<!-- page content -->
<div class="right_col" role="main">
<?php if($this->session->flashdata('message')){ ?>
  <div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
  <div class="alert alert-dismissible fade in <?php if(isset($this->session->flashdata('message')['status'])){ echo $this->session->flashdata('message')['status'];} ?>" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
      </button>
      <?php if(isset($this->session->flashdata('message')['message'])){ echo $this->session->flashdata('message')['message']; } ?>
  </div>
  </div>
  </div>
  <?php } ?>
  <div class="clearfix"></div>
  <br/>
  <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Products</h2>
               <ul class="nav navbar-right panel_toolbox" id="menu">
                     <li><a class="btn btn-default btn-xs" href="<?php echo admin_url('products/add'); ?>" ><i class="fa fa-plus-square-o" aria-hidden="true"></i> &nbsp;Add New </a> </li>
			   </ul>
            <div class="clearfix"></div>
          </div>
              <?php
                $attributes = array('class' => 'form-horizontal form-label-left', 'id' => 'filter');
                echo form_open(admin_url('products/actions'));  ?>
            
            
 
       

          <div class="x_content">

            <div class="table-responsive">
              <table class="table table-striped jambo_table">
                <thead>
                  <tr class="headings">
                    <th class="column-title">Title</th>
                    <th class="column-title">Icon</th>
                    <th class="column-title" style="width:140px;">Sort Order
                    <button class="btn btn-default btn-xs"   style="margin:0px;" type="submit" name="sortsave" value="Save" >Save</button></th>
                    <th class="column-title">Status</th>
                    <th class="column-title no-link last"><span class="nobr">Action</span>
                    </th>
                  </tr>
                </thead>

                <tbody>
                <?php foreach($products as $product):?>
                  <tr class="even pointer">
                    <td class=" "><?php echo $product['title'];?></td>
                    <td class=" "><img src="<?php if($product['icon']!='') { echo base_url('public/uploads/products/icon/'.$product['icon']); } else { echo base_url('public/console/images/noimage.jpg'); } ?>" width="50"></td>
					<td class=""><input style="text-align:center;padding:0; line-height:1;" type="text" size="2" name="sort_order[<?php echo $product['id']; ?>]" value="<?php echo $product['sort_order']; ?>" /></td> 
                    <td class=" "><?php echo $status[$product['status']];?></td>
                    <td class="last">
					                 
                    <a href="<?php echo admin_url('products/edit/'.$product['id']); ?>"><i class="fa fa-edit"></i></a>
                    <a class="confirmDelete" href="<?php echo admin_url('products/delete/'.$product['id']); ?>"><i class="fa fa-trash-o"></i></a>
                    
                    </td>
                  </tr>
                <?php endforeach; ?>
                </tbody>
              </table>
            </div>
          </div>
           <?php echo form_close(); ?>
        </div>
      </div>
    </div>
    <div class="pagination_wrap">
    <ul class="pagination"><?php echo $this->pagination->create_links(); ?></ul>
    </div>
  </div>
  

<!-- /page content -->
