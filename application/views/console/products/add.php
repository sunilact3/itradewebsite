<div class="right_col" role="main">
<div class="clearfix"></div>
  <br/>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Add Products</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br />
        <?php
        $attributes = array('class' => 'form-vertical form-label-right', 'id' => 'product-add');
        echo form_open_multipart(admin_url_string('products/add'),$attributes);
        ?>

          <div class="form-group">
            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="title">Title<span class="required">*</span>
            </label>
            <div class="col-md-12 col-sm-12 col-xs-12">
            <?php echo form_error('title'); ?>
              <input type="text" id="title" name="title" required="required" value="<?php echo set_value('title'); ?>" class="form-control col-md-7 col-xs-12">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="short_desc">Short Desc
            </label>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <textarea id="short_desc" class="form-control col-md-7 col-xs-12" name="short_desc"><?php echo set_value('short_desc'); ?></textarea>
            </div>
          </div>

          <div class="form-group">
			  <label class="control-label col-md-12 col-sm-12 col-xs-12" for="description">Description
			  </label>
			  <div class="col-md-12 col-sm-12 col-xs-12">
			  <?php echo $this->ckeditor->editor("description",html_entity_decode(set_value('description'))); ?>
			  </div>
			  <div class="clearfix"></div>
		  </div>

          <div class="form-group">
            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="meta_title">Meta Title
            </label>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <input type="text" id="meta_title" name="meta_title" value="<?php echo set_value('meta_title'); ?>" class="form-control col-md-7 col-xs-12">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="meta_keywords">Meta Keywords
            </label>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <textarea id="meta_keywords" class="form-control col-md-7 col-xs-12" name="meta_keywords"><?php echo set_value('meta_keywords'); ?></textarea>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="meta_desc">Meta Desc
            </label>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <textarea id="meta_desc" class="form-control col-md-7 col-xs-12" name="meta_desc"><?php echo set_value('meta_desc'); ?></textarea>
            </div>
          </div>



          <div class="form-group">
            <label class="control-label col-md-12 col-sm-12 col-xs-12" for="label">Icon
            </label>
            <div class="col-md-12 col-sm-12 col-xs-12">
               <input id="icon" name="icon" class="col-md-7 col-xs-12" style="padding:0px;"  type="file">
            </div>
            <div class="clearfix"></div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-12 col-sm-12 col-xs-12">Status <span class="required">*</span></label>
            <div class="col-md-12 col-sm-12 col-xs-12">
            <?php echo form_error('status'); ?>
              <div id="status" class="btn-group" data-toggle="buttons">
                <label class="btn btn-default <?php if(set_value('status')=='1') { echo 'active'; } ?>">
                  <input type="radio" required="required" name="status" value="1" <?php if(set_value('status')=='1') { echo 'checked="checked"'; } ?> /> &nbsp; Enabled &nbsp;
                </label>
                <label class="btn btn-default <?php if(set_value('status')=='0') { echo 'active'; } ?>">
                  <input type="radio" required="required" name="status" value="0" <?php if(set_value('status')=='0') { echo 'checked="checked"'; } ?> /> Disabled
                </label>
              </div>
            </div>
          </div>

        <div class="clearfix"></div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-3">

              <button type="submit" class="btn btn-success">Submit</button>
              <a class="btn btn-primary" href="<?php echo admin_url('products/overview'); ?>">Cancel</a>
            </div>
          </div>

        <?php echo form_close(); ?>
      </div>
    </div>
  </div>
</div>
</div>
