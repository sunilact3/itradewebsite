<?php
class CSAdmin_Controller extends CI_Controller {

    function __construct() {

        parent::__construct();
        if (!$this->session->userdata('admin_user_logged_in')) {
            $redirect_data =array('user_destination' => current_url());
            $this->session->set_userdata($redirect_data);
            $urlstring = admin_url_string('login');
            redirect($urlstring);

        }

        $this->load->model(admin_url_string('SettingsModel'));
        $settings=$this->SettingsModel->get_array_set();
        foreach($settings as $setting):
            $this->settings[$setting['settingkey']]=$setting['settingvalue'];
        endforeach;


        if($this->session->userdata('admin_user_role')!='1'){
            $allowedfuncs = array('home/logout','home/accessdenied','home/index');
            $function_link =  strtolower($this->router->fetch_class().'/'.$this->router->fetch_method());
            if(!in_array($function_link,$allowedfuncs)){
                if(!$this->check_permission($function_link,$this->session->userdata('admin_user_role'))){
                    redirect(admin_url_string('home/dashboard'));
                }
            }
        }
        $this->mainvars = array();
        $this->mainvars['side_menu']=$this->side_menu();
        $this->mainvars['top_menu']=$this->load->view(admin_url_string('includes/top_menu'),'',true);
        $this->mainvars['footer']=$this->load->view(admin_url_string('includes/footer'),'',true);
        $this->mainvars['page_scripts']='';
        //$this->output->enable_profiler(TRUE);


    }

    function check_permission($function_link,$role_id){
        $this->load->model(admin_url_string('RolesModel'));
        return $this->RolesModel->check_permission($function_link,$role_id);
    }

    function check_reqpermission($status,$role_id){
        $this->load->model(admin_url_string('RolesModel'));
        return $this->RolesModel->check_reqpermission($status,$role_id);
    }

    function side_menu(){
        $this->load->model(admin_url_string('AdminMenuModel'));
        $vars['menus'] = $this->AdminMenuModel->get_menu();
        return $this->load->view(admin_url_string('includes/side_menu'),$vars,true);
    }
    function paginationConfig(){
        $config = array();
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['uri_segment'] = 4;
        $config['per_page'] = 15;
        return $config;
    }

    function sendfromadmin($to, $subject, $message, $attachment = '') {
        $this->load->library('email');
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['wordwrap'] = TRUE;
        $this->email->initialize($config);
        $this->email->from ($this->settings['FROM_MAIL'],$this->settings['FROM_NAME']);
        $this->email->to($to);
        $this->email->subject($subject);
        if ($attachment != '') {
            $this->email->attach($attachment);
        }
        $this->email->message($message);
        if ($this->email->send()) {
            return true;
        } else {
            return false;
        }
    }


    function ckeditorCall(){
        $this->load->library('ckeditor');
        $this->load->library('ckfinder');
        $this->ckeditor->basePath = base_url('public/console/ckeditor/').'/';
        if($this->session->userdata('admin_user_language')=='ar'){
            $this->ckeditor->config['language'] = 'ar';
        } else {
            $this->ckeditor->config['language'] = 'en';
        }
        $this->ckeditor->config['width'] = '100%';
        $this->ckeditor->config['height'] = '200px';
        //Add Ckfinder to Ckeditor
        $this->ckfinder->SetupCKEditor($this->ckeditor,base_url('public/console/ckfinder/').'/');
    }

}
