<?php
/* Frontend Controller */

class CSFront_Controller extends CI_Controller {

    function __construct() {
        parent::__construct();
		$this->settings=array();
		$this->fronthead();
        
        
    }
	function fronthead()
	{
		$this->load->model(frontend_model_path('SettingsModel'));
		$settings=$this->SettingsModel->get_array_set();
		foreach($settings as $setting):
			$this->settings[$setting['settingkey']]=$setting['settingvalue'];
		endforeach;
		$this->pagetitle=$this->settings['DEFAULT_META_TITLE'];
		$this->metadesc=$this->settings['DEFAULT_META_DESCRIPTION'];
		$this->metakeywords=$this->settings['DEFAULT_META_KEYWORDS'];
		//$this->breadcrumbarr=array('/'=>$this->settings['BREADCRUMB_START']);
	}

	function frontmetahead()
	{
		return  $this->load->view('frontend/include/meta','',true);
	}

	function frontheaderbar()
	{
        
       
		return  $this->load->view('frontend/include/headerbar','',true);
	}
	function fronthomeheader()
	{
		$homeheader['headerbar']=$this->frontheaderbar();
		return  $this->load->view('frontend/include/homeheader',$homeheader,true);
	}
	function fronthomefooter($footercontent)
	{
		$footer['footercontent']=$footercontent;
		return $this->load->view('frontend/include/footerbottom',$footer,true);
	}
	function frontcontent($frontcontents)
	{
		$frontcontent['maintcontent']=$frontcontents;
		return $this->load->view('frontend/include/content',$frontcontent,true);
	}
	function frontheader($headerbanner)
	{
		
        $homeheader['headerbar']=$this->frontheaderbar();
		$homeheader['headerbanner']= $headerbanner;
		return  $this->load->view('frontend/include/header',$homeheader,true);
	}
	function frontfooter($footerbg)
	{
		$footer['bg_image']=$footerbg;
        $this->load->model(frontend_model_path('menuitems_model'));
		$footer['mainmenu']=$this->menuitems_model->get_active_array();
        $this->load->model(frontend_model_path('productcategory_model'));
		$footer['categorytree'] = $this->productcategory_model->get_category_footertree();
		return $this->load->view('frontend/include/footer',$footer,true);
	}

	function sendfromadmin($to,$subject,$message,$attachment='')
	{
		$this->load->library('email');
		$config['protocol'] = 'sendmail';
		$config['mailtype'] = 'html';
		$config['charset'] = 'utf-8';
		$config['wordwrap'] = TRUE;
		$this->email->initialize($config);
		$this->email->from($this->settings['FROM_EMAIL'],$this->settings['SITE_NAME']);
		$this->email->to($to);
		$this->email->subject($subject);
		$this->email->message($message);
		if($attachment!=''){$this->email->attach($attachment);	}
		if($this->email->send()){ $this->email->clear(true); return true; } else { return false; }

	}

	function sendtoadmin($fromemail,$fromname,$attachment='',$subject,$message='')
	{
		$this->load->library('email');
		$config['protocol'] = 'sendmail';
		$config['mailtype'] = 'html';
		$config['charset'] = 'utf-8';
		$config['wordwrap'] = TRUE;
		$this->email->initialize($config);

		$this->email->from($fromemail, $fromname);
		$this->email->to($this->settings['ADMIN_EMAIL']);
		$this->email->subject($subject);
		$this->email->message($message);
		$this->email->attach($attachment);
		if($this->email->send()){ $this->email->clear(true); return true; } else { return false; }
	}

	function adminaccountnotification($subject,$message,$attachment='')
	{
		$this->load->library('email');
		$config['protocol'] = 'sendmail';
		$config['mailtype'] = 'html';
		$config['charset'] = 'utf-8';
		$config['wordwrap'] = TRUE;
		$this->email->initialize($config);
		$this->email->from ($this->settings['FROM_EMAIL'],$this->settings['SITE_NAME']);
		$this->email->to($this->settings['ACCOUNT_EMAIL']);
		$this->email->subject($subject);
		$this->email->message($message);
		if($attachment!=''){$this->email->attach($attachment);	}
		if($this->email->send()){ $this->email->clear(true); return true; } else { return false; }
	}

	function sendtofriend($fromemail,$fromname,$toemail,$toname,$subject,$message)
	{
		$this->load->library('email');
		$config['protocol'] = 'sendmail';
		$config['mailtype'] = 'html';
		$config['charset'] = 'utf-8';
		$config['wordwrap'] = TRUE;
		$this->email->initialize($config);

		$this->email->from($fromemail,$fromname);
		$this->email->to($toemail,$toname);
		$this->email->subject($subject);
		$this->email->message($message);
		if($this->email->send()){ $this->email->clear(true); return true; } else { return false; }
	}

	function sendemail($fromemail,$fromname,$toemail,$toname,$subject,$message)
	{
		$this->load->library('email');
		$config['protocol'] = 'sendmail';
		$config['mailtype'] = 'html';
		$config['charset'] = 'utf-8';
		$config['wordwrap'] = TRUE;
		$this->email->initialize($config);

		$this->email->from($fromemail,$fromname);
		$this->email->to($toemail);
		$this->email->subject($subject);
		$this->email->message($message);
		if($this->email->send()){ $this->email->clear(true); return true; } else { return false; }
	}

	function outputCache(){
		if($this->settings['CACHE_TIME']!='0'){
			$this->output->cache($this->settings['CACHE_TIME']);
		}
	}
}