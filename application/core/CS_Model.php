<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class CS_Model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->table_name = '';
        $this->primary_key = '';
    }

    function get_array() {
        $query = $this->db->get($this->table_name);
        return $query->result_array();
    }

    function get_array_limit($limit) {
        $this->db->limit($limit);
		$this->db->where('status','1');
        $query = $this->db->get($this->table_name);
        return $query->result_array();
    }
    
    function get_array_cond($cond) {
        if (is_array($cond) && count($cond) > 0) {
            $this->db->where($cond);
        }
        $query = $this->db->get($this->table_name);
        return $query->result_array();
    }

	function get_active()
	{
		$this->db->where('status','1');
		$this->db->from($this->table_name);
		$query = $this->db->get();
		return $query->result_array();
	}


	function get_active_array()
	{
		$this->db->where('status','1');
		$this->db->from($this->table_name);
		$query = $this->db->get(); 
		return $query->result_array();
	}

    
    function load($id) {
        $id = $this->db->escape_str($id);
        $cond = array($this->primary_key => $id);
        $this->db->where($cond);
        $query = $this->db->get($this->table_name);
        return $query->row();
    }

    function get_row_cond($cond) {
		$this->db->where($cond);
        $query = $this->db->get($this->table_name);
        return $query->row();
    }

    function insert($data) {
        $this->db->insert($this->table_name, $data);
        return $this->db->insert_id();
    }

    function update($data, $cond) {
        return $this->db->update($this->table_name, $data, $cond);
    }

    function delete($cond) {
        return $this->db->delete($this->table_name, $cond);
    }

    function get_pagination_count($cond = '') {
        $this->db->select('*');
        if (is_array($cond) && count($cond) > 0) {
            $this->db->where($cond);
        }
        $this->db->from($this->table_name);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_pagination($num, $offset, $cond = '') {
        $this->db->select('*');
        if (is_array($cond) && count($cond) > 0) {
            $this->db->where($cond);
        }
        $this->db->from($this->table_name);
        $this->db->limit($num, $offset);
        $query = $this->db->get();
        return $query->result_array();
    }
    function row_exists($cond) {
            if (is_array($cond)) {
            $this->db->where($cond);
            }
            $query = $this->db->get($this->table_name);
            $result = $query->num_rows();
            if ($result > 0) {
                return $query->row();
            } else {
                return false;
            }
      }
      function get_count($cond = '') {
          $this->db->select('*');
          if (is_array($cond) && count($cond) > 0) {
              $this->db->where($cond);
          }
          $this->db->from($this->table_name);
          $query = $this->db->get();
          return $query->num_rows();
      }

    function create_slug($title)
    {
        $slug=sanitizeStringForUrl($title);
        $this->db->where('slug',$slug);
        $this->db->from($this->table_name);
        $query = $this->db->get();
        $result = $query->num_rows();
        if($result>0){
            return $slug.date('ymdhis');
        } else {
            return $slug;
        }
    }
    
    function update_slug($slug,$id)
    {
        $slugtxt = str_replace('&','and', $slug);
        $slugtxt = str_replace(" ",'_',$slugtxt);
        $slugtxt = str_replace('%','_',$slugtxt);
        $slugtxt = str_replace('-','_',$slugtxt);
        $slugtxt = str_replace('(','_',$slugtxt);
        $slugtxt = str_replace(')','_',$slugtxt);
        $slug=sanitizeStringForUrl($slugtxt);
        $this->db->where('slug',$slug);
        $this->db->where('id !=',$id);
        $this->db->from($this->table_name);
        $query = $this->db->get();
        $result = $query->num_rows();
        if($result>0){
            return $slug.date('ymdhis');
        } else {
            return $slug;
        }
    }
}
